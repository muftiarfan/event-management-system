<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['email_templates'] = array(
	'user_registration' => array(
		'message' 		=> '
<h3>Verify this email address</h3>

<p>
You recently registered for an event. To verify that you 
own this email address, simply click on the link below.
</p> 
<p>
Your email address was added to the User: %username%.
</p>
<p>
Your login credentials are as follows:<br/>
<strong>Username:</strong> %username% <br/>
<strong>Password:</strong> %password% <br/>
</p>
<p>To verify your email, click on the link below (or copy and paste the URL into your browser):<br>
<a href="%url%">%url%</a></p>

<p>If you have any concerns, please contact us at %contact% </p>
		',
		),
			'user_registration_admin' => array(
		'message' 		=> '
<h3>New EMS User Registration</h3>

<p>
New user signed up for an event on EMS
</p> 
<p>
Details:
</p>
<p>
<strong>Name:</strong> %fullname% <br/>
<strong>Phone:</strong> %phone% <br/>
<strong>Mobile:</strong> %mobile% <br/>
<strong>Email:</strong> %email% <br/>
<strong>Team/Org:</strong> %team% <br/>
</p>
		',
		),
);

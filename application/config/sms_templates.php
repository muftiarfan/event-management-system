<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['sms_templates'] = array(
	'otp' => array(
		'message' 		=> 'Your OTP for EMS is %%otp%%. Thanks',
		'gateway_id'	=> 1,
		),
	'registration_success' => array(
		'message' => 'You have successfully registered for %%event%%. Please check your mail to confirm your participation and further instructions. Thanks.',
		'gateway_id' => 1,
		),
);

/* End of file sms_templates.php */
/* Location: ./system/application/config/sms_templates.php */
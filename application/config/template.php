<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| Active template
|--------------------------------------------------------------------------
|
| The $template['active_template'] setting lets you choose which template 
| group to make active.  By default there is only one group (the 
| "default" group).
|
*/
$template['active_template'] = 'default';

/*
|--------------------------------------------------------------------------
| Explaination of template group variables
|--------------------------------------------------------------------------
|
| ['template'] The filename of your master template file in the Views folder.
|   Typically this file will contain a full XHTML skeleton that outputs your
|   full template or region per region. Include the file extension if other
|   than ".php"
| ['regions'] Places within the template where your content may land. 
|   You may also include default markup, wrappers and attributes here 
|   (though not recommended). Region keys must be translatable into variables 
|   (no spaces or dashes, etc)
| ['parser'] The parser class/library to use for the parse_view() method
|   NOTE: See http://codeigniter.com/forums/viewthread/60050/P0/ for a good
|   Smarty Parser that works perfectly with Template
| ['parse_template'] FALSE (default) to treat master template as a View. TRUE
|   to user parser (see above) on the master template
|
| Region information can be extended by setting the following variables:
| ['content'] Must be an array! Use to set default region content
| ['name'] A string to identify the region beyond what it is defined by its key.
| ['wrapper'] An HTML element to wrap the region contents in. (We 
|   recommend doing this in your template file.)
| ['attributes'] Multidimensional array defining HTML attributes of the 
|   wrapper. (We recommend doing this in your template file.)
|
| Example:
| $template['default']['regions'] = array(
|    'header' => array(
|       'content' => array('<h1>Welcome</h1>','<p>Hello World</p>'),
|       'name' => 'Page Header',
|       'wrapper' => '<div>',
|       'attributes' => array('id' => 'header', 'class' => 'clearfix')
|    )
| );
|
*/

/*
|--------------------------------------------------------------------------
| Default Template Configuration (adjust this or create your own)
|--------------------------------------------------------------------------
*/

$template['default']['template'] = 'layouts/default';
$template['default']['regions'] = array(
   'title',
   'sub_title',
   'alert',
   'messages',
   'content',
   'navigation',
   'userinfo',
);
$template['default']['parser'] = 'parser';
$template['default']['parser_method'] = 'parse';
$template['default']['parse_template'] = FALSE;

$template['defaultv2']['template'] = 'layouts/defaultv2';
$template['defaultv2']['regions'] = array(
   'title',
   'sub_title',
   'alert',
   'messages',
   'content',
   'navigation',
   'userinfo',
);
$template['defaultv2']['parser'] = 'parser';
$template['defaultv2']['parser_method'] = 'parse';
$template['defaultv2']['parse_template'] = FALSE;

$template['defaultv2main']['template'] = 'layouts/defaultv2main';
$template['defaultv2main']['regions'] = array(
   'title',
   'navigation',
   'content',
);
$template['defaultv2main']['parser'] = 'parser';
$template['defaultv2main']['parser_method'] = 'parse';
$template['defaultv2main']['parse_template'] = FALSE;

$template['user']['template'] = 'layouts/user';
$template['user']['regions'] = array(
   'title',
   'sub_title',
   'alert',
   'messages',
   'content',
   'userinfo',
);
$template['user']['parser'] = 'parser';
$template['user']['parser_method'] = 'parse';
$template['user']['parse_template'] = FALSE;

$template['print']['template'] = 'layouts/print';
$template['print']['regions'] = array(
   'title',
   'content',
   'printinfo',
);
$template['print']['parser'] = 'parser';
$template['print']['parser_method'] = 'parse';
$template['print']['parse_template'] = FALSE;

/* End of file template.php */
/* Location: ./system/application/config/template.php */
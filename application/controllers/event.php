<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Event extends CI_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('user_model','userM');
        $this->load->model('frontend_model','frontM');
        $this->load->model('event_model','eventM');
        $this->load->model('fees_model','feeM');
        $this->load->library('form_validation');
        $this->load->library('upload');
        $this->form_validation->set_error_delimiters('', '<br/>');
        if(!$this->ion_auth->logged_in()){
        	redirect('user/dashboard');
        }
        $this->template->set_template('user');
        $this->userM->user_basics();
    }

    public function index(){
    	//echo 'lol';
    }

    public function add(){
        if(!$this->ion_auth->in_group(array(1,10))){
            show_404();
        }
    	$this->template->add_js('assets/js/moment.js');
    	$this->template->add_js('assets/js/bootstrap-datetimepicker.min.js');
    	$this->template->add_js('assets/js/bootstrap.js');
        $this->template->add_js('assets/plugins/bootstrap3-wysiwyg/js/bootstrap3-wysihtml5.all.min.js');
        $this->template->add_js('assets/js/select2.min.js');
        $this->template->add_css('assets/plugins/bootstrap3-wysiwyg/css/bootstrap3-wysihtml5.min.css');
        $this->template->add_css('assets/css/select2.min.css');
    	$this->template->add_js("$(function () {
                $('#datetimepicker1').datetimepicker({
                     format: 'YYYY-M-D hh:mm'
                });
            });",'embed');
        $this->template->add_js('$(document).ready(function(){$(\'.wysihtml5\').wysihtml5({
  toolbar: {
    "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
    "emphasis": true, //Italics, bold, etc. Default true
    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
    "html": false, //Button which allows you to edit the generated HTML. Default false
    "link": true, //Button to insert a link. Default true
    "image": true, //Button to insert an image. Default true,
    "color": false, //Button to change color of font  
    "blockquote": true, //Blockquote  
    "size": "sm" //default: none, other options are xs, sm, lg
  }
});

$( ".event_org" ).select2({        
            ajax: {
                url: baseUrl+"event/ajax_organizers",
                dataType: "json",
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term // search term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            minimumInputLength: 2
        });
    });','embed');
    	$this->template->add_css('assets/css/bootstrap-datetimepicker.min.css');
    	$data['user'] = $this->ion_auth->user()->row();
    	$data['e_types'] = $this->eventM->event_types();
        $data['fee_tickets'] = $this->feeM->tickets_types();
    	$this->form_validation->set_rules('event_name','Event Name','required|xss_clean|trim|min_length[4]|max_length[65]');
    	$this->form_validation->set_rules('event_date','Event Date','required|xss_clean|trim');
    	$this->form_validation->set_rules('event_desc','Event Description','required|xss_clean|trim');
    	$this->form_validation->set_rules('event_cats','Event Platforms','required|xss_clean|trim');
    	$this->form_validation->set_rules('event_members','Event Members','required|xss_clean|trim|is_natural_no_zero');
        $this->form_validation->set_rules('event_type','Event Type','required|xss_clean|trim|callback_validtype');
        $this->form_validation->set_rules('event_location','Venue','required|xss_clean');
        $this->form_validation->set_rules('event_ticket_extra_info','Ticket Info','required|xss_clean');
    	$this->form_validation->set_rules('event_org','Organizer','required|xss_clean');
        $this->form_validation->set_rules('icard_header_img', 'ID Card Logo', 'file_required|file_allowed_type[image]');
        $this->form_validation->set_rules('ticket_logo', 'Ticket Header Logo', 'file_required|file_allowed_type[image]');
        $this->form_validation->set_rules('ticket_logo_footer', 'Ticket Footer Image', 'file_required|file_allowed_type[image]');

    	if($this->form_validation->run()==FALSE){
    		$this->template->write('title', 'Add Event')
				->write_view('content', 'a_event/add',$data)
				->render();
    	
    	}else{
            $upload_card = array(
                        'upload_path' => './uploads/events/images/',
                        'allowed_types' => 'jpeg|jpg|png',
                        'encrypt_name' => true,
                        'max_width' => 1549,
                        'max_height' => 339,
                        );             
            $ticket_header = array(
                        'upload_path' => './uploads/events/images/',
                        'allowed_types' => 'jpeg|jpg|png',
                        'encrypt_name' => true,
                        'max_width' => 884,
                        'max_height' => 215,
                        );             
            $ticket_footer = array(
                        'upload_path' => './uploads/events/images/',
                        'allowed_types' => 'jpeg|jpg|png',
                        'encrypt_name' => true,
                        'max_width' => 2500,
                        'max_height' => 280,
                        ); 
				
                $this->upload->initialize($upload_card);
                $this->upload->do_upload('icard_header_img');
                $icard = (object)$this->upload->data(); 
                $icard = $icard->full_path;        

                $this->upload->initialize($ticket_header);
                $this->upload->do_upload('ticket_logo');
                $ticket_header = (object)$this->upload->data(); 
                $ticket_header = $ticket_header->full_path;    

                $this->upload->initialize($ticket_footer);
                $this->upload->do_upload('ticket_logo_footer');
                $ticket_footer = (object)$this->upload->data(); 
                $ticket_footer = $ticket_footer->full_path;
				
    		$this->eventM->add_event($icard,$ticket_header,$ticket_footer);
    		$this->notify->success('Event has been added!');
    		redirect('user/events');
    	}

    }

    public function ajax_organizers()
    {
        $term = $this->input->get('q');
        echo $this->eventM->ajax_organizers($term);

    }

    public function edit($eid){
        if(!$this->ion_auth->in_group(array(1,11))){
            show_404();
        }
        $this->template->add_js('assets/js/moment.js');
        $this->template->add_js('assets/js/bootstrap-datetimepicker.min.js');
        $this->template->add_js('assets/js/bootstrap.js');
        $this->template->add_js('assets/plugins/bootstrap3-wysiwyg/js/bootstrap3-wysihtml5.all.min.js');
        $this->template->add_js('assets/js/select2.min.js');
        $this->template->add_js('assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js');
        $this->template->add_css('assets/plugins/bootstrap3-wysiwyg/css/bootstrap3-wysihtml5.min.css');
        $this->template->add_css('assets/css/select2.min.css');
        $this->template->add_css('assets/plugins/bootstrap3-editable/css/bootstrap-editable.css');
        $this->template->add_css('assets/css/bootstrap-datetimepicker.min.css');
    	$this->template->add_css('assets/css/bootstrap-datetimepicker.min.css');
    	$data['user'] = $this->ion_auth->user()->row();
    	$data['event'] = $this->eventM->get_event($eid,TRUE);
        $event = $data['event'];
        $org = $this->eventM->organizerDetails($event['event_organizerID']);
    	if(!$data['event']){
    		$this->notify->error('Invalid Event');
    		redirect('user/events');
    	}
          $this->template->add_js("$(function () {
                $('#datetimepicker1').datetimepicker({
                     format: 'YYYY-M-D hh:mm'
                });
            });",'embed');
        $this->template->add_js('$(document).ready(function(){$(\'.wysihtml5\').wysihtml5({
  toolbar: {
    "font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
    "emphasis": true, //Italics, bold, etc. Default true
    "lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
    "html": false, //Button which allows you to edit the generated HTML. Default false
    "link": true, //Button to insert a link. Default true
    "image": true, //Button to insert an image. Default true,
    "color": false, //Button to change color of font  
    "blockquote": true, //Blockquote  
    "size": "sm" //default: none, other options are xs, sm, lg
  }
});

$( ".event_org" ).select2({        
            ajax: {
                url: baseUrl+"event/ajax_organizers",
                dataType: "json",
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term // search term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },

            minimumInputLength: 2,
            data: 
            [
            {
                id: \''. $event['event_organizerID'].'\',
                text: \''.$org['name'].' - '.$org['address'].' - '.$org['email'].'\'
            },
            ]
        });
    });
    $(document).ready(function() {
        $(\'.session_plat\').editable({
                success: function(response, newValue) {
        if(response.status == \'error\') return response.msg; //msg will be shown in editable form
    }
        });
    });
','embed');
    	$data['e_types'] = $this->eventM->event_types();
    	$data['event_platforms'] = $this->eventM->event_platforms($eid);
        $data['fee_tickets'] = $this->feeM->tickets_types();
        $this->form_validation->set_rules('event_name','Event Name','required|xss_clean|trim|min_length[4]|max_length[65]');
        $this->form_validation->set_rules('event_date','Event Date','required|xss_clean|trim');
        $this->form_validation->set_rules('event_desc','Event Description','required|xss_clean|trim');
        $this->form_validation->set_rules('event_members','Event Members','required|xss_clean|trim|is_natural_no_zero');
        $this->form_validation->set_rules('event_type','Event Type','required|xss_clean|trim|callback_validtype');
        $this->form_validation->set_rules('event_location','Venue','required|xss_clean');
        $this->form_validation->set_rules('event_ticket_extra_info','Ticket Info','required|xss_clean');
        $this->form_validation->set_rules('event_org','Organizer','required|xss_clean');
        /*$this->form_validation->set_rules('icard_header_img', 'ID Card Logo', 'file_required|file_allowed_type[image]');
        $this->form_validation->set_rules('ticket_logo', 'Ticket Header Logo', 'file_required|file_allowed_type[image]');
        $this->form_validation->set_rules('ticket_logo_footer', 'Ticket Footer Image', 'file_required|file_allowed_type[image]');*/
        if($this->form_validation->run()==FALSE){
            $this->notify->error(validation_errors());
            $this->template->write('title', 'Edit Event')
                ->write_view('content', 'a_event/edit',$data)
                ->render();  
        }else{
            /*$upload_card = array(
                        'upload_path' => './uploads/events/images/',
                        'allowed_types' => 'jpeg|jpg|png',
                        'encrypt_name' => true,
                        'max_width' => 1549,
                        'max_height' => 339,
                        );             
            $ticket_header = array(
                        'upload_path' => './uploads/events/images/',
                        'allowed_types' => 'jpeg|jpg|png',
                        'encrypt_name' => true,
                        'max_width' => 884,
                        'max_height' => 215,
                        );             
            $ticket_footer = array(
                        'upload_path' => './uploads/events/images/',
                        'allowed_types' => 'jpeg|jpg|png',
                        'encrypt_name' => true,
                        'max_width' => 2500,
                        'max_height' => 280,
                        ); 
                $this->upload->initialize($upload_card);
                $this->upload->do_upload('icard_header_img');
                $icard = (object)$this->upload->data(); 
                $icard = $icard->full_path;        

                $this->upload->initialize($ticket_header);
                $this->upload->do_upload('ticket_logo');
                $ticket_header = (object)$this->upload->data(); 
                $ticket_header = $ticket_header->full_path;    

                $this->upload->initialize($ticket_footer);
                $this->upload->do_upload('ticket_logo_footer');
                $ticket_footer = (object)$this->upload->data(); 
                $ticket_footer = $ticket_footer->full_path;  */
                $this->eventM->edit_event($eid);
                $this->notify->success('Event updated!');
                redirect('user/events');        
        }

    }
    public function update_ajax(){
        if(!$this->input->is_ajax_request() || !$this->ion_auth->in_group(array(1,10))){
            show_404(); 
        }
        $id = $this->input->post('pk');
        $value = $this->input->post('value');
        $type = $this->input->post('name');

        switch($type){

            case 'session_rn':
            $data = array(
                'name' => htmlspecialchars($value),
                'updated_at' => time(),
                );
            $this->db->where('PID',$id);
            $action = $this->db->update('events_platforms',$data);
            break;

            default:

            break;
        }
        if($action){
            echo 'success';
        }else{
            echo 'error';
        }
    }
    public function event_ticket($uid=0,$eid=0,$ref_id=0,$indi=0,$hash=''){
        $this->template->set_template('print');
        if($indi==1){
            $user_id = $this->userM->user_profile(0,true,0,$hash);
            if(!$user_id){
                $this->notify->error('Invalid action attempted.');
                redirect('user/dashboard');      
            }
            $uid = $this->ion_auth->user($user_id['UID'])->row();
        }else{
            $uid = $this->ion_auth->user()->row();       
        }
        $data['event'] = $this->eventM->get_event($eid,TRUE);
        $data['members']  = $this->userM->get_members($eid,$uid->id);
        $data['uid_basics']  = $this->userM->user_profile($uid->ref_id,true,$uid->id);
        $data['base_url'] =  base_url();
        if(!$data['event']){
            $this->notify->error('Invalid event.');
            redirect('user/dashboard');
        }
        if(!$data['members']){
            $this->notify->error('No members added yet.');
            redirect('user/dashboard');
        }
        if(!$data['uid_basics']){
            $this->notify->error('Invalid action attempted.');
            redirect('user/dashboard');
        }
        if(!$uid->id){
            $this->notify->error('Invalid action attemped');
            redirect('user/dashboard');
        }
        if(!$ref_id){
            $this->notify->error('Invalid action attemped');
            redirect('user/dashboard');           
        }
        $this->template->add_css('assets/css/ticket.css');
        $this->template->add_css('assets/css/jumbotron-narrow.css');
        $this->template->write('title', 'Event Ticket')
                ->write_view('content', 'a_event/ticket',$data)
                ->render();
    }

    public function event_organizers(){
        if(!$this->ion_auth->in_group(array(1,13))){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        $this->form_validation->set_rules('org_name','Name','required');
        $this->form_validation->set_rules('org_address','Address','required');
        $this->form_validation->set_rules('org_contact','Contact','required');
        $this->form_validation->set_rules('org_email','Email','required|valid_email');
        if($this->form_validation->run()){
            $this->notify->success('Event organizer added.');
            $this->eventM->add_organizer();
            redirect('event/event_organizers','refresh');
        }else{
            if(validation_errors()):
            $this->notify->error(validation_errors());
        endif;
            $this->template->write('title', 'Add Event Organizer')
                ->write_view('content', 'a_event/add_organizer',$data)
                ->render();           
        }

    }

    public function view_organizers(){
        if(!$this->ion_auth->in_group(array(1,13))){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        $data['organizers'] = $this->eventM->organizerDetails();
        $this->template->write('title', 'View Organizers')
                ->write_view('content', 'a_event/view_organizers',$data)
                ->render();     
    }
    public function add_membersAdmin($eid){
        if(!$this->ion_auth->in_group(array(1,10,11,13))){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        $data['EID'] = $eid;
        $this->template->write('title', 'Add Extra Members')
                ->write_view('content', 'a_event/add_membersAdmin',$data)
                ->render();       
    }

    public function add_membersAdminAjax(){
        if(!$this->ion_auth->in_group(array(1,10,11,13))){
            show_404();
        }
        $type = $this->input->post('get_option');
        switch($type){
            case 'registered':
            $data['users'] = $this->ion_auth->users()->result();
            $this->load->view('a_event/ajax/registered_user',$data);
            break;            
            case 'unregistered':
            $data['eid'] = (int)$this->input->post('EID',TRUE);
            $this->load->view('a_event/ajax/unregistered_user',$data);
            break;
        }

    }
    public function delete($eid){
        if(!$this->ion_auth->in_group(array(1,11))){
            show_404();
        }
    	$data['events'] = $this->frontM->get_events('WHERE status=1 AND EID='.$eid);
    	if(!$data['events']){
    		$this->notify->error('Invalid Event');
    		redirect('user/events');
    	}
    	$this->eventM->deleteEvent($eid);
    	$this->notify->success('Event id: '.$eid.' deleted.');
    	redirect('user/events');
    }

    public function icards(){
        if(!$this->ion_auth->in_group(array(1,12))){
            show_404();
        }
    	$data['user'] = $this->ion_auth->user()->row();
    	$data['events'] = $this->frontM->get_events('WHERE status=1 ORDER BY EID DESC');
    	$data['recents'] = $this->userM->get_events('WHERE deleted=0');
    	$this->template->write('title', 'Icards :: Events')
    	->write_view('content', 'a_event/icards_list',$data)
    	->render();
    }

    public function icards_users($eid){
        if(!$this->ion_auth->in_group(array(1,12))){
            show_404();
        }
    	$data['user'] = $this->ion_auth->user()->row();
    	$data['users'] = $this->eventM->get_usersEvent($eid);
    	$data['events'] = $this->frontM->get_events('WHERE status=1 AND EID='.$eid.' ORDER BY EID DESC');
        $data['eid'] =  $eid;
    	$this->template->write('title', 'Icards :: Users')
    	->write_view('content', 'a_event/icards_users',$data)
    	->render();
    }

    public function icards_user($uid,$eid){
        if(!$this->ion_auth->in_group(array(1,12))){
            show_404();
        }
        $this->template->add_js('assets/plugins/bootstrap3-editable/js/bootstrap-editable.min.js');
        $this->template->add_css('assets/plugins/bootstrap3-editable/css/bootstrap-editable.css');
        $this->template->add_js('assets/js/confirm-bootstrap.js');
        $this->template->add_js('  if($(\'[data-toggle="confirm-modal"]\').length > 0) {
    $(\'[data-toggle="confirm-modal"]\').confirmModal();
  }','embed');

        $this->template->add_js('    $(document).ready(function() {
        $(\'.att_name\').editable();
            $.post(baseUrl+\'event/ajax_sessions/'.$eid.'\', \'\', function (res) {
        $(\'.att_sess\').editable({
            emptytext: \'Session\',
            type: \'select\',
            title: \'Select Session\',
            source: res
        });
    });
    });','embed');
    	$data['user'] = $this->ion_auth->user()->row();
    	$data['members'] = $this->userM->get_members($eid,$uid);
        $data['eid'] = $eid;
        $data['already'] = $this->userM->already_members($eid,$data['user']->id);
        $data['platforms'] = $this->userM->event_platforms($eid);
        $user = $this->ion_auth->user($uid)->row();
        $data['uid_basics']  = $this->userM->user_profile($user->ref_id,true,$user->id);

        $get_inputs = $this->db->query("SELECT participants FROM events WHERE EID='$eid'");
        $get_inputs =  $get_inputs->row_array();
        $t_query =  $this->db->query("SELECT COUNT(MID) AS cnt FROM events_members WHERE EID='$eid' AND UID='$uid' AND deleted=0 ORDER BY MID ASC");
        $data['test'] = $t_query->row_array();
    	$this->template->write('title', 'Icards :: Members')
    	->write_view('content', 'a_event/icards_members',$data)
    	->render();
    }

    public function delete_member($mid=0,$eid=0,$uid=0){
        if(!$this->ion_auth->in_group(array(1,12))){
            show_404();
        }       

        $this->eventM->delete_member($mid,$eid,$uid);
        $this->notify->success('Member removed successfully.');
        redirect('event/icard/user/'.$uid.'/'.$eid);
    }
    public function ajax_sessions($eid){
        $platforms = $this->userM->event_platforms($eid);
        foreach ($platforms as $platform) {
            $data[] =array(
                    'value'=> $platform->PID,
                    'text' => htmlspecialchars_decode($platform->name),
                    );
                
        }   
        print_r(json_encode($data));
   
    }
    public function icard_ajax_edit($type=''){
        if(!$this->ion_auth->in_group(array(1,12))){
            show_404();
        }
        $id = $this->input->post('pk');
        $value = $this->input->post('value',true);
        switch($type){

            case 'member_name':
            $data = array(
                'name' => $value,
                'updated_at' => time(),
                );
            $this->db->where('MID',$id);
            $this->db->update('events_members',$data);
            break;

            case 'member_session':
            $data = array(
                'platform' => $value,
                'updated_at' => time(),
                );
            $this->db->where('MID',$id);
            $this->db->update('events_members',$data);
            break;
        }
    }

    public function icard_member($uid,$eid,$mid){
        if(!$this->ion_auth->in_group(array(1,12))){
            show_404();
        }
        $this->template->set_template('print');
        $uid = $this->ion_auth->user($uid)->row();
        $this->template->add_css('assets/css/print.id.css');
        $data['member'] = $this->eventM->member_details($uid->id,$eid,$mid);
        $data['uid_basics']  = $this->userM->user_profile($uid->ref_id,true,$uid->id);
        $event = $this->eventM->get_usersEvent($eid);
        if(!$event){
            $this->notify->error('Invalid event');
            redirect('event/icards');            
        }
        if(!$data['member']){
            $this->notify->error('Invalid member ID');
            redirect('event/icard/user/'.$uid->id.'/'.$eid);
        }
        $this->template->write('title', 'Icard :: Member')
        ->write_view('content', 'a_event/icards_members_single',$data)
        ->render();
    }

    public function icard_batch($uid=0,$eid=0){
        if(!$this->ion_auth->in_group(array(1,12))){
            show_404();
        }
        $this->template->set_template('print');
        $this->template->add_css('assets/css/print.id.css');
        $uid = $this->ion_auth->user($uid)->row();
        $data['uid_basics']  = $this->userM->user_profile($uid->ref_id,true,$uid->id);
        $data['members'] = $this->userM->get_members($eid,$uid->id);
        $event = $this->eventM->get_usersEvent($eid);
        if(!$event){
            $this->notify->error('Invalid event');
            redirect('event/icards');            
        }
        $this->template->write('title', 'Icard :: Batch')
        ->write_view('content', 'a_event/icards_members_batch',$data)
        ->render();
    }

    public function attendee_list(){
        if(!$this->ion_auth->in_group(array(1,10))){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        $data['events']  = $this->frontM->get_events('WHERE status=1 ORDER BY date_epoch DESC');
        $this->template->write('title', 'Attendee List')
        ->write_view('content', 'a_event/attendee_list',$data)
        ->render();
    }
    public function attendee_listPrint($eid,$session='all',$gender='all',$checked_in='all',$user='all'){
        if(!$this->ion_auth->in_group(array(1,10))){
            show_404();
        }
        $this->template->set_template('print');
        $data['attendees'] = $this->eventM->attendee_list($eid, $session, $gender,$checked_in,$user);
        $data['event'] = $this->eventM->get_event($eid,true);
        $data['session'] = $session;
        $data['gender'] = $gender;
        $data['checked_in'] = $checked_in;
        $data['p_user'] = $user;
        $this->template->write('title', 'Attendee List :: ')
        ->write_view('content', 'a_event/attendee_list_print',$data)
        ->render();
    }

    public function ajax_UserSession(){
        if(!$this->ion_auth->in_group(array(1,10))){
            show_404();
        }
        $eid = $this->input->post('EID');
        $platforms = $this->eventM->event_platforms($eid);
        $users = $this->eventM->get_usersEvent($eid);
        echo '<div class="col-md-2">
                <select class="form-control" id="session">
                <option value="all">All Sessions</option>';
        foreach($platforms as $platform){
            echo '<option value="'.$platform->PID.'">'.$platform->name.'</option>';
        }
        echo '</select></div>';
        echo '<div class="col-md-2">
                <select class="form-control" id="usr">
                <option value="all">All Users</option>';
                if(!$users)
                    echo '<option value="0">No Registrations</option>';
        foreach($users as $user){
            $uid = $this->ion_auth->user($user->UID)->row();
            $basics = $this->userM->user_profile($uid->ref_id,true,$uid->id);
            echo '<option value="'.$uid->id.'">'.$basics['full_name'].' - '.$basics['t_org'].'</option>';
        }
        echo '</select></div>';    
    }

    public function member_checkin(){
        if(!$this->input->is_ajax_request() || !$this->ion_auth->in_group(array(1,12))){
            show_404(); 
        }
        $MID = $this->input->post('mid');
        $EID = $this->input->post('eid');
        $type = $this->input->post('t');
        switch($type){

            case 'checkin':
            $this->eventM->member_checkin($EID,$MID,$type);
            $message = 'Checked in at: '.unix_to_human(time());
            break;

            case 'uncheckin':
            $this->eventM->member_checkin($EID,$MID,$type);
            $message = '';
            break;
        }
        echo $message;

    }

    public function sessions($eid){
        if(!$this->ion_auth->in_group(array(1,10))){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        $data['platforms'] = $this->eventM->event_platforms($eid);
        $data['fee_tickets'] = $this->feeM->tickets_types();
        $this->session->set_userdata('eid',$eid);
         $this->template->write('title', 'Event :: Sessions')
        ->write_view('content', 'a_event/sessions',$data)
        ->render();
    }

    public function sessions_update(){
        if(!$this->ion_auth->in_group(array(1,10))){
            show_404();
        }
        $eid = $this->session->userdata('eid');
        $platforms = $this->eventM->event_platforms($eid);
        //print_r($platforms);
        foreach($platforms as $platform){
            $this->form_validation->set_rules('ticket_id['.$platform->PID.']','Ticket for '.$platform->name,'required');
            $this->form_validation->set_rules('session_amount['.$platform->PID.']','Session Amount for'.$platform->name,'required|numeric');
            $this->form_validation->set_rules('session_attendee['.$platform->PID.']','Session Attendee Limit for '.$platform->name,'required|numeric');
        }
        if($this->form_validation->run()==FALSE){
            $this->notify->error(validation_errors());
            redirect('event/sessions/'.$eid);
        }else{
            $platforms = $this->input->post('PID');
            foreach($platforms as $platform):
            $this->eventM->insert_eventTicketsPlatform($platform,$eid);
            endforeach;
            $this->notify->success('Sessions ticket/ammount updated successfully.');
            redirect('event/sessions/'.$eid);
        }
    }

    public function dashboard($eid=0){
        if(!$this->ion_auth->in_group(array(1,10))){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        $data['platforms'] = $this->eventM->event_platforms($eid);
        $data['event'] = $this->eventM->get_event($eid,TRUE);
        if(!$data['event']){
            $this->notify->error('Invalid Event.');
            redirect('user/events');
        }
        $this->template->write('title', 'Event :: Dashboard')
        ->write_view('content', 'a_event/dashboard',$data)
        ->render();

    }
    public function validtype($str){
    	$this->db->where('ID',$str);
    	$this->db->from('events_types');
    	if($this->db->count_all_results()==1){
    		return TRUE;
    	}else{
			$this->form_validation->set_message('validtype', 'Invalid %s , what are you trying to do?!');
			return FALSE;
    	}
    }

}
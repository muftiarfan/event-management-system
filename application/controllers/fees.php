<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fees extends CI_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('frontend_model','frontM');
        $this->load->model('fees_model','feeM');
        $this->load->model('event_model','eventM');
        $this->load->model('user_model','userM');
        $this->load->library('form_validation');
        		$this->form_validation->set_error_delimiters('', '<br/>');
		if ($this->ion_auth->logged_in() && !$this->ion_auth->in_group(array(1,8,9)))
		{
			// redirect them to the login page
			redirect('user/dashboard');
		}
		$this->template->set_template('user');
    }

    public function index(){

    }

    public function add(){
    	$data['user'] = $this->ion_auth->user()->row();
    	$data['events']  = $this->frontM->get_events('WHERE status=1 ORDER BY date_epoch DESC');
    	$this->template->write('title', 'Add Fee')
                       ->write_view('content', 'a_fee/add',$data)
                       ->render();
    }

    public function fees_ajax(){
    	if(!$this->input->is_ajax_request()){
    		show_404();	
    	}
    	$type = $this->input->post('type');
    	$ID = $this->input->post('ID');
    	switch($type){

    		case 'event_return_users':
            $this->session->set_userdata('val_EID',$ID);
    		$users = $this->eventM->get_usersEvent($ID);
    		//echo '<select id="">';
    		echo'<div class="col-lg-12">
    		<label for="events">Registered Users</label>
    		<select class="form-control" onchange="fetch_select(this.value, \'event_return_members\',\'event_return_members\');" name="event_return_users" id="event_return_users">';
    		echo '<option value="0" selected="selected" disabled="disabled">Select</option>';
    		foreach($users as $user){
    			$user_info = $this->ion_auth->user($user->UID)->row();
    			$user_profile = $this->userM->user_profile($user_info->ref_id,true,$user->UID);
    			echo '<option value="'.$user_info->id.'">'.$user_info->username.' - '.$user_profile['t_org'].' - '.$user_profile['full_name'].'</option>';
    		}
    		echo'</select>
    		</div>';
    		//echo '</select>';
    		break;

    		case 'event_return_members':
    		$eid = $this->session->userdata('val_EID');
    		$this->load->model('user_model','userM');
    		$members  = $this->userM->get_members($eid,$ID,1);
    		$fee_tickets = $this->feeM->tickets_types();
    		echo '
    		<div class="col-lg-12">
    		<div class="panel panel-success" style="margin-top:5px;">
    		<div class="panel-heading"><h4>Details <small class="text-muted">Checked In</small>	</h4></div>
    		<table class="table table-hover">
    		<thead>
    		<th class="text-left">#</th>
    		<th class="text-center">Name</th>
    		<th class="text-right">Session/Price</th>
    		</thead>
    		<tbody>';
    		$amount = 0.00;
    		if(!$members){
    			echo '<tr>
    			<td colspan="3">
    			<div class="well well-sm text-center">
    			<strong>No Attendees checked in yet, please checkin and add fees.</strong>
    			</td>
    			</tr>';
    		}
    		$i = 1;
    		foreach($members as $member):
    		$platform_name = $this->userM->idData($member->platform,$eid,'platform_name');
    		$platform_price = $this->userM->idData($member->platform,0,'session_amount');
    		$amount +=$platform_price;
    		echo'<tr>
    		<td class="text-left">'.$i++.'</td>
    		<td class="text-center">'.$member->name.'</td>
    		<td class="text-right">'.$platform_name.' / INR. <strong>'.$platform_price.'</strong></td>
    		</tr>';
    		endforeach;
    		
    		echo '
    		<tr>
    			<td class="text-left"><strong>Sessions Total</strong></td>
    			<td class="text-center"></td>
    		<td class="text-right">
    		<div class="input-group">
    		<span class="input-group-addon">INR.</span>
    		<input type="text" class="form-control" readonly="readonly" value="'.$amount.'" name="total_sessions_amount" placeholder="0.00">
    		<span class="input-group-addon">.00</span>
    		</div>
    		</td>
    		</tr>
    		<tr>
    		<td class="text-left"><strong>Ticket Type</strong></td>
    		<td class="text-center"></td>
    		<td class="text-right">
    		<select class="form-control" id="ticket_type" name="ticket_type" onchange="fetch_amount(this.value,\''.$amount.'\');">
    		<option value="0" selected="selected" disabled="disabled">Select</option>';
    		foreach($fee_tickets as $ticket):
    			echo '<option value="'.$ticket->ID.'">'.$ticket->name.' - '.$ticket->description.'</option>';
    			endforeach;
    		echo'
    		</select>
    		</td>
    		</tr>
    		</table>
    		</div>
    		</div>
    		<div class="col-lg-12">
    		<div class="panel panel-info" id="resp-amnt" style="display:none;">
    		<div class="panel-heading"><h2>Fee Details</h2></div>
    		<div class="panel-body">
    		<div id="final_amount">

    		</div>
    		</div>
    		</div>
    		<button type="submit" class="btn btn-lg btn-success">Pay</button>
    		</div>
    		';
    		break;
    	}

    }

    public function fee_amount_ajax(){

    	$ticket =  $this->input->post('ticket_type');
    	$amount = $this->input->post('amnt');
    	$custom = 0;
    	$ticket_info = $this->feeM->ticket_info($ticket);
    	//echo $ticket;
    	//echo $amount;
    	//print_r($ticket_info);
    	switch($ticket_info['method']){

    		case 'sum':
    		$final_amount = ($amount+$ticket_info['method_amount']);
    		break;

    		case 'multiply':
    		$final_amount = ($amount*$ticket_info['method_amount']);
    		break;

    		case 'reset':
    		///what it does is 
    		break;

    		case 'custom':
    		$final_amount = $amount;
    		$custom = 1;
    		break;
    	}
    		if($custom)
    		echo '
    		<div class="col-lg-12">
    		<label for="add_fees_amount"><strong>Charges</strong></label>
    		<div class="input-group">
    		<span class="input-group-addon">INR.</span>
    		<input type="text" class="form-control" value="" onchange="custom_add(this.value,\''.$amount.'\');" name="add_fees_amount" placeholder="0.00">
    		<span class="input-group-addon">.00</span>
    		</div>
    		</div>
    		<div class="col-lg-12">
    		 <label for="add_fees_amount_desc"><strong>Charges Description</strong></label>
    		<div class="form-group">
    		 <input type="text" class="form-control" name="add_fees_amount_desc" placeholder="What are these extra Charges for?" value="">
    		 <input type="hidden" name="custom_fee_yes" value="1">
    		 </div>
    		 </div>
    		';
    	echo '
    		<div class="col-lg-12">
    		<label for="total_fees_amount"><strong>Final Amount</strong></label>
    		<div class="input-group">
    		<span class="input-group-addon">INR.</span>
    		<input type="text" class="form-control" value="'.$final_amount.'" id="total_fees_amount" name="total_fees_amount" placeholder="0.00">
    		<span class="input-group-addon">.00</span>
    		</div>
    		</div>
    		';
    }

    public function add_fee(){

    	$this->form_validation->set_rules('events','Event','required');
    	$this->form_validation->set_rules('event_return_users','User','required');
    	$this->form_validation->set_rules('total_fees_amount','Total Amount','required');
    	$this->form_validation->set_rules('ticket_type','Ticket Type','required');
    	if($this->input->post('custom_fee_yes')){
    		$this->form_validation->set_rules('add_fees_amount','Custom Charges','required');
    		$this->form_validation->set_rules('add_fees_amount_desc','Custom Charges Description','required|xss_clean');
    	}
    	if($this->form_validation->run()){
    		$this->feeM->add_fee($this->input->post('events'),$this->input->post('event_return_users'),$this->input->post('total_fees_amount'),$this->input->post('ticket_type'),$this->input->post('add_fees_amount'),$this->input->post('add_fees_amount_desc'));
    		$this->notify->success('Fees Added, <a href="fees/print_receipt/'.$this->input->post('events').'/'.$this->input->post('event_return_users').'">print receipt.</a>');
    		redirect('fees/add');
    	}else{
    		$this->notify->error(validation_errors());
    		redirect('fees/add');
    	}
    }

    public function view($event='all',$ticket_type='all',$user='all'){
    	$data['user'] = $this->ion_auth->user()->row();
    	$data['payments_fee']  = $this->feeM->payments_fee($event,$ticket_type,$user);
    	$data['events'] =  $this->frontM->get_events('WHERE status=1 ORDER BY date_epoch DESC');
    	$data['tickets'] = $this->feeM->tickets_types();
    	$data['users'] = $this->ion_auth->users(array(1,9))->result();
    	$this->template->write('title', 'View Fee')
                       ->write_view('content', 'a_fee/view',$data)
                       ->render();

    }

    public function fee_detail($transID){

    	$details = $this->feeM->payments_fee('all', 'all', 'all',$transID);
    	foreach($details as $detail){
$ticket_type = $this->feeM->ticket_info($detail->ticket_id);
$reciever = $this->ion_auth->user($detail->UID)->row();
$attendee = $this->ion_auth->user($detail->event_idUser)->row();
$attendee = $this->userM->user_profile($attendee->ref_id,true,$attendee->id);
$all_members = $this->userM->get_members($detail->event_id,$detail->event_idUser,1); //checked in attendees
    		        echo '
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                 <h4 class="modal-title">Transaction #'.$detail->ID.' Summary</h4>
            </div>          <!-- /modal-header -->
            <div class="modal-body">
            <div class="row">
            <div class="col-lg-12">
            <table class="table table-bordered table-condensed">
	<thead>
		<tr>
			<th>Attendee User</th>
			<th class="text-center">Dated</th>
			<th>Ticket Type</th>
			<th class="text-right">Amount</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td><strong>'.$attendee['full_name'].'</strong></td>
			<td class="text-center">'.unix_to_human($detail->created_at).'</td>
			<td>'.$ticket_type['name'].'</td>
			<td class="text-right">'.$detail->amount.'</td>
		</tr>
	</tbody>
</table>
<table class="table table-bordered table-condensed">
	<thead>
		<tr>
			<th>Attendee</th>
			<th class="text-center">Session</th>
			<th>Session Amount</th>
		</tr>
	</thead>
	<tbody>
	';
	foreach($all_members as $member):
		$platform_name = $this->userM->idData($member->platform,$member->EID,'platform_name');
	    $platform_price = $this->userM->idData($member->platform,0,'session_amount');
		echo'
		<tr>
			<td><strong>'.$member->name.'</strong></td>
			<td class="text-center">'.$platform_name.'</td>
			<td>'.$platform_price.'</td>
		</tr>';
		endforeach;
		echo'
		<tr>
		<td colspan="1"></td>
		<td>
		Ticket Type:
		</td>
		<td>'.$ticket_type['name'].'</td>
		</tr>
		<tr>
		<td colspan="1"></td>
		<td>Ticket Method:</td>
		<td>'.$ticket_type['method'].'</td>
		</tr>';
		if($ticket_type['method']=='custom'):
		echo'<tr>
		<td colspan="1"></td>
		<td>
		Charges:
		</td>
		<td>'.$detail->custom_charges.'</td>
		</tr>';		
		echo'<tr>
		<td colspan="1"></td>
		<td>
		Description:
		</td>
		<td>'.$detail->custom_charges_desc.'</td>
		</tr>';
		else:
			echo'
		<tr>
		<td colspan="1"></td>
		<td>Ticket Method Amount:</td>
		<td>'.$ticket_type['method_amount'].'</td>
		</tr>';
		endif;
		echo'
		<tr>
		<td colspan="1"></td>
		<td>Total:</td>
		<td><strong>'.$detail->amount.'</strong></td>
		</tr>
		<tr>
		<td colspan="1"></td>
		<td>Action</td>
		<td>
		<a href="'.base_url().'fees/reciept_print/'.$detail->ID.'" target="_blank" class="btn btn-default btn-xs"><span class="fa fa-print"></span></a>
		<a href="#" class="btn btn-default btn-xs"><span class="fa fa-trash"></span></a>
		</td>
		</tr>
	</tbody>
</table>
            </div>
            </div>
            </div>
            </div>          <!-- /modal-body -->
            <div class="modal-footer">
            </div>          <!-- /modal-footer -->';
    	}
    }

    public function reciept_print($transID){
    	$this->template->set_template('print');
    	$data['trans'] =  $this->feeM->payments_fee('all', 'all', 'all',$transID);
    	$this->template->write('title', 'Fee Invoice')
                       ->write_view('content', 'a_fee/invoice',$data)
                       ->render();	
    }
}
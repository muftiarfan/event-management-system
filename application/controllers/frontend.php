<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontend extends CI_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('frontend_model','frontM');
        $this->load->model('user_model','userM');
        $this->load->model('event_model','eventM');
        $this->load->library('form_validation');
        $this->load->helper('text_helper');
        		$this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '</div>');
		if ($this->ion_auth->logged_in() && !$this->ion_auth->is_admin())
		{
			// redirect them to the login page
			redirect('user/dashboard');
		}
    }
	public function index()
	{
		$this->template->set_template('defaultv2');
		$this->template->add_css('assets/css/bootstrap.min.css');
		$this->template->add_js('assets/js/bootstrap.min.js');
		$data['null'] = '';
		$data['events'] = $this->frontM->get_events('WHERE date_epoch >=UNIX_TIMESTAMP(CURDATE()) ORDER BY event_date ASC');
		$data['event_upcoming'] = $this->frontM->get_events('WHERE date_epoch >=UNIX_TIMESTAMP(CURDATE()) AND status=1 ORDER BY EID DESC LIMIT 1');
		$event_upcoming = $data['event_upcoming'];
		foreach($event_upcoming as $upcoming):
			$this->template->add_js('
	if (jQuery.isFunction(jQuery.fn.countDown)) {
$(\'#qcEventCountDown\').countDown({
	targetDate: {
		\'day\': 	'.date('d',$upcoming->date_epoch).',
		\'month\': 	'.date('m',$upcoming->date_epoch).',
		\'year\': 	'.date('Y',$upcoming->date_epoch).',
		\'hour\': 	0,
		\'min\': 		0,
		\'sec\': 		0
	},
	omitWeeks: true
});
}','embed');
endforeach;
		$this->template->write('title', 'Home')
                       ->write_view('content', 'frontend/index',$data)
                       ->render();
	}

	public function register($eid){
		$this->template->set_template('defaultv2main');
		$this->load->library('Dob_dropdown');
		$this->template->add_js('assets/js/moment.js');
    	$this->template->add_js('assets/js/bootstrap-datetimepicker.min.js');
    	$this->template->add_js('assets/js/bootstrap.js');
    	$slug = $this->eventM->get_event($eid,true);
    	$data['event_ticket'] = 'register/'.$eid;
		$data['event_details'] = 'events/'.$slug['url_slug'];
		$data['ed_c'] = '';
		$data['et_c'] = 'current';
    	$this->template->add_js("$(function () {
                $('#datetimepicker1').datetimepicker({
                     format: 'YYYY-M-D'
                });
            });",'embed');
    	$this->template->add_css('assets/css/bootstrap-datetimepicker.min.css');
		$data['events'] = $this->frontM->get_events('WHERE status=1 AND EID='.$eid.'');
		if($data['events']){
			$check_reg = $this->eventM->reg_open($eid);
			if($check_reg==FALSE){
				show_error('Registrations for this event have already been closed.');
			}
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean|trim|max_length[64]|min_length[8]');
			$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|trim|valid_email|is_unique[users_basics.email]');
			$this->form_validation->set_rules('mobile', 'Mobile', 'required|xss_clean|trim|numeric|exact_length[10]|callback_phoneNumbervalidation');
			
			$this->form_validation->set_message('is_unique', '%s is already registered. Please login.');
			if($this->form_validation->run()==FALSE){
			$this->template->write('title', 'Get Tickets')
						     ->write('navigation',$this->load->view('frontend/navigation',$data,true))
			->write_view('content', 'frontend/register',$data)
			->render();
			}else{
				$this->frontM->front_reg(false,$eid,'cybercrew@dpssrinagar.com or +91 9419422263','cybercrew@dpssrinagar.com','CyberCrew');
				$this->load->library('sms_lib');
				$this->config->load('sms_templates');
				$this->templates = $this->config->item('sms_templates');
				$sms = str_replace('%%event%%', $slug['name'], $this->templates['registration_success']['message']);
				$this->sms_lib->queue($this->input->post('mobile'),$sms);
				$this->template->write('title', 'Get Tickets :: Thanks')
			     ->write('navigation',$this->load->view('frontend/navigation',$data,true))
				->write_view('content', 'frontend/register_success',$data)
				->render();
			}
		}else{
			show_404();
		}
	}

	public function register_indi($eid){
		$this->load->library('upload');
		$this->template->set_template('defaultv2main');
    	$slug = $this->eventM->get_event($eid,true);
    	$data['event_ticket'] = 'register/'.$eid;
		$data['event_details'] = 'events/'.$slug['url_slug'];
		$data['ed_c'] = '';
		$data['et_c'] = 'current';
		$this->load->library('Dob_dropdown');
		$this->template->add_js('assets/js/moment.js');
    	$this->template->add_js('assets/js/bootstrap-datetimepicker.min.js');
    	$this->template->add_js('assets/js/bootstrap.js');
    	$this->template->add_js("$(function () {
                $('#datetimepicker1').datetimepicker({
                     format: 'YYYY-M-D'
                });
            });",'embed');
    	$this->template->add_css('assets/css/bootstrap-datetimepicker.min.css');
		$data['events'] = $this->frontM->get_events('WHERE status=1 AND EID='.$eid.'');
		$data['sessions'] = $this->eventM->event_platforms($eid);
		if($data['events']){
			if($check_reg==FALSE){
				show_error('Registrations for this event have already been closed.');
			}
			$this->form_validation->set_rules('name', 'Name', 'required|xss_clean|trim|max_length[64]|min_length[8]');
			$this->form_validation->set_rules('t_org', 'Team/Organization', 'required|xss_clean|trim|max_length[64]|min_length[6]');
			$this->form_validation->set_rules('email', 'Email', 'required|xss_clean|trim|valid_email|is_unique[users_basics.email]');
			$this->form_validation->set_rules('dob', 'Birthday', 'xss_clean|trim');
			$this->form_validation->set_rules('mobile', 'Mobile', 'required|xss_clean|trim|numeric|exact_length[10]|callback_phoneNumbervalidation');
			$this->form_validation->set_rules('phone', 'Phone', 'required|xss_clean|trim|numeric');
			$this->form_validation->set_rules('t_org_card', 'ID Card', 'file_required|file_allowed_type[image]');
			$this->form_validation->set_rules('member_gender',  'Gender', 'required');
			
			$this->form_validation->set_message('is_unique', '%s is already registered. Please login.');
			if($this->form_validation->run()==FALSE){
			$this->template->write('title', 'Get Tickets')
			->write('navigation',$this->load->view('frontend/navigation',$data,true))
			->write_view('content', 'frontend/register_indi',$data)
			->render();
			}else{
				$upload_card = array(
                   		'upload_path' => './uploads/icards/individuals/',
                   		'allowed_types' => 'jpeg|jpg|png',
                   		'encrypt_name' => true,
                   		); 
				$this->upload->initialize($upload_card);
				$do_ = $this->upload->do_upload('t_org_card');
				$icard = (object)$this->upload->data(); 
				$icard = $upload_card['upload_path'].$icard->file_name;
				if ($do_){
					$this->frontM->front_reg(true,$eid,'cybercrew@dpssrinagar.com or +91 9419422263','cybercrew@dpssrinagar.com','CyberCrew',$icard);	
					$this->load->library('sms_lib');
					$this->config->load('sms_templates');
					$this->templates = $this->config->item('sms_templates');
					$sms = str_replace('%%event%%', $slug['name'], $this->templates['registration_success']['message']);
					$this->sms_lib->queue($this->input->post('mobile'),$sms);
					$this->template->write('title', 'Get Tickets')
					->write('navigation',$this->load->view('frontend/navigation',$data,true))
					->write_view('content', 'frontend/register_success',$data)
					->render();
				}else{
					$this->notify->error($this->upload->display_errors('',''));
					redirect('register/individual/'.$eid,'refresh');
				}
				
			}
		}else{
			show_404();
		}
	}

	public function event($slug){
		$this->template->set_template('defaultv2main');
		$this->load->model('event_model','eventM');
		$data['events'] = $this->frontM->get_events('WHERE url_slug="'.$slug.'"');
		if(!$data['events']){
			show_404();
		}
		foreach($data['events'] as $evt):
			$event = $this->eventM->get_event($evt->EID,true);
		$org = $this->eventM->organizerDetails($event['event_organizerID']);
		$data['sessions'] = $this->eventM->event_platforms($evt->EID);
		$data['event_ticket'] = 'register/'.$evt->EID;
		$data['event_details'] = 'events/'.$slug;
		$data['ed_c'] = 'current';
		$data['et_c'] = '';
		$this->template->write('title', $event['name'].' :: An event by '.$org['name'].' :: WebDegg ')
				->write('navigation',$this->load->view('frontend/navigation',$data,true))
				->write_view('content', 'frontend/event_detail',$data)
				->render();
				endforeach;

	}

	function phoneNumbervalidation($str)
	{
		if(preg_match('/^((\+){0,1}91(\s){0,1}(\-){0,1}(\s){0,1})?([0-9]{10})$/', '91'.$str,$matches)){
			return true;
		}
		else{
			$this->form_validation->set_message('phoneNumbervalidation', 'Invalid mobile number.');
			return false;
		}
	}
}

/* End of file frontend.php */
/* Location: ./application/controllers/frontend.php */
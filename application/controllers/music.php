<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Music extends CI_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('user_model','userM');
        $this->load->model('music_model','musicM');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '</div>');
        if(!$this->ion_auth->logged_in()){
        	$this->notify->warning('Please login to proceed.');
        	redirect('auth/login');
        }
        $this->template->set_template('user');
        $this->userM->user_basics();
    }

    public function index(){
        if(!$this->ion_auth->is_admin()){
            show_404();
        }
    }

    public function dashboard(){
        if(!$this->ion_auth->is_admin()){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        $data['c_total'] = $this->musicM->stats('total');
        $data['c_ototal'] = $this->musicM->stats('total_orders');
        $data['c_ototalc'] = $this->musicM->stats('total_orders_c');
        $data['last_order'] = $this->musicM->stats('last_order');
        $this->template->write('title', 'Music :: Dashboard')
        ->write_view('content', 'a_music/dashboard',$data)
        ->render();
    }

    public function add(){
        if(!$this->ion_auth->is_admin()){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        $this->form_validation->set_rules('title','Title','required|xss_clean|trim');
        $this->form_validation->set_rules('artist','Artist','required|xss_clean|trim');
        $this->form_validation->set_rules('album','Album','xss_clean|trim');
        $this->form_validation->set_rules('year','Year','xss_clean|trim');
        if($this->form_validation->run()==FALSE){
            $this->template->write('title', 'Music :: Add')
            ->write_view('content', 'a_music/add',$data)
            ->render();
        }else{
            $this->musicM->add();
            $this->notify->success('Song Added!');
            redirect('music');
        }

    }
    public function songs($type=null,$entity=null){
        $groups = array(1,4,7);
        if(!$this->ion_auth->in_group($groups)){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        switch($type){
            case 'artist':
            $where = 'WHERE artist = "'.htmlspecialchars_decode($entity).'"';
            break;

            case 'album':
            $where = 'WHERE album = "'.htmlspecialchars_decode($entity).'"';
            break;

            case 'year':
            $where = 'WHERE year = "'.(int)$entity.'"';
            break;

            default:
            $where = '';
            break;
        }
        $data['songs'] = $this->musicM->listing($where);
        if(!$data['songs']){
            $this->notify->error('Something went wrong.');
            redirect('music/add');
        }
        $this->template->write('title', 'Music :: Listing :: '.htmlspecialchars($type).' '.htmlspecialchars($entity))
        ->write_view('content', 'a_music/list',$data)
        ->render();
    }

    public function ajax_songs()
    {
        $term = $this->input->get('q');
        echo $this->musicM->ajax_orders($term);

    }
    public function order(){
        $groups = array(1,3);
        if(!$this->ion_auth->in_group($groups)){
            show_404();
        }
        $this->template->add_css('assets/css/select2.min.css');
        $this->template->add_js('assets/js/jquery-migrate-1.2.1.min.js');
        $this->template->add_js('assets/js/select2.min.js');
        $this->template->add_js('$( ".song" ).select2({        
            ajax: {
                url: "'.base_url().'music/ajax_songs",
                dataType: "json",
                delay: 250,
                data: function (params) {
                    return {
                        q: params.term // search term
                    };
                },
                processResults: function (data) {
                    return {
                        results: data
                    };
                },
                cache: true
            },
            minimumInputLength: 2
        });','embed');

        $data['user'] = $this->ion_auth->user()->row();
        $data['songs'] = $this->musicM->listing('WHERE status=1');
        $data['price'] = $this->musicM->get_varVal('music_order_price');
        $this->form_validation->set_rules('song','Song','required|xss_clean|is_natural_no_zero');
        $this->form_validation->set_rules('from','From','xss_clean');
        if($this->input->post('from',TRUE)!=''){
        $this->form_validation->set_rules('to','To','xss_clean|required');
        }
        if($this->form_validation->run()==FALSE){
            $this->template->write('title', 'Music :: Add Order')
            ->write_view('content', 'a_music/add_order',$data)
            ->render();          
        }else{
            $this->musicM->add_order();
            $this->notify->success('Song qued in order');
            redirect('music/order');
        }
    
    }    

    public function orders($type=null,$entity=null){
        $groups = array(1,3);
        if(!$this->ion_auth->in_group($groups)){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        switch($type){

            case 'song':
            $where = 'WHERE SID = '.(int)$entity.'';
            break;

            case 'unplayed':
            $where = 'WHERE played_at=0';
            break;
            
            case 'played':
            $where = 'WHERE played_at<>0';
            break;

            default:
            $where = '';
            break;

        }
        $data['songs'] = $this->musicM->listing_orders($where);
        $this->template->write('title', 'Music :: Orders :: '.$type.'')
            ->write_view('content', 'a_music/list_orders',$data)
            ->render(); 
        }

        public function settings(){
        if($this->ion_auth->is_admin()){
            $data['user'] = $this->ion_auth->user()->row();
            $data['price'] = $this->musicM->get_varVal('music_order_price');
            $this->form_validation->set_rules('music_order_price','Price','required|xss_clean|is_natural_no_zero');
            if($this->form_validation->run()==FALSE){
                $this->template->write('title', 'Music :: Settings')
                ->write_view('content', 'a_music/settings',$data)
                ->render();   
            }else{
                $posts = $this->input->post();
                foreach($posts as $key=>$value){
                    $this->musicM->update_variables($key,$value);
                 //   echo $key.$value;
                }
                $this->notify->success('Music system variables updated!');
                redirect('music/settings');
            }

        }else{
            show_404();
        }  
        }

        public function played($sid){
            $this->musicM->played($sid);
            $this->notify->success('Song '.$sid.' marked as played');
            redirect('music/orders');
        }

        public function view_lcd(){
        if(!$this->ion_auth->is_admin()){
                show_404();
        }
        $this->template->set_template('print');
        $data['songs'] = $this->musicM->listing2('WHERE status=1');
        $data['order_now'] = $this->musicM->listing_orders2('WHERE played_at<>0 ORDER BY ID DESC LIMIT 1');
        $data['trending'] = $this->musicM->listing2('WHERE status=1 ORDER BY `orders` DESC LIMIT 3');
        $this->template->write('title', 'Music :: Trends')
                ->write_view('content', 'a_music/trends',$data)
                ->render(); 
        }
        public function print_songs(){
            if(!$this->ion_auth->is_admin()){
                show_404();
            }
            $this->template->set_template('print');
            $data['songs'] = $this->musicM->listing('WHERE status=1');
               $this->template->write('title', 'Music :: Print')
                ->write_view('content', 'a_music/print_songs',$data)
                ->render();   
        }
}
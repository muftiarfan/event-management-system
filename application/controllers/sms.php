<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
use EMS\libraries\Sms_recipient;
class Sms extends CI_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('user_model','userM');
        $this->load->model('frontend_model','frontM');
        $this->load->model('sms_model','smsM');
        $this->load->library('form_validation');
        $this->load->library('sms_lib');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '</div>');
        $this->template->set_template('user');
        //$this->userM->user_basics();
    }

    public function index(){
        //$this->dashboard();
        $this->sms_lib->execute();
    }

    public function dashboard(){
                if(!$this->ion_auth->logged_in()){
            $this->notify->warning('Please login to proceed.');
            redirect('auth/login');
        }
        if(!$this->ion_auth->in_group(array(1,17,18))){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        $data['c_total'] = $this->smsM->counts('total');
        $data['c_qtotal'] = $this->smsM->counts('queue');
        $data['c_todaytotal'] = $this->smsM->counts('today');
        $data['last_activity'] = $this->smsM->counts('last_activity');
        $this->template->write('title', 'SMS :: Dashboard')
                ->write_view('content', 'a_sms/dashboard',$data)
                ->render();
    }

    public function sent(){
      if(!$this->ion_auth->in_group(array(1,17,18))){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        $data['smses'] = $this->smsM->smses('WHERE status=1');
        $this->template->write('title', 'SMS :: Sent')
                ->write_view('content', 'a_sms/list',$data)
                ->render();
    }

    public function queue(){
        if(!$this->ion_auth->in_group(array(1,17,18))){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        $data['smses'] = $this->smsM->smses('WHERE status=0');
        $this->template->write('title', 'SMS :: Sent')
                ->write_view('content', 'a_sms/list',$data)
                ->render();
    }

    public function compose(){
        if(!$this->ion_auth->in_group(array(1,17,18))){
            show_404();
        }
        $data['user'] = $this->ion_auth->user()->row();
        //$this->template->add_js('assets/js/select2.full.min.js');
        $this->template->add_js('assets/js/select2.3.min.js');
        //$this->template->add_css('assets/css/select2.min.css');
        $this->template->add_css('assets/css/select2.3.2.css');
        $this->template->add_js("  if($('select.select2').length > 0) {
    $('select.select2').select2();
  }

  if($('.tag').length > 0) {
    $('.tag').select2({tags:[]});
  }

  //Select2 for message->to field
  if($('#recipients.jsonlist').length) {
    $('#recipients.jsonlist').select2({
      placeholder: \"Enter recepient...\",
      allowClear: true,
      multiple: true,
      data: recipients,
      formatResult: function(item) {
        return item.display;
      }
    });
  }",'embed');
       $this->template->add_js("$('.ajaxlist').each(function() {
    var thisEl = $(this);
    $(this).select2({
      
      placeholder: 'Enter Recipient',
      minimumInputLength: 2,
      multiple : !($(this).attr('multiple') === 'undefined'),
      ajax: {
          url: baseUrl + 'sms/recipients',
          dataType: 'json',

          data: function (term) { 
              return {
                  q: $.trim(term), //search term
              };
          },
          results: function (data) {
              return {results: data};
          }
      },
      formatResult: function(item) {
        return item.display;
      }, // omitted for brevity, see the source of this page
    });
  });",'embed');
        $data['gateways'] = $this->smsM->senderApi('result');
        $this->template->write('title', 'SMS :: Compose')
                ->write_view('content', 'a_sms/compose',$data)
                ->render();
      
    }

    public function composeA(){
      if(!$this->ion_auth->in_group(array(1,17,18))){
            show_404();
        }
        $inputrecipients = $this->input->post('recipients');
        echo $inputrecipients;
        $gateway_id = $this->smsM->senderApi();
            $gateway_id = $gateway_id['gateway_id'];
        $recipients = sms_recipient::with()
                ->parse($inputrecipients)
                ->flatten()
                ;
            $recipients = array_filter($recipients, function ($item) {
                if(isMobileNo($item)) return true;

                return false;
            });

            $unfilteredCount = count($recipients);
            $skipped = $unfilteredCount - count($recipients);
            $queued = count($recipients);
            $messages = array();
            foreach ($recipients as $recipient) {
                $messages[] = array(
                    'recipient' => $recipient,
                    'message'   => $this->input->post('message'),
                    'gateway_id'=> $gateway_id,
                    'scheduled' => time(),
                    'created'   => time(),
                    
                );
            }
            $messages = array_chunk($messages, 200);
            foreach($messages as $chunk) {
             //print_r($chunk);
                $this->smsM->insertSMS($chunk);
            }
            $this->notify->success($queued.' SMS queued. ');
            redirect('sms/compose');

    }
    public function recipients()
    {
      if(!$this->ion_auth->in_group(array(1,17,18))){
            show_404();
        }
        //error_reporting(0);
        $data = array();
        $data = sms_recipient::find($this->input->get('q',true))->select2Json();

        print $data;
    }
}
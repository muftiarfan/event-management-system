<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('user_model','userM');
        $this->load->model('frontend_model','frontM');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '</div>');
        if(!$this->ion_auth->logged_in()){
        	$this->notify->warning('Please login to proceed.');
        	redirect('auth/login');
        }
        $this->template->set_template('user');
        $this->userM->user_basics(); //checks for mobile num verificaiton
    }

    public function index(){
    	$this->dashboard();
    }

    public function dashboard(){
    	$data['user'] = $this->ion_auth->user()->row();
    	$data['events_reg']  = $this->userM->get_events('WHERE UID='.$data['user']->id.' AND deleted=0');
    	$data['events_reg2']  = $this->userM->get_events('WHERE UID='.$data['user']->id.' AND deleted=0 ORDER BY MID DESC LIMIT 1');
    	$this->template->write('title', 'Dashboard')
				->write_view('content', 'user/dashboard',$data)
				->render();
    }

    public function events(){

    	$data['user'] = $this->ion_auth->user()->row();
    	$data['events']  = $this->frontM->get_events('WHERE status=1 ORDER BY date_epoch DESC');
    	$this->template->write('title', 'Events')
				->write_view('content', 'user/events',$data)
				->render();
    }

    public function event_register($eid){
    	$user = $this->ion_auth->user()->row();
    	$check = $this->userM->idData($eid,0,'event_passed');
    	if($check==FALSE){
    		$this->notify->info('The event has already passed.!');
    		redirect('user/events');
    	}
    	$data['user'] = $this->ion_auth->user()->row();
    	$data['events']  = $this->frontM->get_events('WHERE status=1 AND EID='.$eid.'');
    	if(!$data['events']){
    		$this->notify->error('Invalid event.');
    		redirect('user/events');
    	}
        $profile = $this->userM->user_profile($user->ref_id,true,$user->id);
        //if($profile[''])
        /*if($profile['mobile_status']==0){
            $this->notify->error('Please verify your number before adding attendees for the event.');
            redirect('user/verify_mobile');
        }*/
        if($profile['occupation']==NULL || $profile['upto_class']==NULL || $profile['address']==NULL || $profile['district']==NULL || $profile['t_org']==NULL){
            $this->notify->warning('Please complete your profile before adding attendees for the event.');
            redirect('auth/edit_user/'.$data['user']->id);
        }
        $alternate_user = 0;
        if($this->input->post('admin_add')==1){
            $alternate_user = $this->userM->user_profile(0,true,0,$this->input->post('UID_hash'));
            $alternate_user = $alternate_user['UID'];
        }
    	$data['already'] = $this->userM->already_members($eid,$data['user']->id);
    	$data['platforms'] = $this->userM->event_platforms($eid);
    	$get_inputs = $this->db->query("SELECT participants FROM events WHERE EID='$eid'");
    	$get_inputs =  $get_inputs->row_array();
    	$t_query =  $this->db->query("SELECT COUNT(MID) AS cnt FROM events_members WHERE EID='$eid' AND UID='$user->id' AND deleted=0 ORDER BY MID ASC LIMIT $get_inputs[participants]");
    	$data['test'] = $t_query->row_array();
    	if($data['test']['cnt']<0){ //for incriminting purposes.
    		$i=0;
    	}else{
    		$i=$data['test']['cnt'];
    	}
        if($this->input->post('admin_add')==1){
            $participants_get = 20;
        }else{
            $participants_get = $get_inputs['participants'];
        }
        if($profile['individual']==1 && !$this->ion_auth->is_admin() && $data['already']>=1){
            $this->notify->warning('You can not add more attendees from an individual account');
            redirect('user/event_edit/'.$eid);
        }
    	while($i<$participants_get):
    		$i = $i+1;
    		$this->form_validation->set_rules("member[$i]","Name of member $i",'xss_clean|trim');
    		$this->form_validation->set_rules("member_email[$i]","Email of member $i",'xss_clean|trim');
    		$this->form_validation->set_rules("member_phone[$i]","Phone of member $i",'xss_clean|trim');
    		$this->form_validation->set_rules("platform[$i]","Platform of member $i",'xss_clean|trim');
    	endwhile;
    	if($this->form_validation->run()==FALSE){
    		$this->template->write('title', 'Register Event')
				->write_view('content', 'user/event_register',$data)
				->render();	
    	}else{
    		//echo 'Hello';
    		$this->userM->add_members($eid,$alternate_user);
    		redirect(referer_url());
    	}
    	if($data['already']==$get_inputs['participants']){
    		$this->notify->info('Already added maximum members, you may update your entries.');
    		redirect('user/event_edit/'.$eid);
    	}
    }

    public function event_edit($eid){
    	$user = $this->ion_auth->user()->row();
    	$check = $this->userM->idData($eid,0,'event_passed');
    	if($check==FALSE){
    		$this->notify->info('This event has already passed.!');
    		redirect('user/events');
    	}
    	$data['user'] = $this->ion_auth->user()->row();
    	$data['events']  = $this->frontM->get_events('WHERE status=1 AND EID='.$eid.'');
    	if(!$data['events']){
    		$this->notify->error('Invalid event.');
    		redirect('user/events');
    	}
    	$data['members']  = $this->userM->get_members($eid,$user->id);
    	$data['already'] = $this->userM->already_members($eid,$data['user']->id);
    	$data['platforms'] = $this->userM->event_platforms($eid);

    	$get_inputs = $this->db->query("SELECT participants FROM events WHERE EID='$eid'");
    	$get_inputs =  $get_inputs->row_array();
    	$t_query =  $this->db->query("SELECT COUNT(MID) AS cnt FROM events_members WHERE EID='$eid' AND UID='$user->id' AND deleted=0 ORDER BY MID ASC LIMIT $get_inputs[participants]");
    	$data['test'] = $t_query->row_array();
    	//if($data['already']>$get_inputs['participants']-1){
    	//	$this->notify->error('Already added maximum members, you may update your entries.');
    	//	redirect('user/event_edit/'.$eid);
    	//}
    	if($data['test']['cnt']<0){ //for incriminting purposes.
    		$i=0;
    	}else{
    		$i=$data['test']['cnt'];
    	}
    	foreach($data['members'] as $imember):
    		$i = $imember->PID;
    		$this->form_validation->set_rules("member[$i]","Name of member $i",'xss_clean|trim');
    		$this->form_validation->set_rules("member_email[$i]","Email of member $i",'xss_clean|trim');
    		$this->form_validation->set_rules("member_phone[$i]","Phone of member $i",'xss_clean|trim');
    		$this->form_validation->set_rules("platform[$i]","Platform of member $i",'xss_clean|trim');
    	endforeach;
    	if($this->form_validation->run()==FALSE){
    		$this->template->write('title', 'Modify members')
				->write_view('content', 'user/event_edit',$data)
				->render();	
    	}else{
    		//echo 'Hello';
    		$this->userM->update_members($eid);
    		redirect('user/event_edit/'.$eid);
    	}
    	if($data['already']<1){
    		$this->notify->info('Add your members.');
    		redirect('user/event_register/'.$eid);
    	}
    }

    public function profile($ref_id=0,$uid=0){
    	$data['user'] = $this->ion_auth->user()->row();
    	if($this->ion_auth->is_admin() && $ref_id!=0){
    		$ref_id = $ref_id;
            $uid = $this->ion_auth->user($uid)->row();
    	}else{
    		$ref_id =  $data['user']->ref_id;
            $uid = $this->ion_auth->user($uid)->row();
    	}
    	$data['user_basics'] = $this->userM->user_profile($ref_id,false,$uid->id);
    	$this->template->write('title', 'Profile')
				->write_view('content', 'user/profile',$data)
				->render();
    }
    public function verify_mobile(){
    	$this->config->load('sms_templates');
		$this->templates = $this->config->item('sms_templates');
    	$this->load->library('sms_lib');
    	$data['user'] = $this->ion_auth->user()->row();
    	$code = $this->frontM->generatePassword(6, 4);
    	$check = $this->userM->user_profile($data['user']->ref_id,false,$data['user']->id);
    	foreach($check as $chk):
    		if($chk->mobile_otp==''){
    			$code = $code;
    		}
    		else{
    			$code = $chk->mobile_otp;
    		}
    		$generate = $this->userM->generate_otp($code,$data['user']->ref_id);
    		$sms = str_replace('%%otp%%', $code, $this->templates['otp']['message']);
    		$this->sms_lib->queue($chk->mobile,$sms);
    		if($chk->mobile_status==1):
    			redirect('user/dashboard');
    		endif;
            $data['phone'] = '+91 '.$chk->mobile;
    	endforeach;
    	$this->form_validation->set_rules('otp','OTP','required|xss_clean|trim|max_length[6]|callback_verifyotp');
    	if($this->form_validation->run()==FALSE){
    		$this->template->write('title', 'Verify Mobile')
				->write_view('content', 'user/verify_mobile',$data)
				->render();
    	}else{
    		$this->notify->success('Phone number verified!');
    		redirect('user/dashboard');
    	}

    }

    public function verifyotp($str){
    	$user = $this->ion_auth->user()->row();
    	$this->db->where('ID', $user->ref_id);
    	$this->db->where('mobile_otp', $str);
    	$this->db->from('users_basics');
    	$count = $this->db->count_all_results();
    	if($count>0){
    		$data = array(
    			'mobile_status'=>'1',
    			'updated_at'=>time(),
    			'mobile_otp' => '',
    			);
    		$this->db->where('ID', $user->ref_id);
    		$this->db->update('users_basics',$data);
    		return TRUE;
    	}else{
    		$this->form_validation->set_message('verifyotp', 'The %s you entered is invalid.');
    		return FALSE;
    	}
    }
}
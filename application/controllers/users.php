<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->model('user_model','userM');
        $this->load->model('frontend_model','frontM');
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<div class="alert alert-danger alert-dismissible" role="alert">
		  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>', '</div>');
        if(!$this->ion_auth->logged_in() || !$this->ion_auth->in_group(array(1,14))){
        	$this->notify->warning('Please login to proceed.');
        	redirect('auth/login');
        }
        $this->template->set_template('user');
        $this->userM->user_basics();
    }

    public function index(){
        $this->dashboard();
    }

    public function dashboard(){

    }

    public function view($status='all',$type='all',$profile='all'){
        if(!$this->ion_auth->in_group(array(1,14))){
            show_404();
        }
        $this->load->helper('date');
        $data['user'] = $this->ion_auth->user()->row();
        $data['users'] = $this->userM->users($status,$type,$profile);
        $this->template->add_js('assets/js/confirm-bootstrap.js');
        $this->template->add_js('  if($(\'[data-toggle="confirm-modal"]\').length > 0) {
    $(\'[data-toggle="confirm-modal"]\').confirmModal();
  }','embed');
        $this->template->write('title', 'Users')
                ->write_view('content', 'a_users/view',$data)
                ->render();
    }    

    public function groups(){
        $data['user'] = $this->ion_auth->user()->row();
        $data['groups'] = $this->ion_auth->groups()->result();
        $this->template->write('title', 'User Groups')
                ->write_view('content', 'a_users/groups',$data)
                ->render();
    }

    public function changeStatus($UID){

        $user = $this->ion_auth->user($UID)->row();
        if($user->active==1){
            $data = array(
                'active' => 0,
                );
            $status = 'Inactive';
        }elseif($user->active==0){
            $data = array(
                'active' => 1,
                );
            $status = 'Active';
        }
        $this->db->where('id',$UID);
        $this->db->update('users',$data);
        $this->notify->success('Changed status of <strong>'.$user->username.'</strong> to '.$status);
        redirect('users/view');
    }

}
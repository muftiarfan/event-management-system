<?php defined('BASEPATH') OR exit('No direct script access allowed.');

if (!function_exists('referer_url')) {

    /**
    * Returns url of the current referer
    *
    * @access   public
    * @return   string Referer url
    */
    function referer_url()
    {
        return isset($_SERVER['HTTP_REFERER']) ? 
            $_SERVER['HTTP_REFERER'] : '';
    }

}

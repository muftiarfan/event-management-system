<?php
  if(! function_exists('isMobileNo')) {
    /**
     * Validates mobile number
     * 
     * Checks a valid number with 10 digits and starting with 7 or 8 or 9
     * 
     * @param string $mobileNumber Mobile Number
     * @return boolean True if the given string is a valid mobile number, else false
     */
    function isMobileNo($mobileNumber)
    {
      return preg_match('/^(91)*[789]\d{9}$/', $mobileNumber);
    }
  }
 
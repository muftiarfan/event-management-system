<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Quickgit 

{
    /** @var int */
    private $major = 2;

    /** @var int */
    private $minor = 97;

    /** @var string */
    private $patch = '';

    /** @var int */
    private $commits = 0;

    /** @var string */
    private $commit = '';

  
    /**
     * @method __construct
     */
    public function __construct()
    {
        // Collect git data.
        exec('git describe --always', $gitHashShort);
        $this->patch = $gitHashShort[0];

        exec('git rev-list HEAD | wc -l', $gitCommits);
        $this->commits = $gitCommits;

        exec('git log -1', $gitHashLong);
        $this->commit = $gitHashLong[2];
    }

    public function version(){
        $version = $this->toString('long');
        //print_r($this->commit);
        return $version;
        
    }
    /**
     * @return string
     */
    public function toString($format = 'short')
    {
        switch ($format) {
            case 'long':
                return sprintf(
                    '%d.%d.%s (#%d, %s)',
                    $this->major,
                    $this->minor,
                    $this->patch,
                    $this->commits,
                    $this->commit
                );
            default:
                return sprintf(
                    '%d.%d.%s',
                    $this->major,
                    $this->minor,
                    $this->patch
                );
        }
    }
  
    /**
     * @method __toString
     */
    public function __toString()
    {
        return $this->toString();
    }

}

<?php
namespace EMS\libraries;
class Sms_recipient
{

	protected $ci;
    protected $list = array();
    protected $parsed = array();
    protected $attributes = array(
        'events'   => array(
            'e.phone',
            'e.EID',
            'e.ev',
            ),
        'user'     => array(
            'mobile'
            ),
        );
	public function __construct()
	{
	// Copy an instance of CI so we can use the entire framework.
		$this->ci =& get_instance();
	}

	public static function all()
    {
        $instance = new static;
        $instance->ci = &get_instance();

        $instance->groups();
        
        $instance->individuals();

        return $instance;
    }

    public static function find($find)
    {
        $instance = new static;
        $instance->ci = &get_instance();

        $instance->groups($find);
        
        $instance->individuals($find);

        return $instance;
    }
   public static function with($attributes = array())
    {
        $instance = new static;
        $instance->ci = &get_instance();
        foreach($instance->attributes as $key => &$attribute) {
            if(isset($attributes[$key])) {
                $attribute = array_merge($attribute, $attributes[$key]);    
            }
        }
        
        return $instance;
    }

    public function parse($recipientList)
    {
        $entities = array();
        if(!is_array($recipientList)) {
            $recipientList = explode(',', $recipientList);
        }
        // All entities to be parsed
        if (in_array('e:events', $recipientList)) {
            $entities['events'] = $this->ci->db
                ->select('*')
                ->from('events')
                ->where('status',1)
                ->get()
                ->result()
                ;
            $this->parsed = $entities;

            return $this;
        }

        foreach ($recipientList as $item) {
            $filters = explode('|', $item);
            $entity = explode(':', $filters[0]);
            $entity = $entity[1];
            unset($filters[0]);
            $entities[$entity][] = $filters;
        }
        $this->parsed = $this->parseFilters($entities);

        return $this;
    }
    public function flatten()
    {
        $mergeRecipientTypes = array();
        if(empty($this->parsed)) return array();
        foreach($this->parsed as $key => $parsed)  {
            $mergeRecipientTypes = array_merge($mergeRecipientTypes, $parsed);
        }
        $flatten = array();
        foreach($mergeRecipientTypes as $item) {
            $item = (array) $item;

            if(count($item) == 1) {
                $item = array_values($item);
                $flatten[] = $item[0];
            } else {
                foreach($item as $value) {
                    $flatten[] = $value;
                }
            }
        }

        return $flatten;
    }

	public function groups($find = null)
    {

        $groups = array();

        //All events
        $groups[] = array(
            'id'        => 'e:events',
            'name'      => 'All Events',
            'tagline'   => 'All event attendees, including passed ones',
            'photo'     => '',
            );
        //All active users
        $groups[] = array(
            'id'        => 'e:user',
            'name'      => 'All Users',
            'tagline'   => 'All active Users',
            'photo'     => '',
            );
        //Users who have atleast registered for one event
        $groups[] = array(
            'id'        => 'e:users',
            'name'      => 'Returning Users',
            'tagline'   => 'All users who have registered once for atleast one event',
            'photo'     => '',
            );       

        // event wise groups
        $this->ci->load->model('frontend_model','frontM');
        $this->ci->load->model('event_model','eventM');
        $events = $this->ci->frontM->get_events('WHERE status=1');
        foreach($events as $event) {
            //All users of an event
            $groups[] = array(
                'id'        => 'e:events|ev:'.$event->EID.'|u:all',
                'name'      => 'Event '.$event->name,
                'tagline'   => 'All users of event '.$event->name,
                'photo'     => '',
                );
            // User registrations of an event
            $events_u = $this->ci->eventM->get_usersEvent($event->EID);
            foreach($events_u as $events_us) {
            	$user =  $this->ci->ion_auth->user($events_us->UID)->row();
            	$profile = $this->ci->userM->user_profile($user->ref_id,true,$user->id);
            	//var_dump($profile);
                $groups[] = array(
                    'id'        => 'e:events|ev:'.$event->EID.'|u:'.$user->ref_id.'|evn:'.$event->name,
                    'name'      => 'Event:'.$event->name.'/ User: '.$profile['full_name'].'/'.$profile['t_org'],
                    'tagline'   => 'Registration by '.$profile['full_name'].' for '.$event->name,
                    'photo'     => '',
                    );
            }
        }

        //Event wise, session wise, gender wise
        $eventGroups = $this->ci->db->query("SELECT `EID`, `gender`, `platform` FROM 
            events_members WHERE deleted = '0' GROUP BY EID, gender, platform
            ORDER BY EID ASC, gender ASC, platform ASC")->result();
        foreach ($eventGroups as $eventGroup) {
        	$this->ci->load->model('event_model','eventM');
        	$this->ci->load->model('user_model','userM');
        	$event = $this->ci->eventM->get_event($eventGroup->EID,true);
        	$session = $this->ci->userM->idData($eventGroup->platform,$eventGroup->EID,'platform_name');
            $event = url_title($event['name']); // event
            $dashedGender = url_title($eventGroup->gender); // gender
            $dashedSession = url_title($session); // session
            $groups[] = array(
                'id'        => 'e:users|ev:'.$eventGroup->EID.'|g:'.$dashedGender.'|s:'.$eventGroup->platform,
                'name'      => 'Event > '.$event.' > '.$eventGroup->gender.' > '.$session,
                'tagline'   => 'Event / Gender / For Session',
                'photo'     => '',
                );
        }

        if($find) {
            $groups = array_filter($groups,
                function ($group) use ($find) 
                {
                    if(preg_match("/".$find."/i", $group['name'])) {
                        return true;
                    }
                }
                );
        }

        $this->list['groups'] = $groups;

        return $this;

    }

    public function individuals($find = null)
    {
    	$this->ci->load->model('user_model','userM');
        $append = '';
        if (!is_null($find) && is_numeric($find)) {
            $append = "id = $find";
        } elseif (!is_null($find)) {
            $append =  
                "username LIKE '%$find%'";
        }
        $items = array();
        $entities = array();
        
        $entities = $this->ci->userM->users('active','all','all',$append);
        if($entities) {
            foreach($entities as $entity) {
            	$profile = $this->ci->userM->user_profile($entity->ref_id,true,$entity->id);
                $items[] = array(
                    'id'        => 'e:user|id:'.$entity->id,
                    'name'      => $profile['full_name'].' // '.$entity->id,
                    'tagline'   => $profile['t_org'],
                    'photo'     => '',
                    );   
            }
        }
        
        $this->list['individuals'] = $items;

        return $this;
    }

    // TODO: Only implementation
    public function only($itemType)
    {
        if(!in_array($itemType, $this->list)) return $this;
        $list = $this->list;
    }

    public function toArray()
    {
        return $this->list;
    }

    public function toJson()
    {
        
        return json_encode($this->toArray());
    }

    public function select2Json()
    {
        $recipients = array();
        $recipients = array_merge($this->list['individuals'], $this->list['groups']);
               // $recipients = $this->list['individuals'];
        if(empty($recipients)) return json_encode(array());

        $themedrecipients = array();
        foreach($recipients as $recipient) {
            $themedrecipients[] = array(
                'id'        => $recipient['id'],
                'text'      => $recipient['name'],
                'display'   => $this->select2OptionDisplay($recipient),
            );
        }
        
        return json_encode($themedrecipients);
    }
    /**
     * Themes selec2 option for display
     *
     * Themes a single recipient in select2 format for display as option
     *
     * @param array $recipient
     *      Single recipient item from $item property
     * @return string Formattted HTML for display
     */
    private function select2OptionDisplay($item){
        if(!$item or !is_array($item)) return '';
        $output = 
            '<div class="clearfix">'.
                '<div class="text-left">'.addslashes($item['name']).'</div>
                <div class="text-muted"><small>'.$item['tagline'].'</small></div>
            </div>';

        return $output;
    }
    private function attributes($entity)
    {
        return $this->attributes[$entity];
    }

    private function columns($entity)
    {
        $attributes = $this->attributes($entity);

        return implode(',', $attributes);
    }
    // Valid filters
    //  - Student: id, c, s, b
    private function parseFilters($parsed)
    {
        $validEventFilters = array('ev', 's', 'u', 'eid','g');
        $validUserFilters = array('id', 'ev', 'g', 's', 'u');
        $recipients = array();
        $temp = array();
        foreach($parsed as $entity => $filterGroups) {
            foreach($filterGroups as $filters) {
                $filtersAssoc = array();
                foreach($filters as $f) {
                    $f = explode(':', $f);
                    $filtersAssoc[$f[0]] = $f[1];    
                }
                $temp[$entity][] = $this->entityrecipients(
                    $entity, $filtersAssoc);
            }
        }
        //return $temp;
        foreach($temp as $entity => $byType) {
            $recipients[$entity] = array();
            for($i = 0; $i < count($byType); $i++) {
                $recipients[$entity] = array_merge(
                    $recipients[$entity], $byType[$i]);
            }
        }

        return $recipients;
    }
    private function entityrecipients($entity, $filtersAssoc)
    {
    	$this->ci->load->model('event_model','eventM');
    	$this->ci->load->model('user_model','userM');
        $recipients = array();
        if($entity == 'events') {
            $recipients = $this->ci->db
                ->select('phone')
                ->from('events_members')
                ->where('deleted',0)
                ;

            $recipients = $this->applyEventFilters($recipients, $filtersAssoc);
        } elseif($entity == 'user') {

        } elseif($entity == 'users'){
            $recipients = $this->ci->db
                ->select('phone')
                ->from('events_members')
                ->where('deleted',0)                
                ;
                $recipients = $this->applyEventFilters($recipients, $filtersAssoc);
        }

        return $recipients;
    }
    private function applyEventFilters($EventObject, $filtersAssoc)
    {
        if(array_key_exists('ev', $filtersAssoc)) {
            $EventObject->where('EID', $filtersAssoc['ev']);
            
            //return $EventObject->get()->result_array();
        }
        foreach($filtersAssoc as $key => $value) {
            if($key == 'g') {
                $EventObject->where('gender', $filtersAssoc['g']);
            }
            if($key == 's' && $value != 'all') {
                $EventObject->where('platform', $filtersAssoc['s']);
            }
            if($key == 'u' && $value != 'all') {
                $EventObject->where('UID', $filtersAssoc['u']);
            }
        }

        return $EventObject->get()->result_array();
    }
	/*public function __get($var)
	{
		return get_instance()->$var;
	}*/
	
}
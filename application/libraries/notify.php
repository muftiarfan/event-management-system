<?php

class Notify
{
    public static $notifications = array();
    
    public static function success($message)
    {
        static::add('success', $message);
    }

    public static function error($message)
    {
        static::add('danger', $message);
    }

    public static function warning($message)
    {
        static::add('warning', $message);
    }

    public static function info($message)
    {
        static::add('info', $message);
    }

    public static function show()
    {
        $ci = &get_instance();
        $notifications = $ci->session->flashdata('notifications');
        if($notifications) {
            $output = '';
            foreach($notifications as $type => $msgs){
                $output .= '<div class="alert alert-'.$type.'"><a class="close" data-dismiss="alert">&times;</a><strong>FYI:</strong> ';
                $output .= implode('<br>', $msgs);
                $output .= '</div>';
            }
            
            return $output;
        }
        
        return false;
    }
    
    protected static function add($type, $message)
    {
        $ci = &get_instance();
        $message = (array) $message;
        foreach($message as $m) {
            static::$notifications[$type][] = $m;
        }
        
        $ci->session->set_flashdata('notifications', static::$notifications);
    }
}
<?php

class Sms_lib
{
	public function __construct()
	{
	// Copy an instance of CI so we can use the entire framework.
		$this->ci =& get_instance();
	}
	
	public function execute()

	{
    	// Request headers
    	$headers = array(
    		'Referer'	=> base_url(),
    	);
    	// Sms not locked / not sent / not scheduled for future
    	$smses = $this->ci->db->query("SELECT * FROM sms WHERE status=0 ORDER BY id DESC LIMIT 500");
    	$smses = $smses->result();
    	
    	if(!$smses) return;
    	
    	foreach($smses as $sms) {
            // Check if the sms has not been sent
            // by another process, in meantime

            if($this->isLocked($sms->id) || $this->isSent($sms->id)) continue;

            $this->lock($sms->id);

            $senderApi = $this->ci->db->query("SELECT * FROM sms_gateways WHERE status=1 ORDER BY RAND()");
            $senderApi = $senderApi->result();
			$api =  array();
			foreach ($senderApi as $key => $value) 
			{
				$api['extra'] = $value->extra;
			}
            parse_str($api['extra'], $gatewayExtras);
            $gatewayExtras = (object) $gatewayExtras;
            //print_r($senderApi);
            foreach($senderApi as $sndrAPI){
            	$sndrAPI = $sndrAPI;
            }
            $parsedApi = $this->parseApi($sndrAPI, $sms);
            $url = $parsedApi->scheme.'://'.$parsedApi->host.$parsedApi->path;

            //$apiResponse = $CI->curl->simple_post($url, $headers, $parsedApi->query);
            $this->ci->curl->create($url.'?'.$parsedApi->query);
            $this->ci->curl->http_header('Referer:'.base_url());
            //print_r($parsedApi->query);
            //$this->ci->curl->post($parsedApi->query);
            $apiResponse = $this->ci->curl->execute();
            //print_r($apiResponse);
            //If API returned success status, mark sms as sent
            $pattern = $this->parseSuccessPattern($gatewayExtras->successpattern);
            
            $sms->gateway_id = $sndrAPI->gateway_id;
            $sms->api_response = $apiResponse;
            
            $this->unlock($sms->id);
            $data = array(
            	'gateway_id' => $sms->gateway_id,
            	'api_response' => $sms->api_response,
            	'status' => 1,
            	'updated' => time(),
            	);
            $this->ci->db->where('id',$sms->id);
            $this->ci->db->update('sms',$data);
        }
    }

    public function queue($recipient,$message){
    	$data = array(
    		'recipient'=>$recipient,
    		'message'=>$message,
    		'created' => time(),
    		'scheduled' => time(),
    		'status' => '0',
    		'locked' => '0',
    		);
    	$this->ci->db->insert('sms',$data);
    }

    public function isLocked($id)
    {
    	$query = $this->ci->db->query('SELECT * FROM sms WHERE id='.$id.'');
    	$query = $query->row_array();

        return ($query['locked'] == 1) ? true : false;
    }

    public function isSent($id)
    {
    	$query = $this->ci->db->query('SELECT * FROM sms WHERE id='.$id.'');
    	$query = $query->row_array();
        return ($query['status'] == 1) ? true : false;
    }

    public function lock($id)
    {
    	$data = array(
    		'locked' => '1',
    		);
    	$this->ci->db->where('id',$id);
    	$this->ci->db->update('sms',$data);
    }    

    public function unlock($id)
    {
    	$data = array(
    		'locked' => '0',
    		);
    	$this->ci->db->where('id',$id);
    	$this->ci->db->update('sms',$data);
    }
    private function parseSuccessPattern($pattern)
    {
        if(strpos($pattern, '~~') !== false) {
            $start = strpos($pattern, '~~');
            $length = strrpos($pattern, '~~') - $start;
            $eval = substr($pattern, $start, $length);
            $eval = trim($eval, '~');
            $pattern = eval('return '.$eval.';');
            
            return $pattern;
        }
        return $pattern;
    }

	private function parseApi($gateway, $sms)
	{
    	parse_str($gateway->extra, $gatewayInfo);
    	$gatewayInfo = (object) $gatewayInfo;
    	
    	$recipient = $sms->recipient;
    	if($gatewayInfo->prefix_cc == 1) {
    		$recipient = '91'.$recipient;
    	}

    	$message = $sms->message;
    	if($gatewayInfo->urlencode == 1) {
    		$message = urlencode($message);
    	}
    	
    	$placeholders = array(
            '##username##',
            '##password##',
            '##recipient##',
            '##sender_id##',
            '##message##',
            '##route_type##',
            '##message_id##'
        );
        $replacements = array(
            $gatewayInfo->username,
            $gatewayInfo->password,
            $recipient,
            'DPSSGR',
            urlencode($sms->message),
            $gatewayInfo->route_type,
            $sms->api_response,
        );
    	
    	return (object) parse_url(str_replace($placeholders, $replacements, $gateway->api));
    }
}
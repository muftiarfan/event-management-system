<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Event_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function event_types(){

    	$query = $this->db->query("SELECT * FROM events_types ORDER BY ID");
    	return $query->result();
    }
    public function add_event($icardImg,$tLogo,$tLogoFooter){
    	$date_epoch = date_create($this->input->post('event_date',TRUE)); 
		$date_epoch = date_format($date_epoch, 'U');
    	$data = array(
    		'name' => htmlspecialchars($this->input->post('event_name',TRUE)),
            'url_slug' => url_title(strtolower($this->input->post('event_name',TRUE))),
    		'description' => $this->input->post('event_desc',TRUE),
    		'type' => $this->input->post('event_type',TRUE),
    		'participants' => $this->input->post('event_members',TRUE),
    		'event_date' => $this->input->post('event_date',TRUE),
    		'date_epoch' => $date_epoch,
    		'status' => '1',
            'ticket_extra_info' => $this->input->post('event_ticket_extra_info',TRUE),
            'location' => $this->input->post('event_location',TRUE),
            'registration_extra_info' => $this->input->post('registration_extra_info',TRUE),
            'icard_header_img' => $icardImg,
            'ticket_logo' => $tLogo,
            'ticket_logo_footer' => $tLogoFooter,
            'event_organizerID' => $this->input->post('event_org',TRUE),
    		'created_at' => time(),
    		'updated_at' => time(),
    		);
    	$this->db->insert('events',$data);
    	$eid = $this->db->insert_id();
    	//add this events cats/platforms
    	$plats = explode(',',$this->input->post('event_cats',TRUE)); 
          foreach($plats as $platform){

          	$data = array(
          		'EID' => $eid,
          		'name' => htmlspecialchars(ucwords($platform)),
          		);
          	$this->db->insert('events_platforms',$data);
          }
          $event_tickets = $this->input->post('event_tickets');
          foreach($event_tickets as $ticket){

            $data = array(
                'event_id' => $eid,
                'ticket_id' => $ticket,
                'UID' => 1,
                'created_at' => time(),
                'updated_at' => time(),
                );
            $this->db->insert('events_tickets',$data);
          }
    }
    public function edit_event($eid){
        $date_epoch = date_create($this->input->post('event_date',TRUE)); 
        $date_epoch = date_format($date_epoch, 'U');
        $data = array(
            'name' => htmlspecialchars($this->input->post('event_name',TRUE)),
            'url_slug' => url_title(strtolower($this->input->post('event_name',TRUE))),
            'description' => $this->input->post('event_desc',TRUE),
            'type' => $this->input->post('event_type',TRUE),
            'participants' => $this->input->post('event_members',TRUE),
            'event_date' => $this->input->post('event_date',TRUE),
            'date_epoch' => $date_epoch,
            'status' => '1',
            'ticket_extra_info' => $this->input->post('event_ticket_extra_info',TRUE),
            'location' => $this->input->post('event_location',TRUE),
            'registration_extra_info' => $this->input->post('registration_extra_info',TRUE),
            'event_organizerID' => $this->input->post('event_org',TRUE),
            'updated_at' => time(),
            );
        $this->db->where('EID',$eid);
        $this->db->update('events',$data);
        
    }

    public function event_platforms($eid){
    	$query = $this->db->query("SELECT * FROM events_platforms WHERE EID=$eid");
    	return $query->result();
    }

    public function deleteEvent($eid){
    	$data = array(
    		'status' => '0',
    		'updated_at' => time(),
    		);
    	$this->db->where('EID',$eid);
    	$this->db->update('events',$data);
    }

    public function get_usersEvent($eid, $extra=null){
    	$query = $this->db->query("SELECT DISTINCT(UID) FROM events_members WHERE EID=$eid AND deleted=0 $extra");
    	return $query->result();
    }

    public function get_event($eid,$row=false){

        $this->db->select("*");
        $this->db->from('events');
        $this->db->where('EID',$eid);
        $do = $this->db->get();
        if($row){
            return $do->row_array();
        }else{
            return $do->result();
        }
    }

    public function member_details($uid,$eid,$pid){
        $query = $this->db->query("SELECT * FROM events_members WHERE EID='$eid' AND UID='$uid' AND PID='$pid' AND deleted = 0 ORDER BY PID ASC");
        return $query->row_array();
    }

    public function insert_eventTicketsPlatform($PID,$eid){

        $this->db->select("*");
        $this->db->from('events_tickets');
        $this->db->where('platform_ID',$PID);
        $get = $this->db->get();
        $data = array(
            'platform_ID' => $PID,
            'event_id' => $eid,
            'created_at' => time(),
            );
        if($get->num_rows()>0){

            $this->db->where('platform_ID',$PID);
            $this->db->update('events_tickets',$data);
            $event_tickets_id = $this->db->query("SELECT * FROM events_tickets WHERE platform_ID='$PID' AND event_ID='$eid'");
            $event_tickets_id = $event_tickets_id->row_array();
            $event_tickets_id = $event_tickets_id['TID'];
        }else{
            $this->db->insert('events_tickets',$data);
            $event_tickets_id =  $this->db->insert_id();           
        }
        $tickets = $this->input->post('ticket_id');
        foreach($tickets as $p_id=>$ticket_id){

            $data = array(
                'ticket_id' => $ticket_id,
                'updated_at' => time(),
                );
            $this->db->where('platform_ID',$p_id);
            $this->db->update('events_tickets',$data);
        }

        $amount = $this->input->post('session_amount');
        foreach($amount as $p_id=>$amnt){

            $data = array(
                'amount' => $amnt,
                'updated_at' => time(),
                );
            $this->db->where('platform_ID',$p_id);
            $this->db->update('events_tickets',$data);
        }

        $attendee_limit = $this->input->post('session_attendee');
        foreach ($attendee_limit as $p_id=>$limit){

            $data = array(
                'attendee_limit' => $limit,
                'updated_at' => time(),
                );
            $this->db->where('PID',$p_id);
            $this->db->update('events_platforms',$data);
        }
        $data = array(
            'events_ticket_id'=> $event_tickets_id,
            'updated_at'=> time(),
            );
        $this->db->where('PID',$PID);
        $this->db->update('events_platforms',$data);
    }

    public function add_organizer(){

        $data = array (
            'name' => $this->input->post('org_name',TRUE),
            'contact' => $this->input->post('org_contact',TRUE),
            'email' => $this->input->post('org_email',TRUE),
            'address' => $this->input->post('org_address',TRUE),
            'UID' => 1,
            'created_at' => time(),
            'updated_at' => time(),
            );
        $this->db->insert('event_organizers',$data);
    }
    public function ajax_organizers($term){
        $query = $this->db->query("SELECT * FROM event_organizers WHERE name LIKE '%$term%' OR email LIKE '%$term%' OR ID='$term' LIMIT 40");
        $query = $query->result();
        foreach ($query as $org) {
            $data[] = array('id' => $org->ID, 'text' => $org->name.' - '.$org->address.' - '.$org->email);              
        } 
        return json_encode($data);
    }

    public function organizerDetails($ID=''){

        $this->db->select("*"); 
        $this->db->from('event_organizers');
        if($ID!=''):
        $this->db->where('ID',$ID);
        endif;
        $get = $this->db->get();
        if($ID==''){
            return $get->result();
        }else{
            return $get->row_array();
        }
    }
     public function attendee_list($eid, $session='all', $gender='all',$checked_in='all',$user='all'){

        $this->db->select("*");
        $this->db->from('events_members');
        $this->db->where('EID',$eid);
        $this->db->where('deleted',0);
        if($session !='all')
        $this->db->where('platform',$session);
        if($gender !='all')
        $this->db->where('gender',$gender);
        if($checked_in !='all')
        $this->db->where('checked_in',$checked_in);        
        if($user !='all')
        $this->db->where('UID',$user);

        $get = $this->db->get();
        return $get->result();
    }

    public function member_checkin($eid,$mid,$type){

        switch($type){

            case 'checkin':
            $data = array(
                'checked_in' => 1,
                'checked_in_at' => time(),
                );
            $this->db->where('EID',$eid);
            $this->db->where('MID',$mid);
            $this->db->update('events_members',$data);
            break;

            case 'uncheckin':
            $data = array(
                'checked_in' => 0,
                'checked_in_at' => 0,
                );
            $this->db->where('EID',$eid);
            $this->db->where('MID',$mid);
            $this->db->update('events_members',$data);
            break;
        }
    }

    public function session_counts($PID,$EID,$type){

        switch($type){
            case 'registrations':
            $count = $this->db->query("SELECT * FROM events_members WHERE platform='$PID' AND EID='$EID' AND deleted=0");
            break;

            case 'checked_in':
            $count = $this->db->query("SELECT * FROM events_members WHERE platform='$PID' AND EID='$EID' AND deleted=0 AND checked_in=1");
            break;
        }
        return $count->num_rows();
    }

    public function event_statistics($EID,$type){

        switch($type){

            case 'registrations_att':
            $this->db->where('deleted', 0);
            $this->db->where('EID', $EID);
            $this->db->from('events_members');
            return $this->db->count_all_results();
            break;           

            case 'registrations_checked':
            $this->db->where('deleted', 0);
            $this->db->where('checked_in', 1);
            $this->db->where('EID', $EID);
            $this->db->from('events_members');
            return $this->db->count_all_results();
            break;

            case 'registrations_users':
            $count = $this->db->query("SELECT COUNT(DISTINCT(UID)) as regs FROM events_members WHERE EID='$EID' AND deleted=0");
            $count = $count->row_array();
            return $count['regs'];
            break;            

            case 'registrations_users_T':
            $count = $this->db->query("SELECT COUNT(DISTINCT(UID)) as regs FROM events_members WHERE EID='$EID' AND deleted=0 AND icard_path IS NULL");
            $count = $count->row_array();
            return $count['regs'];
            break;

            case 'registrations_indi':
            $this->db->where('deleted', 0);
            $this->db->where('EID', $EID);
            $this->db->where('icard_path IS NOT NULL',null,false);
            $this->db->from('events_members');
            return $this->db->count_all_results();
            break;

            case 'registrations_male':
            $this->db->where('deleted', 0);
            $this->db->where('gender', 'Male');
            $this->db->where('EID', $EID);
            $this->db->from('events_members');
            return $this->db->count_all_results();
            break;

            case 'registrations_female':
            $this->db->where('deleted', 0);
            $this->db->where('gender', 'Female');
            $this->db->where('EID', $EID);
            $this->db->from('events_members');
            return $this->db->count_all_results();
            break;
        }
    }

    public function delete_member($mid,$eid,$uid){

        $data = array(
            'deleted' => 1,
            'updated_at' => time(),
            );
        $this->db->where('MID',$mid);
        $this->db->where('EID',$eid);
        $this->db->where('UID',$uid);
        $this->db->update('events_members',$data);
    }

    public function reg_open($EID){

        $this->db->select('*');
        $this->db->from('events');
        $this->db->where('EID',$EID);
        $this->db->where('allow_reg',1);
        $count = $this->db->count_all_results();

        if($count==1){
            return TRUE;
        }else{
            return FALSE;
        }
    }
}
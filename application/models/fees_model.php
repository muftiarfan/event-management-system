<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Fees_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

/*    public function fee_types(){
    	$this->db->select("*");
    	$this->db->from('fee_types');
    	$get = $this->db->get();
    	return $get->result();
    }*/

    public function tickets_types(){
    	$this->db->select("*");
    	$this->db->from('fees_tickets');
    	$get = $this->db->get();
    	return $get->result();
    }

    public function ticket_info($ID){
    	$this->db->select("*");
    	$this->db->from('fees_tickets');
    	$this->db->where('ID',$ID);
    	$get = $this->db->get();
    	return $get->row_array();
    }
    public function sum_sessionsAmountAttendees($EID,$PID){

    	$this->db->select("SUM(amount), *");
    	$this->db->from('events_tickets');
    	//$this->db->where
    }

    public function add_fee($EID,$EID_user,$amount,$ticket_type,$custom_charges,$custom_charges_desc){
    	$UID = $this->ion_auth->user()->row()->id;
    	$data = array(
    		'event_id' => $EID,
    		'event_idUser' => $EID_user,
    		'amount' => $amount,
    		'UID' => $UID,
    		'ticket_id' => $ticket_type,
    		'custom_charges' => $custom_charges,
    		'custom_charges_desc' => $custom_charges_desc,
    		'created_at' => time(),
    		'updated_at' => time(),
    		);
    	$this->db->select("*");
    	$this->db->from('fees');
    	$this->db->where('event_id',$EID);
    	$this->db->where('event_idUser',$EID_user);
    	$get = $this->db->get();
    	if($get->num_rows()>0){
    		$this->notify->error('Fee payment of this event, for this user already exists.');
    		redirect('fees/add');
    	}else{
    		$this->db->insert('fees',$data);
    	}
    }

    public function payments_fee($eid='all', $ticket_id='all', $user_id='all',$trans='all'){

    	$this->db->select("*");
    	$this->db->from('fees');
    	if($eid !='all')
    	$this->db->where('event_id',$eid);
    	if($ticket_id !='all')
    	$this->db->where('ticket_id',$ticket_id);
    	if($user_id !='all')
    	$this->db->where('UID',$user_id);
    	if($trans !='all')
    	$this->db->where('ID',$trans);

    	$get = $this->db->get();
    	return $get->result();
    }
}
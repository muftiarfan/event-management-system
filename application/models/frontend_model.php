<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frontend_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function get_events($extra=''){
    	$query = $this->db->query('SELECT * FROM events '.$extra.'');
    	return $query->result();
    }

    public function front_reg($indi=false,$eid,$contact_details,$admin_email,$admin_from,$icard=false){
        $this->load->library('encrypt');
    	$this->load->library('swiftmailer');
    	$full_name = $this->input->post('name',true);
    	$email = $this->input->post('email',true);
    	$t_org = $this->input->post('t_org',true);
        $mobile = $this->input->post('mobile',true);
    	$phone = $this->input->post('phone',true);

    	$dob = $this->input->post('dob',true);
    	$hash = $this->encrypt->sha1($email);
        if($indi==true){
            $individual = 1;
        }else{
            $individual = 0;
        }
        if(!$t_org){
            $t_org = null;
        }
        if(!$phone){
            $phone = null;
        }
    	$data = array(
    		'full_name' => $full_name,
    		'email' => $email,
    		't_org' => $t_org,
            'mobile' => $mobile,
    		'phone' => $phone,
    		'birthday' => $dob,
    		'hash' => $hash,
            'icard_path' => $icard,
            'individual' => $individual,
    		'created_at' => time(),
    		'updated_at' => time(),
    		);
    	$this->db->insert('users_basics', $data);
    	$id = $this->db->insert_id();
    	$password = $mobile;
    	$group = array(2,5,6);
    	$add_data = array(
    		'ref_id' => $id,
    		);
        $otherData = array(
            'EID' => $eid,
            'contact_details' => $contact_details,
            'admin_email' => $admin_email,
            'admin_from' => $admin_from,
            );
    	$user = $this->ion_auth->register('user'.$id, $password, $email, $add_data, $group,$otherData);
        if($indi==true){
            $data = array(
                        'UID'=> $user,
                        'PID' => 1,
                        'platform'=> $this->input->post('session'),
                        'EID'=> $this->input->post('eid'),
                        'hash' => md5($email),
                        'name'=>ucwords($full_name),
                        'email' => $email,
                        'phone' => $phone,
                        'icard_path' => $icard,
                        'gender' => $this->input->post('member_gender',TRUE),
                        'created_at' => time(),
                        'updated_at' => time(),
                        );
                    $add = $this->db->insert('events_members',$data);
        }

        $this->config->load('email_templates');
        $this->templates = $this->config->item('email_templates');
        $search = array('%fullname%','%phone%','%mobile%','%email%','%team%');
        $admin_email = $otherData['admin_email'];
        $admin_from = $otherData['admin_from'];
        $replace  = array($full_name,$phone,$mobile,$email,$t_org);
        $message = str_replace($search, $replace, $this->templates['user_registration_admin']['message']);
        $email = $this->swiftmailer->send_mail('EMS: New User Registration',array($admin_email => $admin_from),array($admin_email,'muftiarfanfarooqi07@gmail.com','ehsan.quddusi@gmail.com'),$message);
    }
    /*
    * Verify mobile
    * Generates an OTP in table and also sends SMS to mobile
    * $ref_id : user identification in users_basics
    * similar method in user model : generate_otp();
    */
    public function sms_otp($ref_id){
    	$otp = $this->generatePassword(6,4);
    	$data = array(
    		'mobile_otp' => $otp,
    		);
    	$this->db->where('id', $ref_id);
    	$this->db->update('users_basics', $data);
    }

    public function generatePassword($length, $strength) 
    {
    	$vowels = 'aeuy';
    	$consonants = 'bdghjmnpqrstvz';
    	if ($strength & 1) 
    		{
    			$consonants .= 'BDGHJLMNPQRSTVWXZ';
    		}
    		if ($strength & 2) 
    		{
    			$vowels .= "AEUY";
    		}
    		if ($strength & 4) 
    		{
    			$consonants .= '23456789';
    		}
    		if ($strength & 8) 
    		{
    			$consonants .= '@#$%';
    		}
    		$password = '';
    		$alt = time() % 2;
    		for ($i = 0; $i < $length; $i++) 
    		{
    			if ($alt == 1) 
    			{
    				$password .= $consonants[(rand() % strlen($consonants))];
    				$alt = 0;
    			} 
    			else 
    			{
    				$password .= $vowels[(rand() % strlen($vowels))];
    				$alt = 1;
    			}
    		}
    		return $password;
    	}
}
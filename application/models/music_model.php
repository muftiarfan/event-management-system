<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Music_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function add(){

    	$data = array(
    		'title' => ucwords($this->input->post('title',TRUE)),
    		'artist' => ucwords($this->input->post('artist',TRUE)),
    		'album' => ucwords($this->input->post('album',TRUE)),
    		'year' => $this->input->post('year',TRUE),
    		'status' => 1,
    		'created_at' => time(),
    		'updated_at' => time(),
    		);
    	$this->db->insert('music_songs',$data);
    }

    public function listing($where=''){
        $query = $this->db->query('SELECT * FROM music_songs '.$where.' ORDER BY ID');
        return $query->result();
    }   

     public function listing2($where=''){
    	$query = $this->db->query('SELECT * FROM music_songs '.$where.'');
    	return $query->result();
    }    

    public function listing_orders($where=''){
        $query = $this->db->query('SELECT * FROM music_orders '.$where.' ORDER BY ID');
        return $query->result();
    }
    public function listing_orders2($where=''){
    	$query = $this->db->query('SELECT * FROM music_orders '.$where.'');
    	return $query->result();
    }

    public function add_order(){
    	$price = $this->get_varVal('music_order_price');
    	$data = array(
    		'SID' => (int)$this->input->post('song',TRUE),
    		'ordered_at' => time(),
    		'price' => $price,
            'from' => $this->input->post('from',TRUE),
            'to' => $this->input->post('to',TRUE),
    		);
 	    $this->db->insert('music_orders',$data);
 	    $this->orders_number((int)$this->input->post('song',TRUE));
    }
    public function played($sid){
        $this->db->where('SID',$sid);
        $data = array(
            'played_at'=>time(),
            );
        $this->db->update('music_orders',$data);
    }
    public function ajax_orders($term){
    	$query = $this->db->query("SELECT * FROM music_songs WHERE title LIKE '%$term%' OR artist LIKE '%$term%' OR ID='$term' LIMIT 40");
    	$query = $query->result();
    	foreach ($query as $song) {
    		$data[] = array('id' => $song->ID, 'text' => $song->title.' - '.$song->artist.' - '.$song->album);			 	
    	} 
    	return json_encode($data);
    }
    public function orders_number($SID){
    	$this->db->where('SID', $SID);
    	$this->db->from('music_orders');
    	$count = $this->db->count_all_results();
    	$data = array(
    		'orders' => $count,
    		'updated_at' => time(),
    		);
    	$this->db->where('ID',$SID);
    	$this->db->update('music_songs',$data);
    }

    public function update_variables($var,$value){
    	$data = array(
    		'var_value' => $value,
    		'updated_at' =>time(),
    		);
    	$this->db->where('var_name',$var);
    	$this->db->update('sys_variables',$data);
    }

    public function get_varVal($var){
    	$query = $this->db->query('SELECT * FROM sys_variables WHERE var_name = "'.$var.'"');
    	$query = $query->row_array();
    	return $query['var_value'];
    }

    public function stats($type){

    	switch ($type) {
    		case 'total':
    			return $this->db->count_all('music_songs');
    		break;

    		case 'total_orders':
    		return $this->db->count_all('music_orders');
    		break;

    		case 'total_orders_c':
    		$this->db->where('played_at <>', '0');
    		$this->db->from('music_orders');
    		return $this->db->count_all_results();
    		break;

    		case 'last_order':
    		$query = $this->db->query('SELECT * FROM music_orders ORDER BY ID DESC LIMIT 1');
    		$query = $query->row_array();
    		return $query['ordered_at'];
    		default:
    			# code...
    			break;
    	}
    }
}
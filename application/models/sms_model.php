<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Sms_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function counts($type){

    	switch($type){

    		case 'total':
    		$this->db->from('sms');
			$count = $this->db->count_all_results();
    		return $count;
    		break;

    		case 'queue':
    		$this->db->where('status','0');
    		$this->db->from('sms');
			$count = $this->db->count_all_results();
    		return $count;
    		break;

    		case 'today':
    		$count = $this->db->query('SELECT COUNT(*) AS `count` FROM sms WHERE status=1 AND updated>= UNIX_TIMESTAMP(CURDATE())');
			$count = $count->row_array();
    		return $count['count'];
    		break;

    		case 'last_activity':
    		$this->db->select('updated');
    		$this->db->from('sms');
    		$this->db->order_by('id', "desc");
    		$this->db->limit(1);
    		$row = $this->db->get();
    		foreach($row->result() as $row){
    			return $row->updated;
    		}
    		break;
    	}
   }

   public function smses($append=null){
    $query = $this->db->query("SELECT * FROM sms $append ORDER BY id DESC");
    return $query->result();
   }

   public function senderApi($type='array'){
    $this->db->select('*');
    $this->db->from('sms_gateways');
    $this->db->where('status',1);
    $query = $this->db->get();
    if($type=='array'){
        $data = $query->row_array();
    }elseif($type=='result'){
        $data  = $query->result_array();
    }
    return $data;
   }

   public function insertSMS($chunk){
    foreach($chunk as $message){
        $data = $message;
        $this->db->insert('sms',$data);
    }
    
   }
}
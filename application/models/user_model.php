<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class User_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function user_basics($uid=false){
    	$user = $this->ion_auth->user()->row();
        if($uid){
            $user = $uid;
        }else{
            $user = $user->ref_id;
        }
    	$query = $this->db->query('SELECT * FROM users_basics WHERE ID='.$user.'');
    	$phone = $query->row_array();
                if($phone['mobile_status']==0){
                	$this->notify->warning('Your mobile number hasn\'t been verified, this will help us assist you better and keep you updated about future events. <a href="'.base_url().'user/verify_mobile" title="Send code" class="btn btn-sm btn-info">Send Code</a>');
                }
    }
    public function user_profile($ref_id,$row=false,$uid,$hash=''){
    	//$user = $this->ion_auth->user()->row();
        if($hash!=''){
            $query = $this->db->query('SELECT * FROM users_basics WHERE hash="'.$hash.'" ');
        }else{
            $query = $this->db->query('SELECT * FROM users_basics WHERE ID='.$ref_id.' AND UID='.$uid.' ');     
        }
        if($row){
            return $query->row_array();
        }else{
            return $query->result();
        }
    }

    public function update_profile($ref_id,$data){
    	$this->db->where('ID',$ref_id);
    	$this->db->update('users_basics',$data);
    }
    public function generate_otp($code,$ref_id){
    	$data = array(
    		'mobile_otp'=>$code,
    		);
    	$this->db->where('ID',$ref_id);
    	$this->db->update('users_basics',$data);
    }
    public function event_platforms($EID){
    	$query = $this->db->query("SELECT * FROM events_platforms WHERE EID=$EID");
    	return $query->result();
    }
    public function already_members($eid,$uid,$checked=0){
    	$this->db->where('EID',$eid);
    	$this->db->where('UID',$uid);
    	$this->db->where('deleted','0');
        if($checked==1){
            $this->db->where('checked_in',1);
        }
    	$this->db->from('events_members');
    	return $this->db->count_all_results();
    }
    public function add_members($eid,$alternate_user=0){
        if($alternate_user!=0){
            $user = $this->ion_auth->user($alternate_user)->row();
        }else{
            $user =  $this->ion_auth->user()->row();
        }	
        //$this->notify->info(''.$alternate_user.''.$user->id);
    	//if(isset($this->input->post('member',TRUE))){
    		
    		foreach($this->input->post('member') as $key=>$name)
    		{
    			if(empty($name)){

    			}else{
    				$last =  $this->db->query("SELECT PID FROM events_members WHERE UID='$user->id' AND EID='$eid' ORDER BY PID DESC LIMIT 1");
    				$last = $last->row_array();
    				//print_r($last);
    				$PID=1+$last['PID'];
                    $platform =  $this->input->post('platform',TRUE);
                    ///get limit for above posted platform :3
                    $attendee_limit = $this->idData($platform[$key],0,'session_attendee_limit');
                    $gender =  $this->input->post('member_gender',TRUE);
                    $email_m =  $this->input->post('member_email',TRUE);
                    $member_phone =  $this->input->post('member_phone',TRUE);
    				$data = array(
    					'UID'=>$user->id,
    					'PID'=>$PID,
    					'EID'=>$eid,
                        'platform' => $platform[$key],
                        'gender' => $gender[$key],
                        'email' => $email_m[$key],
                        'phone' => $member_phone[$key],
    					'name'=>ucwords($name),
    					'created_at' => time(),
    					);
                    $check_limited = $this->db->query("SELECT * FROM events_members WHERE platform='$platform[$key]' AND EID='$eid' AND UID='$user->id' AND deleted=0");
                    $check_limited = $check_limited->num_rows();
                    if($alternate_user==0){
                    if($check_limited>=$attendee_limit){
                        $session_name = $this->idData($platform[$key],$eid,'platform_name');
                        $this->notify->warning(htmlspecialchars($name).' can not be added, you have already added '.$check_limited.' attendess for '.$session_name);             
                    }else{
                        $add = $this->db->insert('events_members',$data);
                        $this->notify->success(htmlspecialchars($name).' has been added successfully.');             
                    }
                }else{
                    $add = $this->db->insert('events_members',$data);
                    $this->notify->success(htmlspecialchars($name).' has been added successfully.');
                    $plat = $this->input->post('platform',TRUE);
                }

    			}
               // $MID = $this->db->insert_id();
    		}

            /*foreach($this->input->post('platform',TRUE) as $p_id=>$pt_name){

                $data = array(
                    'platform'=>$pt_name,
                    );
                $this->db->where('UID', $user->id);
                $this->db->where('EID', $eid);
                $this->db->where('PID', $p_id);
               // $this->db->where('MID',$MID);
                $this->db->where('deleted', '0');
                $this->db->update('events_members', $data); 
            }    		

            foreach($this->input->post('member_gender',TRUE) as $g_id=>$gender){
    			$data = array(
    				'gender'=> $gender,
    				);
    			$this->db->where('UID', $user->id);
    			$this->db->where('EID', $eid);
    			$this->db->where('PID', $g_id);
               // $this->db->where('MID',$MID);
    			$this->db->where('deleted', '0');
    			$this->db->update('events_members', $data); 
    		}
    		foreach($this->input->post('member_email') as $e_id=>$email_e){
    			$data = array(
    				'email'=>$email_e,
    				);
    			$this->db->where('UID', $user->id);
    			$this->db->where('EID', $eid);
    			$this->db->where('PID', $e_id);
                //$this->db->where('MID',$MID);
                $this->db->where('deleted', '0');
    			$this->db->update('events_members', $data); 
    		}
    		foreach($this->input->post('member_phone',TRUE) as $c_id=>$contact_p){
    			$data = array(
    				'phone'=>$contact_p,
    				);
    			$this->db->where('UID', $user->id);
    			$this->db->where('EID', $eid);
    			$this->db->where('PID', $c_id);
                //$this->db->where('MID',$MID);   			
                $this->db->where('deleted', '0');
    			$this->db->update('events_members', $data); 
    		}*/
    		/// regenerate new PIDS for students
    		$refresh = $this->db->query("UPDATE events_members SET updated_at='".time()."',PID = (select @rownum:=@rownum+1 rownum FROM (SELECT @rownum:=0) r) WHERE deleted = 0 AND UID='$user->id' AND EID='$eid'");
    		//} // end if for $_POST
    	}
    	 public function update_members($eid){
    	$user =  $this->ion_auth->user()->row();
    	//if(isset($this->input->post('member',TRUE))){
    		
    		foreach($this->input->post('member') as $key=>$name)
    		{
    			if(empty($name)){
    				$delete = $this->db->query("UPDATE events_members SET deleted=1 WHERE UID='$user->id' AND PID='$key' AND EID='$eid'");
    			}else{
    				$data = array(
    					'name'=>ucwords($name),
    					);
    				$this->db->where('UID',$user->id);
    				$this->db->where('EID',$eid);
    			    $this->db->where('PID',$key);
    				$this->db->where('deleted','0');
    				$this->db->update('events_members',$data);
    				$this->notify->success('Details successfully updated!');
    			}
    		}
    		foreach($this->input->post('platform',TRUE) as $p_id=>$pt_name){
    			$data = array(
    				'platform'=>$pt_name,
    				);
    			$this->db->where('UID', $user->id);
    			$this->db->where('EID', $eid);
    			$this->db->where('PID', $p_id);
    			$this->db->where('deleted', '0');
    			$this->db->update('events_members', $data); 
    		}
            foreach($this->input->post('member_email') as $e_id=>$email_e){
                $data = array(
                    'email'=>$email_e,
                    );
                $this->db->where('UID', $user->id);
                $this->db->where('EID', $eid);
                $this->db->where('PID', $e_id);
                $this->db->where('deleted', '0');
                $this->db->update('events_members', $data); 
            }    		

            foreach($this->input->post('member_gender',TRUE) as $g_id=>$gender){
                $data = array(
                    'gender'=> $gender,
                    );
                $this->db->where('UID', $user->id);
                $this->db->where('EID', $eid);
                $this->db->where('PID', $g_id);
                $this->db->where('deleted', '0');
                $this->db->update('events_members', $data); 
            }
    		foreach($this->input->post('member_phone',TRUE) as $c_id=>$contact_p){
    			$data = array(
    				'phone'=>$contact_p,
    				);
    			$this->db->where('UID', $user->id);
    			$this->db->where('EID', $eid);
    			$this->db->where('PID', $c_id);
    			$this->db->where('deleted', '0');
    			$this->db->update('events_members', $data); 
    		}
    		/// regenerate new PIDS for students
    		$refresh = $this->db->query("UPDATE events_members SET updated_at='".time()."',PID = (select @rownum:=@rownum+1 rownum FROM (SELECT @rownum:=0) r) WHERE deleted = 0 AND UID='$user->id' AND EID='$eid'");
    		//} // end if for $_POST
    	}
        public function get_members($eid,$uid,$checked=0){
/*           $query = $this->db->query("SELECT * FROM events_members WHERE EID='$eid' AND UID='$uid' AND deleted = 0 AND checked_in=$checked ORDER BY PID ASC");
           return $query->result();*/

           $this->db->select("*");
           $this->db->from('events_members');
           if($eid)
            $this->db->where('EID',$eid);
           if($uid)
            $this->db->where('UID',$uid);
           if($checked==1)
            $this->db->where('checked_in',$checked);

           $this->db->where('deleted',0);
           $this->db->order_by('PID',"ASC");
           $get = $this->db->get();
           return $get->result();
        }    	

       /* 
       buggyyyyyy because i cant seem to understand what i am doing TODO: fix the join and return correct platform 
       public function get_membersJoined($eid,$uid){
            $this->db->select("*,events_members.name as memname,events_members.PID as memPID");
            $this->db->from('events_members');
            $this->db->where('events_members.EID',$eid);
            $this->db->where('events_members.UID',$uid);
            $this->db->where('events_members.deleted',0);
            $this->db->join('events_platforms','events_platforms.EID=events_members.EID');
            $this->db->order_by('events_members.PID','ASC');
            $this->db->group_by('events_platforms.');
            $query = $this->db->get();
    		return $query->result();
    	}*/

    	public function idData($entity,$entity2=0,$type){
    		switch($type){

    			case 'platform_name':
    			$query = $this->db->query("SELECT name FROM events_platforms WHERE PID=$entity AND EID=$entity2");
    			$query = $query->row_array();
    			return $query['name'];
    			break;    			

    			case 'event_name':
    			$query = $this->db->query("SELECT name FROM events WHERE EID=$entity");
    			$query = $query->row_array();
    			return $query['name'];
    			break;

    			case 'type_name':
    			$query = $this->db->query("SELECT name FROM events_types WHERE ID=$entity");
    			$query = $query->row_array();
    			return $query['name'];
    			break;
    			
    			case 'event_passed':
    			$yesterday = date('Y-m-d',time()-(24*60*60));
    			$date_epoch = date_create($yesterday); 
    			$date_epoch = date_format($date_epoch, 'U');
    			$check = $this->db->query("SELECT COUNT(EID) as `count` FROM events WHERE EID='$entity' AND date_epoch>$date_epoch");
    			$check = $check->row_array();
    			$check = $check['count'];
    			if($check==0){
    				return false;
    			}else{
    				return true;
    			}
    			break;

    			case 'event_participating':
    			$check = $this->db->query("SELECT COUNT(PID) as `count` FROM events_members WHERE EID='$entity' AND UID='$entity2' AND deleted=0");
    			$check = $check->row_array();
    			$check = $check['count'];
    			if($check>0){
    				return true;
    			}else{
    				return false;
    			}
    			break;

                case 'session_ticket_name':
                $this->db->select("*");
                $this->db->from('events_tickets');
                $this->db->where('events_tickets.platform_ID',$entity);
                $this->db->join('fees_tickets','events_tickets.ticket_id=fees_tickets.ID');
                $get = $this->db->get();
                return $get->row_array();
                break;

                case 'session_amount':
                $query = $this->db->query("SELECT amount FROM events_tickets WHERE platform_ID=$entity");
                $query = $query->row_array();
                return $query['amount'];
                break;                

                case 'session_attendee_limit':
                $query = $this->db->query("SELECT attendee_limit FROM events_platforms WHERE PID=$entity");
                $query = $query->row_array();
                return $query['attendee_limit'];
                break;
    		}
    	}
    	public function get_events($extra=''){
    		$query = $this->db->query('SELECT DISTINCT(EID) FROM events_members '.$extra.'');
    		return $query->result();
    	}

        public function users($status='all',$type='all',$profile='all',$append = null){

            if($type !='all'){
                $this->db->select("*");
                $this->db->from('users');
                $this->db->join('users_basics','users_basics.ID=users.ref_id');
                if($type=='team'){
                    $this->db->where('users_basics.individual',0);
                }else{
                    $this->db->where('users_basics.individual',1); 
                }
                if($status =='active')
                    $this->db->where('users.active',1);
                if($status  =='inactive')
                    $this->db->where('users.active',0);
                if($profile=='incomplete'):
                    $this->db->where('users_basics.occupation','');
                    $this->db->or_where('users_basics.upto_class','');
                    $this->db->or_where('users_basics.address','');
                    $this->db->or_where('users_basics.district','');
                    $this->db->or_where('users_basics.t_org','');
                endif;
            }else{
                $this->db->select("*");
                $this->db->from('users');
                if($status =='active')
                    $this->db->where('users.active',1);
                if($status  =='inactive')
                    $this->db->where('users.active',0);            
            }
            if($append){
                 $this->db->where($append);
            }

            $get = $this->db->get();
            return $get->result();
        }
}
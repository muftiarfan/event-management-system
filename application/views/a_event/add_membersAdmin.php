<script type="text/javascript">

function fetch_select(val)
{
   $.ajax({
     type: 'post',
     url: baseUrl+'event/add_membersAdminAjax',

     data: {
       get_option:val,
       <?php echo $this->security->get_csrf_token_name();?> : '<?php echo $this->security->get_csrf_hash();?>',
       EID:<?php echo $EID;?>,
     },
     success: function (response) {
       //document.getElementById("post-response-relation").append(response); 
       $('#post-response-relation').empty();
       $('#post-response-relation').append(response);
     }
   });
}

</script>
<div class="col-lg-12">
<?php
$this->load->view('a_event/toolbar');
?>
<h2><span class="label label-success label-lg">1</span> Type</h2>
  <div class="form-group">
    <select class="form-control" onchange="fetch_select(this.value);">
           <option>
              Select Type
           </option>
           <option value="registered">Registered</option>
           <option value="unregistered">Unregistered</option>
     </select>
    </div>
    <div id="post-response-relation">

</div>
</div>
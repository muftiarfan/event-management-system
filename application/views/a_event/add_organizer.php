<div class="container">

<div class="col-lg-12">
<?php
$this->load->view('a_event/toolbar');
?>
<?php
//echo $this->notify->show();
?>
<?php echo form_open('event/event_organizers','class="form-horzontal"'); ?>

<div class="form-group">
<div class="col-lg-6">
<label for="org_name">Name</label>
<input type="text" class="form-control input-md" name="org_name" placeholder="Event Organizer Name">
</div>
<div class="col-lg-6">
<label for="org_address">Address</label>
<input type="text" class="form-control input-md" name="org_address" placeholder="Address">
</div>
</div>
<div class="form-group">
<div class="col-lg-6">
<label for="org_contact">Contact</label>
<input type="text" class="form-control input-md" name="org_contact" placeholder="Phone/Mobile">
</div>
<div class="col-lg-6">
<label for="org_email">Email</label>
<input type="email" class="form-control input-md" name="org_email" placeholder="email@company.com">
</div>
</div>
<div class="form-group">
<div class="col-lg-12" style="margin-top:15px;">
<button type="submit" class="btn btn-success btn-md">Add</button>
</div>
</div>
<?php echo form_close(); ?>

</div>

</div>
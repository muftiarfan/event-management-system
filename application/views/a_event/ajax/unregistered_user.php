<div class="col-lg-12">
<h1>User <small> Details</small></h1>
<h2><span class="label label-success label-lg">2</span> Basic Details</h2>
<?php echo form_open('register/'.$eid,'class="form-horizontal"');?>
<div class="form-group">
<div class="col-lg-7">
<label for="name">Name</label>
<input type="text" class="form-control" name="name" id="name" placeholder="Full Name" value="">
</div>
<div class="col-lg-5">
<label for="email">Email</label>
<input type="email" class="form-control" name="email" id="email" placeholder="email@company.com" value="">
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<label for="t_org">Team/Organization</label>
<input type="text" class="form-control" name="t_org" id="t_org" placeholder="Name of your team/organization" value="">
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<label for="mobile">Phone</label>
<input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="">
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<button class="btn btn-success btn-lg">Register</button>
</div>
</div>
<?php echo form_close();?>
</div>
</div>
<?php
$this->load->view('a_event/toolbar');
?>
<div class="col-md-2">
<select class="form-control" id="evnt" onchange="fetch_select(this.value)">
<option value="0" disabled="disabled" selected="selected">Select Event</option>
<?php foreach($events as $event):?>
<option value="<?php echo $event->EID;?>"><?php echo $event->name;?></option>
<?php endforeach;?>
</select>
</div>
<div id="event_users_sessions" style="display:none;">
</div>
<div class="col-md-2">
<select class="form-control" id="gender">
<option value="all">All Genders</option>
<option value="Male">Male</option>
<option value="Female">Female</option>
</select>
</div>
<div class="col-md-2">
<select class="form-control" id="checked">
<option value="all">All Attendees</option>
<option value="1">Checked In</option>
<option value="0">Not Checked In</option>
</select>
</div>
<a id="search" href="view" class="btn btn-success" target="_blank" role="button"><i class="fa fa-search"></i></a>
 <script type="text/javascript">$(document).ready(function(){

                    function changeLink()
                    {
                    	
                    	var evnt = $("#evnt").val();
                    	var usr = $("#usr").val();
                    	var sess = $("#session").val();
                    	var chk = $("#checked").val();
                    	var gender = $("#gender").val();
                        if(!evnt) evnt = "0";
                        if(!usr) usr = "all";
                        if(!sess) sess = "all";
                        if(!chk) chk = "all";
                        if(!gender) gender = "all";
                        $("#search").attr("href", baseUrl+"event/attendee_listPrint/"+evnt+"/"+sess+"/"+gender+"/"+chk+"/"+usr);
                        
                    }
					$("#search").on("mousedown", function(){
						changeLink();
                    });	
			});
 				function fetch_select(val)
				{
					$.ajax({
						type: 'post',
						url: baseUrl+'event/ajax_UserSession',
						async: true,
						cache:false,
						data: {
							<?php echo $this->security->get_csrf_token_name();?> : '<?php echo $this->security->get_csrf_hash();?>',
							EID:val,
						},
						success: function (response) {
							$("#event_users_sessions").show();
							$("#event_users_sessions").html(response);
						}
					});
				}
 </script>
<div class="container" style="width:29.7cm;">
<h1>EMS <small>Attendee List :: <?php echo $event['name'];?></small></h1>
<div class="table-responsive" style="margin-top:4px;">
<table class="table table-striped table-bordered">
<thead>
<th class="text-left">#</th>
<th class="text-left">Name</th>
<th>Team/Org</th>
<th>Session</th>
</thead>
<tbody>
<?php 
$i=1;
foreach($attendees as $attendee):
$platform_name = $this->userM->idData($attendee->platform,$attendee->EID,'platform_name');
$user = $this->ion_auth->user($attendee->UID)->row();
$user_basics = $this->userM->user_profile($user->ref_id,true,$user->id);
?>
<tr>
<td><?php echo $i++;?></td>
<td class="text-left"><?php echo $attendee->name;?></td>
<td class="text-left"><?php echo $user_basics['t_org'];?> <strong><?php if($user_basics['individual']==1){echo '(Individual)';}?></strong></td>
<td class="text-left"><?php echo $platform_name;?></td>
</tr>
<?php endforeach; ?>
</tbody>
</table>
</div>
<div class="col-lg-12 text-center text-muted">
		E: <?php echo $event['EID'];?>
        S: <?php echo htmlspecialchars($session);?>
        G: <?php echo htmlspecialchars($gender);?>
        C: <?php echo htmlspecialchars($checked_in);?>
        U: <?php echo htmlspecialchars($p_user);?>
        UID: <?php echo  $this->ion_auth->user()->row()->id;?>
        G: <?php echo date('d-m-y H:i A');?>
</div>
</div>
<div class="col-lg-12">
<h3><strong>Event in a nutshell</strong> <small><?php echo $event['name'];?></small></h3>
<div class="row">

<div class="col-lg-3">

<div class="thumbnail">
<h2 class="text-center"><?php echo $this->eventM->event_statistics($event['EID'],'registrations_users');?></h2>
<h3 class="caption text-center"><strong>REGISTRATIONS</strong></h3>


</div>
</div>

<div class="col-lg-3">

<div class="thumbnail">
<h2 class="text-center"><?php echo $this->eventM->event_statistics($event['EID'],'registrations_att');?></h2>
<h3 class="caption text-center"><strong>ATTENDEES</strong></h3>


</div>
</div>

<div class="col-lg-3">

<div class="thumbnail">
<h2 class="text-center"><?php echo $this->eventM->event_statistics($event['EID'],'registrations_indi');?></h2>
<h3 class="caption text-center"><strong>INDIVIDUAL</strong></h3>


</div>
</div>
<div class="col-lg-3">

<div class="thumbnail">
<h2 class="text-center"><?php echo $this->eventM->event_statistics($event['EID'],'registrations_checked');?></h2>
<h3 class="caption text-center"><strong>CHECKED IN</strong></h3>


</div>
</div>

</div>

<div class="row">

<div class="col-lg-6">

<div class="panel panel-info">
<div class="panel-heading"><strong>Sessions</strong></div>
<div class="panel-body">
<table class="table table-hover table-striped">

<thead>
<tr>
<th>Name</th>
<th>Registrations</th>
<th>Checked In</th>
</tr>
</thead>
<tbody>
<?php foreach($platforms as $session): ?>
<tr>
<td><?php echo $session->name;?></td>
<td><a target="_blank" title="Print attendee list for registered users" href="<?php echo base_url();?>event/attendee_listPrint/<?php echo $session->EID.'/'.$session->PID.'/all/all/all';?>"><?php echo $this->eventM->session_counts($session->PID,$session->EID,'registrations');?></a></td>
<td><a target="_blank" title="Print attendee list for checked in users" href="<?php echo base_url();?>event/attendee_listPrint/<?php echo $session->EID.'/'.$session->PID.'/all/1/all';?>"><?php echo $this->eventM->session_counts($session->PID,$session->EID,'checked_in');?></td>
</tr>
<?php endforeach;?>
</tbody>
</table>
</div>
</div>

</div>
<div class="col-lg-6">

<div class="panel panel-info">
<div class="panel-heading"><strong>Attendees</strong></div>
<div class="panel-body">
<ul class="nav nav-tabs">
  <li class="active"><a data-toggle="tab" href="#all">All&nbsp;<span class="badge"><?php echo $this->eventM->event_statistics($event['EID'],'registrations_users');?></span></a></li>
  <li><a data-toggle="tab" href="#teams">Teams&nbsp;<span class="badge"><?php echo $this->eventM->event_statistics($event['EID'],'registrations_users_T');?></span></a></li>
  <li><a data-toggle="tab" href="#individuals">Individuals&nbsp;<span class="badge"><?php echo $this->eventM->event_statistics($event['EID'],'registrations_indi');?></span></a></li>
</ul>

<div class="tab-content">
  <div id="all" class="tab-pane fade in active">
    <h3>All</h3>
    <table class="table table-hover table-striped">

<thead>
<tr>
<th>Name</th>
<th>Registrations</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<?php
$attendees = $this->eventM->get_usersEvent($event['EID']);
foreach($attendees as $attendee):
	$user = $this->ion_auth->user($attendee->UID)->row();
	$profile = $this->userM->user_profile($user->ref_id,true,$user->id);
	$count = $this->db->query('SELECT COUNT(PID) as `count` FROM events_members WHERE deleted=0 AND EID='.$event['EID'].' AND UID='.$user->id.'');
	$count = $count->row_array();
?>
<tr>
<td><a href="<?php echo base_url();?>user/profile/<?php echo $user->ref_id.'/'.$user->id;?>" title="View Profile"><?php echo $profile['full_name'];?></a>
<p class="text-muted"><?php echo $profile['t_org'];?></p>
</td>
<td><a target="_blank" title="Print attendee list for registered users" href="<?php echo base_url();?>event/attendee_listPrint/<?php echo $event['EID'].'/all/all/all/'.$attendee->UID;?>"><?php echo $count['count'];?></a></td>
<td><?php if($profile['individual']==1){ echo 'Individual';}else{echo 'Team';}?></td>
</tr>
<?php endforeach;?>
</tbody>
</table>
  </div>
  <div id="teams" class="tab-pane fade">
    <h3>Teams</h3>
   <table class="table table-hover table-striped">

<thead>
<tr>
<th>Name</th>
<th>Registrations</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<?php
$attendees_t = $this->eventM->get_usersEvent($event['EID'],'AND icard_path IS NULL');
foreach($attendees_t as $attendee):
	$user = $this->ion_auth->user($attendee->UID)->row();
	$profile = $this->userM->user_profile($user->ref_id,true,$user->id);
	$count = $this->db->query('SELECT COUNT(PID) as `count` FROM events_members WHERE deleted=0 AND EID='.$event['EID'].' AND UID='.$user->id.'');
	$count = $count->row_array();
?>
<tr>
<td><a href="<?php echo base_url();?>user/profile/<?php echo $user->ref_id.'/'.$user->id;?>" title="View Profile"><?php echo $profile['full_name'];?></a>
<p class="text-muted"><?php echo $profile['t_org'];?></p>
</td>
<td><a target="_blank" title="Print attendee list for registered users" href="<?php echo base_url();?>event/attendee_listPrint/<?php echo $event['EID'].'/all/all/all/'.$attendee->UID;?>"><?php echo $count['count'];?></a></td>
<td><?php if($profile['individual']==1){ echo 'Individual';}else{echo 'Team';}?></td>
</tr>
<?php endforeach;?>
</tbody>
</table>
  </div>
  <div id="individuals" class="tab-pane fade">
    <h3>Individuals</h3>
    <table class="table table-hover table-striped">

<thead>
<tr>
<th>Name</th>
<th>Registrations</th>
<th>Type</th>
</tr>
</thead>
<tbody>
<?php
$attendees_t = $this->eventM->get_usersEvent($event['EID'],'AND icard_path IS NOT NULL');
foreach($attendees_t as $attendee):
	$user = $this->ion_auth->user($attendee->UID)->row();
	$profile = $this->userM->user_profile($user->ref_id,true,$user->id);
	$count = $this->db->query('SELECT COUNT(PID) as `count` FROM events_members WHERE deleted=0 AND EID='.$event['EID'].' AND UID='.$user->id.'');
	$count = $count->row_array();
?>
<tr>
<td><a href="<?php echo base_url();?>user/profile/<?php echo $user->ref_id.'/'.$user->id;?>" title="View Profile"><?php echo $profile['full_name'];?></a>
<p class="text-muted"><?php echo $profile['t_org'];?></p>
</td>
<td><a target="_blank" title="Print attendee list for registered users" href="<?php echo base_url();?>event/attendee_listPrint/<?php echo $event['EID'].'/all/all/all/'.$attendee->UID;?>"><?php echo $count['count'];?></a></td>
<td><?php if($profile['individual']==1){ echo 'Individual';}else{echo 'Team';}?></td>
</tr>
<?php endforeach;?>
</tbody>
</table>
  </div>
</div>
</div>
</div>

</div>

</div>
<div class="row">

<div class="col-lg-12">
<?php
$male = $this->eventM->event_statistics($event['EID'],'registrations_male');
$female = $this->eventM->event_statistics($event['EID'],'registrations_female');
$total = $this->eventM->event_statistics($event['EID'],'registrations_att');
$male_p = ($male/$total)*100;
$female_p = ($female/$total)*100;
?>
<div class="panel panel-success">
<div class="panel-heading"><strong>Other Info</strong></div>
<div class="panel-body">
<div class="row">
<div class="col-lg-12">
<h4>Male:Female</h4>
<div class="progress">
  <div class="progress-bar progress-bar-success progress-bar-striped active" style="width: <?php echo round($male_p);?>%">
    <span><?php echo round($male_p);?>% Males</span>
  </div>
  <div class="progress-bar progress-bar-warning progress-bar-striped active" style="width: <?php echo round($female_p);?>%">
    <span><?php echo round($female_p);?>% Females</span>
  </div>
</div>
<h4>Sessions</h4>
<div class="progress">
<?php 
$total_attendees =  $this->eventM->event_statistics($event['EID'],'registrations_att');
foreach($platforms as $session): 
	$session_count = $this->eventM->session_counts($session->PID,$session->EID,'registrations');
$percent = ($session_count/$total_attendees)*100;
$num = rand(1,5);	
switch($num){

	case 1:
	$type = 'success';
	break;

	case 2:
	$type = 'danger';
	break;

	case 3:
	$type = 'info';
	break;

	case 4:
	$type  = 'warning';
	break;

	default:
	$type = 'primary';
	break;
}
?>
  <div class="progress-bar active progress-bar-<?php echo $type;?> progress-bar-striped" style="width: <?php echo $percent;?>%">
    <span><?php echo round($percent);?>% <?php echo $session->name;?></span>
  </div>
<?php //endforeach;?>
<?php endforeach;?>
</div>
</div>
</div>
</div>
</div>

</div>

</div>
</div>
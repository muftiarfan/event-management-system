<div class="col-lg-12">
<?php
$this->load->view('a_event/toolbar');
?>
<?php echo form_open_multipart('event/edit/'.$this->uri->segment(3),'class="form-horizontal"');?>
<div class="form-group <?php if(form_error('event_name')){echo 'has-error';} ?>">
    <div class="col-lg-7">
    <label for="event_name">Event name</label><?php if(form_error('event_name')):?><span class="text text-danger fa fa-question-circle fa-2x" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo form_error('event_name');?>"></span><?php endif;?>
    <input type="text" class="form-control input-lg" id="event_name" name="event_name" placeholder="Name of the event" value="<?php echo $event['name'];?>">
    </div>
</div>
<div class="form-group <?php if(form_error('event_date')){echo 'has-error';} ?>">
            <div class="col-sm-6">
                <label for="event_date">Date</label><?php if(form_error('event_date')):?><span class="text text-danger fa fa-question-circle fa-2x" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo form_error('event_date');?>"></span><?php endif;?>
                <div class="input-group date" id='datetimepicker1'>
    <input type="text" class="form-control input-lg" id="event_date" name="event_date" placeholder="Event Date" value="<?php echo $event['event_date'];?>">
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="col-sm-6">
            <label for="event_location">Venue </label><?php if(form_error('event_location')):?><span class="text text-danger fa fa-question-circle fa-2x" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo form_error('event_location');?>"></span><?php endif;?>
            <input type="text" name="event_location" id="event_location" class="form-control input-lg" placeholder="Venue" value="<?php echo $event['location'];?>">
            </div>            
</div>
<div class="form-group">
<div class="col-lg-12">
            <label for="event_org">Organizer </label><?php if(form_error('event_org')):?><span class="text text-danger fa fa-question-circle fa-2x" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo form_error('event_org');?>"></span><?php endif;?>
           <select class="event_org form-control" name="event_org" id="event_org">
           </select> 
</div>
</div>
<div class="form-group <?php if(form_error('event_desc')){echo 'has-error';} ?>">
    <div class="col-lg-12">
    <label for="event_desc">Event Description</label><?php if(form_error('event_desc')):?><span class="text text-danger fa fa-question-circle fa-2x" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo form_error('event_desc');?>"></span><?php endif;?>
    <textarea class="form-control wysihtml5" placeholder="What the event is about?" name="event_desc" id="event_desc"><?php echo $event['description'];?></textarea>
    </div>
</div>
<div class="form-group <?php if(form_error('event_ticket_extra_info')){echo 'has-error';} ?>">
    <div class="col-lg-12">
    <label for="event_ticket_extra_info">Ticket Information</label><?php if(form_error('event_desc')):?><span class="text text-danger fa fa-question-circle fa-2x" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo form_error('event_ticket_extra_info');?>"></span><?php endif;?>
    <textarea class="form-control wysihtml5" placeholder="Extra information/instructions to be shown on ticket." name="event_ticket_extra_info" id="event_ticket_extra_info"><?php echo $event['ticket_extra_info'];?></textarea>
    </div>    
</div>
<div class="form-group <?php if(form_error('registration_extra_info')){ echo 'hash-error';}?>">
    <div class="col-lg-12">
    <label for="registration_extra_info">Registration Info</label><?php if(form_error('registration_extra_info')):?><span class="text text-danger fa fa-question-circle fa-2x" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo form_error('registration_extra_info');?>"></span><?php endif;?>
    <textarea class="form-control" placeholder="Any relevant instructions to be shown at time of filling form for this event." name="registration_extra_info" id="registration_extra_info"><?php echo $event['registration_extra_info'];?></textarea>
    </div>
</div>
<div class="form-group">
<div class="col-lg-4">
<label for="icard_header_img">I-Card Header Image <small>Dimensions: 1549x339</small></label>
<input type="file" name="icard_header_img" id="icard_header_img">
</div>
<div class="col-lg-4">
<label for="ticket_logo">Ticket Header Logo <small>Dimensions: 884x215</small></label>
<input type="file" name="ticket_logo" id="ticket_logo">
</div>
<div class="col-lg-4">
<label for="ticket_logo_footer">Ticket Footer Logo <small>Dimensions: 2500x280</small></label>
<input type="file" name="ticket_logo_footer" id="ticket_logo_footer">
</div>
</div>
<div class="form-group <?php if(form_error('event_cats')){echo 'has-error';} ?>">
    <div class="col-lg-12">
    <table class="table table-striped table-hover">
    <thead>
    <tr>
    <th>#</th>
    <th>Name</th>
    <th>Actions</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach($event_platforms as $platforms):?>
        <tr>
        <td><?php echo $platforms->PID;?></td>
        <td>
        <a href="#" class="session_plat" data-type="text" data-name="session_rn" data-pk="<?php echo $platforms->PID;?>" data-url="<?php echo base_url();?>event/update_ajax" data-title="Session Name"><?php echo $platforms->name;?></a>
        </td>
        <td>
            <div class="btn-group">

            <a href="<?php echo base_url();?>event/delete_platform/<?php echo $platforms->PID;?>" class="btn btn-sm btn-danger"><span class="fa fa-trash fa-w"></span></a>
            </div>
        </td>
        </tr>
    <?php endforeach;?>
    </tbody>
    </table>
</div>
</div>
<div class="form-group <?php if(form_error('event_members')){echo 'has-error';} ?>">
    <div class="col-lg-2">
    <label for="event_members">Max Participants</label><?php if(form_error('event_members')):?><span class="text text-danger fa fa-question-circle fa-2x" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo form_error('event_members');?>"></span><?php endif;?>
    <input type="number" class="form-control input-md" id="event_members" name="event_members" placeholder="Members" value="<?php echo $event['participants'];?>">
    </div>
</div>
<div class="form-group <?php if(form_error('event_type')){echo 'has-error';} ?>">
    <div class="col-lg-2">
    <label for="event_type">Type</label><?php if(form_error('event_type')):?><span class="text text-danger fa fa-question-circle fa-2x" data-toggle="tooltip" data-placement="right" title="" data-original-title="<?php echo form_error('event_type');?>"></span><?php endif;?>
    <select class="form-control" id="event_type" name="event_type">
    <?php foreach($e_types as $type):?>
    <option value="<?php echo $type->ID;?>"><?php echo $type->name;?></option>
    <?php endforeach;?>
    </select>    
    </div>
</div>
<div class="form-group">
<div class="col-lg-12">
<label>Tickets Applicable</label>
<div class="checkbox">
<?php foreach($fee_tickets as $ticket):?>
  <label><input type="checkbox" name="event_tickets[]" value="<?php echo $ticket->ID;?>"><?php echo $ticket->name;?></label>
<?php endforeach;?>
</div>
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<button type="submit" class="btn btn-lg btn-success">Update</button>
</div>
</div>
<?php echo form_close(); ?>
</div>
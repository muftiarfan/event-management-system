<div class="col-lg-12">
<?php
$this->load->view('a_event/toolbar');
?>
<div class="row">
<div class="col-lg-12">
<div class="panel panel-primary">
<div class="panel-heading">
<h4>Members
<small>
Members added by
<?php
foreach($members as $member):
$user_table =  $this->ion_auth->user($member->UID)->row();
$profiles = $this->userM->user_profile($user_table->ref_id,false,$member->UID);
$event = $this->userM->idData($member->EID,0,'event_name');
endforeach;
foreach($profiles as $profile):
echo '<strong><a href="'.base_url().'user/profile/'.$profile->ID.'/'.$member->UID.'">'.$profile->full_name.'</a></strong>';
echo ' for <strong><a href="'.base_url().'event/icard/users/'.$eid.'">'.$event.'</a></strong> ';
endforeach;
?>
</small>
</h4>
<div class="pull-right">
<a href="<?php echo base_url();?>event/icard/user/<?php echo $member->UID;?>/<?php echo $eid;?>/batch" class="btn btn-success btn-sm">Batch Print</a>
</div>
<div class="clearfix"></div>
</div>
<div class="panel-body">
<ul class="list-group">
<?php 
foreach($members as $member):
	if($member->checked_in==1){
		$type = 'uncheckin';
	}elseif($member->checked_in==0){
		$type = 'checkin';
	}else{
		$type = 'checkin';
	}
?>
<li class="list-group-item">
<a class="att_name" data-pk="<?php echo $member->MID;?>" data-url="<?php echo base_url();?>event/icard_ajax_edit/member_name"><?php echo $member->name;?></a>
<div class="btn-group pull-right">
<a data-toggle="confirm-modal" data-confirm-title="Delete member?"
                    data-confirm-message="Do you really want to delete <strong><?php echo $member->name;?></strong>?"
                    data-confirm-ok="Yes!" data-confirm-cancel="Changed my mind" 
                    class="btn btn-xs btn-danger" title="Make inactive" href="<?php echo base_url();?>event/delete_member/<?php echo $member->MID;?>/<?php echo $member->EID;?>/<?php echo $member->UID;?>"><span class="fa fa-trash"></span></a>
<a title="Print ICard" target="_blank" class="checkin btn btn-xs btn-primary" href="<?php echo base_url();?>event/icard/user/<?php echo $member->UID;?>/<?php echo $eid;?>/member/<?php echo $member->PID;?>"><span class="fa fa-qrcode"></span></a>
<button id="ch_<?php echo $member->MID;?>" data-t="<?php echo $type;?>" data-eid="<?php echo $member->EID;?>" data-mid="<?php echo $member->MID;?>" class="checkin btn btn-xs btn-default"><?php echo ucfirst($type);?></button>
</div>
<p>
Platform:
<span class="text text-muted">

<a class="att_sess" data-value="<?php echo $member->platform;?>" data-url="<?php echo base_url();?>event/icard_ajax_edit/member_session" data-pk="<?php echo $member->MID;?>">
<?php 
echo $this->userM->idData($member->platform,$member->EID,'platform_name');
?>
</a>
</span>
</p>
<p>
<small id="checkin_info_<?php echo $member->MID;?>" class="text-muted"><?php if($member->checked_in==1): echo 'Checked in at: ',unix_to_human($member->checked_in_at); endif;?></small>
</p>
</li>
<?php endforeach;?>
</ul>
</div>
</div>
</div>
</div> <!--end row-->
</div>
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add/Modify Attendees</h4>
      </div>
      <div class="modal-body">
      <ul class="nav nav-tabs">
  <li><a data-toggle="tab" href="#added">Added</a></li>
  <li class="active"><a data-toggle="tab" href="#new">Add New</a></li>
</ul>

<div class="tab-content">
  <div id="added" class="tab-pane fade in">
    <h4>Already Added</h4>
    <hr>
<?php echo form_open('user/event_edit/'.$eid,'class="form-horizontal"');?>
<input type="hidden" name="UID_hash" value="<?php echo $uid_basics['hash'];?>">
<input type="hidden" name="admin_add" value="1">
<?php
foreach($members as $member):
//while($i<$event->participants):
//	$i = $i+1;
//while($i<$event->participants):
//	$i++;
?>
<h4><strong>Member <?php echo $member->PID;?></strong></h4>
<div class="form-group">
<div class="col-lg-4">
<label for="member[<?php echo $member->PID;?>]">Name</label>
<input type="text" name="member[<?php echo $member->PID;?>]" placeholder="Name" class="form-control" value="<?php echo $member->name;?>">
</div>
<div class="col-lg-3">
<label for="member_email[<?php echo $member->PID;?>]">Email</label>
<input type="Email" name="member_email[<?php echo $member->PID;?>]" placeholder="Email" class="form-control" value="<?php echo $member->email;?>">
</div>
<div class="col-lg-2">
<label for="member_phone[<?php echo $member->PID;?>]">Phone</label>
<input type="text" name="member_phone[<?php echo $member->PID;?>]" placeholder="Mobile" class="form-control" value="<?php echo $member->phone;?>">
</div>
<div class="col-lg-1">
<label for="member_gender[<?php echo $member->PID;?>]">Gender</label>
<select class="form-control" id="member_gender[<?php echo $member->PID;?>]" name="member_gender[<?php echo $member->PID;?>]">

<option value="<?php echo $member->gender;?>" selected><?php echo $member->gender;?></option>
<option disabled value="0">Change?</option>
<option value="Male">Male</option>
<option value="Female">Female</option>
</select>
</div>
<div class="col-lg-2">
<label for="member_phone[<?php echo $member->PID;?>]">Session</label>
<select class="form-control" id="platform[<?php echo $member->PID;?>]" name="platform[<?php echo $member->PID;?>]">
<?php
$platform_name = $this->userM->idData($member->platform,$eid,'platform_name');
?>
<option value="<?php echo $member->platform;?>" selected><?php echo $platform_name;?></option>
<option disabled value="0">Other</option>
<?php foreach($platforms as $platform):?>
<option value="<?php echo $platform->PID;?>"><?php echo $platform->name;?></option>
<?php endforeach;?>
</select>
</div>
</div>
<?php endforeach; ?>
<?php //endwhile; ?>
<div class="col-lg-4">
<button class="btn btn-lg btn-success" type="submit">Update</button>
</div>
<?php echo form_close();?>  </div>
  <div id="new" class="tab-pane fade in active">
    <h4>Adding more attendees</h4>
    <hr>
<?php
if($test['cnt']<0){ //for incriminting purposes.
	$i=0;
}else{
	$i=$test['cnt'];
}
?>
<?php echo form_open('user/event_register/'.$eid,'class="form-horizontal"');?>
<input type="hidden" name="UID_hash" value="<?php echo $uid_basics['hash'];?>">
<input type="hidden" name="admin_add" value="1">
<?php
while($i<30):
	$i = $i+1;
//while($i<$event->participants):
//	$i++;
?>
<h4><strong>Member <?php echo $i;?></strong></h4>
<div class="form-group">
<div class="col-lg-4">
<label for="member[<?php echo $i;?>]">Name</label>
<input type="text" name="member[<?php echo $i;?>]" placeholder="Name" class="form-control">
</div>
<div class="col-lg-3">
<label for="member_email[<?php echo $i;?>]">Email</label>
<input type="Email" name="member_email[<?php echo $i;?>]" placeholder="Email" class="form-control">
</div>
<div class="col-lg-2">
<label for="member_phone[<?php echo $i;?>]">Phone</label>
<input type="text" name="member_phone[<?php echo $i;?>]" placeholder="Mobile" class="form-control">
</div>
<div class="col-lg-1">
<label for="member_gender[<?php echo $i;?>]">Gender</label>
<select class="form-control" id="member_gender[<?php echo $i;?>]" name="member_gender[<?php echo $i;?>]">
<option value="Male">Male</option>
<option value="Female">Female</option>
</select>
</div>
<div class="col-lg-2">
<label for="platform[<?php echo $i;?>]">Session</label>
<select class="form-control" id="platform[<?php echo $i;?>]" name="platform[<?php echo $i;?>]">
 <option value="0" selected disabled>Session</option>
<?php foreach($platforms as $platform):?>
<option value="<?php echo $platform->PID;?>"><?php echo $platform->name;?></option>
<?php endforeach;?>
</select>
</div>
</div>
<?php endwhile; ?>
<div class="col-lg-4">
<button class="btn btn-lg btn-success" type="submit">Add</button>
</div>
<?php echo form_close();?>  </div>

</div>
      
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
    <script>
      $(".checkin").click(function(){
      	var MID = $(this).data('mid');
      	var T = $(this).attr('data-t');
      	console.log(MID+T)
      	$.ajax({
      		type: "POST",
      		url: baseUrl+"event/member_checkin",
      		data: {
      			mid: $(this).data('mid'),
      			eid: $(this).data('eid'),
      			t: $(this).attr('data-t'),
      		},
      		dataType: "html",
      		success:function(data){
      				//something
      				//console.log('hello');
      				if(T=='checkin'){
      					$("#ch_"+MID).attr('data-t', 'uncheckin');
      					$("#ch_"+MID).text('Uncheckin');
      					$("#checkin_info_"+MID).html(data);
      				}else if(T=='uncheckin'){
      					$("#ch_"+MID).attr('data-t', 'checkin');
      					$("#ch_"+MID).text('Checkin');
      					$("#checkin_info_"+MID).empty();
      				}
      				
      			}
      	})
      })
    </script>
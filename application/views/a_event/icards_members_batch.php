<!--body style="background:#FFF;"-->
<?php
use Endroid\QrCode\QrCode;
?>
<div class="idcard-wrapper">
<?php
foreach($members as $member):
$name_md5 = md5($member->EID.$member->PID.$member->UID.$member->created_at);
$qrCode = new QrCode();
$qrCode
    ->setText($member->EID.'-'.$member->PID.'-'.$member->checked_in_at.'-'.$member->UID)
    ->setSize(90)
    ->setPadding(4)
    ->setErrorCorrection('high')
    ->setForegroundColor(array('r' => 0, 'g' => 0, 'b' => 0, 'a' => 0))
    ->setBackgroundColor(array('r' => 255, 'g' => 255, 'b' => 255, 'a' => 0))
    ->render('uploads/qrcodes/'.$name_md5.'.png','png');
?>
<div class="idcard idcard-copy">
  <div class="ids">

<div class="text-center" style="margin-bottom:1%;margin-top:-4%;">
<img src="<?php echo base_url();?>assets/images/emscard.png" style="width:90%;">
</div>
  </div>
  <div class="id-head">
    <h4>ID Card</h4>
  </div>
  <table>
    <tr>
      <td><strong>PID</strong></td><td><strong>Team/Org</strong></td>
    </tr>
    <tr>
      <td><?php echo 'EMS/'.$member->PID;?></td><td> <?php
    echo $uid_basics['t_org'];?>
    </td>
      
    </tr>

  </table>
<div>

</div>
  <table>
    <tr>
      <td>
      <?php
        //$filename = $PNG_TEMP_DIR.$sid.'_'.$eid.'_'.$pid.'.png';
        //QRcode::png('Cyber Crew,EMS/'.$sid.'/'.$eid.'/'.$pid, $filename, 'L', 3, 2);
        ?>
      <img class="pull-left" style="margin-left:auto;margin-right:auto;" src="<?php echo base_url();?>uploads/qrcodes/<?php echo $name_md5;?>.png"/>
      <h3 class="text-center" style="font-weight:bold;font-size:1.4em;"><?php echo $member->name;?></h3>
      <h4><?php //echo idToData($data['part_class_id'],'class');?></h4>
      <h4 style="margin-top:12%;line-height:1.2;"><?php
      $platform_name = $this->userM->idData($member->platform,$member->EID,'platform_name');
      echo $platform_name;
      ?></h4>
      </td>
    </tr>
  </table>
  <table>
    <tr>
      <td>
      <h3 class="event_heading" style="margin-top:3px;"><b><?php //echo idToData($eid,'event_name');?></b></h3>
      
      <small id="gen" class="text-center">Generated at <?php
                    echo date('d-m-y h:i:s a');?></small>

      </td>
    </tr>
<?php
//}
//mysqli_free_result($con);
?>
  </table>
  This ID is valid for two days from now.<div style="font-size:0.9em;">
  </div>

</div>
<?php
endforeach;
?>
</div><!-- /idcard-wrapper -->

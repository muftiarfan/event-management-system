<div class="col-lg-12">
<?php
$i = 1;
$this->load->view('a_event/toolbar');
?>
<div class="row">
<div class="col-lg-12">
<div class="panel panel-info">
<div class="panel-heading">
<h4>Users
<small>
<?php foreach($events as $event):?>
Registrations for <?php echo $event->name;?>
<?php endforeach;?>
</small>
</h4>
</div>
<div class="panel-body">
<ul class="list-group">
<?php foreach($users as $user_):
$user_table = $this->ion_auth->user($user_->UID)->row();
$profiles = $this->userM->user_profile($user_table->ref_id,false,$user_->UID);
$individual = '';
foreach($profiles as $profile):
	$count = $this->db->query('SELECT COUNT(PID) as `count` FROM events_members WHERE deleted=0 AND EID='.$eid.' AND UID='.$user_->UID.'');
	$count = $count->row_array();
if($profile->icard_path!='0'){
	$individual = '<a href="'.base_url().$profile->icard_path.'" target="_blank"><label class="label label-primary">Individual</label></a>';
}
?>
<li class="list-group-item"><strong><?php echo $i++;?>.</strong>&nbsp;<a title="View members" href="<?php echo base_url();?>event/icard/user/<?php echo $user_->UID;?>/<?php echo $eid;?>"><?php echo $profile->full_name;?> / <?php echo $profile->t_org;?></a>
<p class="pull-right">
<label class="label label-info label-md"><?php echo $count['count'];?></label>
<?php echo $individual;?>
</p>
</li>
<?php endforeach;?>
<?php endforeach;?>
<li class="list-group-item">
<strong> Total Attendees</strong>
<p class="pull-right">
<?php
	$total = $this->db->query("SELECT * FROM events_members WHERE deleted=0 AND EID='$eid'");
	$total = $total->num_rows();
	echo $total;
	?>
</p>
</li>
</ul>
</div>
</div>
</div>
</div> <!--end row-->
</div>
<div class="col-lg-12">
<?php
$this->load->view('a_event/toolbar');
?>
<?php echo form_open('event/sessions_update','class="form-horizontal"');?>
<table class="table table-stripped">
<thead>
<th>Name</th>
<th>Ticket</th>
<th>Amount</th>
<th>Attendees</th>
</thead>
<tbody>
<?php foreach($platforms as $platform):?>
<tr>
<td><?php echo $platform->name;?></td>
<td>
	<select class="form-control" name="ticket_id[<?php echo $platform->PID;?>]">
	<?php
	$session = $this->userM->idData($platform->PID,0,'session_ticket_name');
	$amount = $this->userM->idData($platform->PID,0,'session_amount');
	$attendee_limit = $this->userM->idData($platform->PID,0,'session_attendee_limit');
	//print_r($session);
	?>
	<?php
	if($session):
		?>
	<option value="<?php echo $session['ID'];?>"><?php echo $session['name'];?></option>
	<option value="0" disabled="disabled">Other</option>
	<?php endif;?>	
	<?php foreach($fee_tickets as $ticket): ?>
		<option value="<?php echo $ticket->ID;?>"><?php echo $ticket->name;?></option>
	<?php endforeach;?>
	</select>
</td>
<td>
	<input type="hidden" name="PID[]" value="<?php echo $platform->PID;?>">
	<input class="form-control input-sm" name="session_amount[<?php echo $platform->PID;?>]" placeholder="0.00" value="<?php echo $amount;?>">

</td>
<td>	<input class="form-control input-sm" name="session_attendee[<?php echo $platform->PID;?>]" placeholder="0" value="<?php echo $attendee_limit;?>">
</td>
</tr>
<?php endforeach;?>
</tbody>
</table>
<div class="form-group">
<div class="col-lg-12">
<button type="submit" class="btn btn-info btn-md">Update</button>
</div>
</div>
<?php echo form_close();?>
</div>
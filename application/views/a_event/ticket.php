
<div class="book">
    <div class="page">
        <div class="subpage"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-6">
                    <h3 style="margin:1px !important; font-weight: bolder;">
                    <?php 
				    echo $uid_basics['t_org'];
                    ?>
                    </h3>
                </div>
                <div class="col-sm-6">
                    <img src="<?php echo base_url();?>assets/images/techknow.gif" style="width: 130px;float: right;">
                </div>
            </div>
            <br>
              <div class="panel panel-default">
                <table class="table table-stripped table-bordered">
                    <thead>
                    <tr>
            <th style="width:50%;">Event Name</th>
            <th style="width:50%;">Date - Time</th>
            </tr>
            </thead> 
            <tbody>
            <tr>
            <td><?php echo $event['name'];?></td>
            <td><?php echo unix_to_human($event['date_epoch']);?></td>
            </tr>
            </tbody>
                </table>
            </div>
        </div>
            <div style="border: 1px solid #ccc; margin-bottom:0.25cm;"></div>

            <div class="panel panel-default">
         <!-- Default panel contents -->
                 <div class="panel-heading">Attendees</div>

  <!-- Table -->
<table class="table table-striped table-bordered">
            <thead>
            <tr>
            <th class="text-left">#</th>
            <th>Name</th>
            <th>Session</th>
            <!--th>Amount</th-->
            </tr>
            </thead>

            <tbody>
            <?php
            ///$i = 1;
            foreach($members as $member):
            $platform_price = $this->userM->idData($member->platform,0,'session_amount');
                ?>
            <tr>
            <td class="text-left"><?php echo $member->PID;?></td>
            <td><?php echo $member->name;?></td>
            <?php 
            $platform_name = $this->userM->idData($member->platform,$member->EID,'platform_name');
            ?>
            <td><?php echo $platform_name;?></td>
            <!--td>INR. <strong><?php echo $platform_price?></strong></td-->
            </tr>
            <?php
            endforeach;
        ?>
        </tbody>
                </table>
                        </div>

                    <div class="panel panel-default">
         <!-- Default panel contents -->
                 <div class="panel-heading">User</div>

  <!-- Table -->
<table class="table table-striped">
            <thead>
            
        
           
            <?php
                ?>
            <tr>

            <td>
            <?php 
            echo '<strong>#',$uid_basics['UID'],'&nbsp;',$uid_basics['full_name'],'</strong>&nbsp;';
            ?>
            
            </td>
            <td>
            <?php 
			echo $uid_basics['mobile'];
            ?>
			</td>
            </tr>
            </thead>
                </table>
                        </div>
            
            <div class="col-sm-12"> <h5 class="pull-right" style="font-weight: bold;">_________________</h5><br/><br/>
            <h5 class="pull-right" style="font-weight: bold;margin-top:10px;">Signature Principal</h5>
            </div>
            <div class="col-sm-12" style="margin-top: 10px;">
            <div class="panel panel-default">
                <div class="panel-heading">Instructions</div>

                <div class="panel-body" style="margin-top:0.3cm;">

                        
                        <p class="text-left">
                        <?php  echo $event['ticket_extra_info'];?>
                        </p>

                </div>

                </div>
                </div>
                               <hr>
                <div class="row" style="margin-top:4px;">
                    <div class="col-sm-12">
                    <p class="text-center">
                    <img src="<?php echo base_url();?>assets/images/dps_print.gif" style="width: 475px;"> </p></div>
                </div>
                <small style="float:right;" class="text-muted text-left">Generated at <?php
                    echo date('d-m-y h:i:s a');?></small>
            </div>
            </div>
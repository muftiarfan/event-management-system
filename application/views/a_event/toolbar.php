<?php
if($this->ion_auth->in_group(array(1,10,11,12,13))):
?>
<div class="col-lg-12" style="margin-bottom:4px;">
<div class="pull-right ">
                              <div class="btn-toolbar hidden-print">

<div class="btn-group">
        <a class="btn btn-primary btn-sm" href="#"><i class="fa fa-fw fa-calendar"></i> <span class="hidden-xs">Event</span></a>
        <a class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url();?>event/add"><i class="fa fa-fw fa-plus"></i> Add</a></li>
          <li><a href="<?php echo base_url();?>event/icards"><i class="fa fa-fw fa-qrcode"></i> I-Cards</a></li>
          <li><a href="<?php echo base_url();?>event/attendee_list"><i class="fa fa-fw fa-list"></i> Attendee List</a></li>
                  </ul>
    </div>
        <div class="btn-group">
        <a class="btn btn-primary btn-sm" href="#"><i class="fa fa-fw fa-eye"></i> <span class="hidden-xs">View</span></a>
        <a class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url();?>user/events"><i class="fa fa-fw fa-calendar"></i> Events</a></li>
          <li><a href="<?php echo base_url();?>event/view_organizers"><i class="fa fa-fw fa-users"></i> Organizers</a></li>
        </ul>
    </div>
    <?php
    if($this->uri->segment(1)=="event" && $this->uri->segment(2)=="icard" && $this->uri->segment(3)=="user"):
    ?>
    <a href="#" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#myModal">Add Attendees</a>
    <?php
    endif;
    ?>
    <?php
    if($this->uri->segment(2)=="view_organizers"):
    ?>
    <a href="<?php echo base_url();?>event/event_organizers" class="btn btn-sm btn-primary">Add Organizer</a>
    <?php
    endif;
    ?>
    

      </div>                            
      </div>
</div>
<?php
endif;
?>
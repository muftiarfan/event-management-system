<div class="container">


<div class="col-lg-12">
<?php
$this->load->view('a_event/toolbar');
?>
<table class="table table-hover">

<thead>
<tr>
<th>Name</th>
<th>Address</th>
<th>Contact</th>
<th>Email</th>
<th>Address</th>
<th>Added</th>
<th>Actions</th>
</tr>
</thead>
<tbody>
<?php // print_r($organizers);?>
<?php foreach($organizers as $organizer): ?>
<tr>
<td><?php echo $organizer->name;?></td>
<td><?php echo $organizer->address;?></td>
<td><?php echo $organizer->contact;?></td>
<td><?php echo $organizer->email;?></td>
<td><?php echo $organizer->address;?></td>
<td><?php echo unix_to_human($organizer->created_at);?></td>
<td>
<div class="btn-group">
<a href="#" class="btn btn-warning btn-xs">Edit</a>
<a href="#" class="btn btn-danger btn-xs">Trash</a>
</div>
</td>
</tr>
<?php endforeach;?>
</table>

</div>
</div>
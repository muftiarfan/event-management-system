<?php
$this->load->view('a_fee/toolbar');
?>
<div class="col-lg-12">

<?php echo form_open('fees/add_fee','class="form-horizontal"');?>

<div class="form-group">

<div class="col-lg-12">
<label for="events">Event</label>
<select class="form-control" onchange="fetch_select(this.value, 'event_return_users', 'event_return_users');" name="events" id="events">
<option value="0" selected="selected">Select</option>
<?php foreach($events as $event):?>
	<option value="<?php echo $event->EID;?>"><?php echo $event->name;?></option>
<?php endforeach;?>
</select>
</div>

</div>
<div id="loader" style="display:none;">
<div class="col-lg-12">
<div style="margin-left: auto;
    margin-right: auto;
    width: 6em;">
<span class="fa fa-spinner fa-spin fa-4x"></span>
</div>
</div>
</div>
<div class="form-group" id="event_return_users" style="display:none;">

</div>

<div class="form-group" id="event_return_members" style="display:none;">

</div>


<?php echo form_close();?>

</div>
<script type="text/javascript">
function fetch_select(val, destination, parent)
{
	$("#loader").show();
   $.ajax({
     type: 'post',
     url: baseUrl+'fees/fees_ajax',
     async: true,
     cache:false,
     data: {
       type:destination,
       <?php echo $this->security->get_csrf_token_name();?> : '<?php echo $this->security->get_csrf_hash();?>',
       ID:val,
       
     },
     success: function (response) {
       $("#"+parent).show();
       if(parent=='event_return_users'){
       	$("#event_return_users").empty();
       	$("#event_return_members").empty();
       }
       $("#"+destination).html(response);
       $("#loader").hide();
     }
   });
}

function fetch_amount(ticket_id,amount){
	//TODO: 
	$.ajax({
		type: 'POST',
		url: baseUrl+'fees/fee_amount_ajax',
		data:{
			ticket_type:ticket_id,
			amnt: amount,
		},
		success: function (response){
			//$("#final_amount").empty();
			$("#resp-amnt").show();
			$("#final_amount").html(response);
		}
	});
}

function custom_add(charges,pre_amount){

	var final_amount = +charges + +pre_amount;
	$("#total_fees_amount").val(final_amount);
}

</script>
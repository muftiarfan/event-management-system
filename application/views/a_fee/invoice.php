<style>
.invoice-title h2, .invoice-title h3 {
    display: inline-block;
}

.table > tbody > tr > .no-line {
    border-top: none;
}

.table > thead > tr > .no-line {
    border-bottom: none;
}

.table > tbody > tr > .thick-line {
    border-top: 2px solid;
}
</style>
<?php
foreach($trans as $detail):?>
<?php
$ticket_type = $this->feeM->ticket_info($detail->ticket_id);
$reciever = $this->ion_auth->user($detail->UID)->row();
$attendee = $this->ion_auth->user($detail->event_idUser)->row();
$attendee = $this->userM->user_profile($attendee->ref_id,true,$attendee->id);
$all_members = $this->userM->get_members($detail->event_id,$detail->event_idUser,1); //checked in attendees
$all_members_count = $this->userM->already_members($detail->event_id,$detail->event_idUser,1);
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
    		<div class="invoice-title">
    			<h2>Invoice</h2><h3 class="pull-right">TID:<?php echo $detail->ID;?></h3>
    		</div>
    		<hr>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    				<strong>Billed To:</strong><br>
    					<?php echo $attendee['full_name'];?><br>
    					<?php echo $attendee['t_org'];?><br>
    					<?php echo '+91 ',$attendee['mobile'];?><br>
    					
    				</address>
    			</div>
    		</div>
    		<div class="row">
    			<div class="col-xs-6">
    				<address>
    					<strong>Payment Method:</strong><br>
    					Cash<br/>
    				</address>
    			</div>
    			<div class="col-xs-6 text-right">
    				<address>
    					<strong>Date/Time:</strong><br>
    					<?php echo unix_to_human($detail->created_at);?><br><br>
    				</address>
    			</div>
    		</div>
    	</div>
    </div>
    
    <div class="row">
    	<div class="col-md-12">
    		<div class="panel panel-default">
    			<div class="panel-heading">
    				<h3 class="panel-title"><strong>Summary</strong></h3>
    			</div>
    			<div class="panel-body">
    				<div class="table-responsive">
    					<table class="table table-condensed">
    						<thead>
                                <tr>
        							<td><strong>Item</strong></td>
        							<td class="text-center"><strong>Price</strong></td>
        							<td class="text-center"><strong>Quantity</strong></td>
        							<td class="text-right"><strong>Totals</strong></td>
                                </tr>
    						</thead>
    						<tbody>
    						<tr>
    								<td>Sessions<br/></td>    							
    								<td class="text-center"></td>
    								<td class="text-center"></td>
    								<td class="text-right"></td>
    							</tr>
    							<tr>
    							<td class="text-left"><strong>Session</strong></td>
    							<td class="text-right"><strong>Amount</strong></td>
    							<td></td>
    							<td></td>
    							</tr>
    							<?php 
    							$session_totals=0;
    							foreach($all_members as $member):
    								$session_amnt = $this->userM->idData($member->platform,0,'session_amount');
    								$session = $this->userM->idData($member->platform,$member->EID,'platform_name');
    								$session_totals +=$session_amnt;
    								?>
    							<tr>
    							<td><?php echo $session;?> / <small class="text-muted"><?php echo $member->name;?></small></td>
    							<td class="text-right"><?php echo $session_amnt;?></td>
    							<td></td>
    							<td></td>
    							</tr>
    							<?php endforeach;?>
    							<tr>
    							<td colspan="2"></td>
    							<td class="text-center"><?php echo $all_members_count;?></td>
    							<td class="text-right">INR. <?php echo $session_totals;?></td>
    							</tr>
    							    						<?php
    						$custom_charges = 0;
    						if($detail->custom_charges>0):
    							$custom_charges = $detail->custom_charges;
    							?>
    							<tr>
    								<td>Addtional Charges<br>
    								<small class="text-muted">Desc: <?php echo $detail->custom_charges_desc;?></small></td>
    								<td class="text-center"><?php echo $detail->custom_charges;?></td>
    								<td class="text-center">1</td>
    								<td class="text-right">INR. <?php echo $custom_charges;?></td>
    							</tr>
    						<?php else:?>
    							<tr>
    								<td>Ticket Type<br>
    								<small class="text-muted">Desc: <?php echo $ticket_type['name'];?></small></td>
    								<td class="text-center"><?php echo $detail->custom_charges;?></td>
    								<td class="text-center">1</td>
    								<td class="text-right">INR. <?php echo $custom_charges;?></td>
    							</tr>
    						<?php endif;?>
    							<tr>
    								<td class="thick-line"></td>
    								<td class="thick-line"></td>
    								<td class="thick-line text-center"><strong>Subtotal</strong></td>
    								<td class="thick-line text-right">INR. <?php echo $detail->amount;?></td>
    							</tr>
    							<tr>
    								<td class="no-line"></td>
    								<td class="no-line"></td>
    								<td class="no-line text-center"><strong>Total</strong></td>
    								<td class="no-line text-right">INR. <?php echo $detail->amount;?></td>
    							</tr>
    						</tbody>
    					</table>
    				</div>
    				<div class="row" style="margin-top:4px;">
                    <div class="col-sm-12"><p class="text-center"><img src="<?php echo base_url();?>assets/images/dps_print.gif" style="padding:25px;width: 473px;margin-left:auto;margin-right:auto;"> </p>
                    <p class="text-center text-muted">UID:<strong><?php echo $this->ion_auth->user()->row()->id;?></strong>
                    <span class="text-muted">This is a computer generated receipt, and doesn't require signature.<br/>
                    Generated at <?php
                    echo date('d-m-y h:i:s a');?></span></p>
                    </div>
                </div>
    			</div>

    		</div>
    	</div>
    </div>
</div>
<?php endforeach;?>
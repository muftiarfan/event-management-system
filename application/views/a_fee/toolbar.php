<div class="col-lg-12">
<div class="pull-right ">
                              <div class="btn-toolbar hidden-print">

<div class="btn-group">
        <a class="btn btn-primary btn-sm" href="#"><i class="fa fa-fw fa-money"></i> <span class="hidden-xs">Fee</span></a>
        <a class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url();?>fees/add"><i class="fa fa-fw fa-rupee"></i> Recieve</a></li>
          <li><a href="<?php echo base_url();?>fee/types"><i class="fa fa-fw fa-sitemap"></i> Types</a></li>
                  </ul>
    </div>
        <div class="btn-group">
        <a class="btn btn-primary btn-sm" href="#"><i class="fa fa-fw fa-print"></i> <span class="hidden-xs">View</span></a>
        <a class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" href="#"><span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="<?php echo base_url();?>fees/view"><i class="fa fa-fw fa-rupee"></i> Fee</a></li>
          <li><a href="<?php echo base_url();?>fees/deleted"><i class="fa fa-fw fa-trash"></i> Deleted Fee</a></li>
          <li><a href="<?php echo base_url();?>fees/statement"><i class="fa fa-fw fa-calculator"></i> Statement</a></li>
        </ul>
    </div>
    
    

      </div>                            
      </div>
</div>
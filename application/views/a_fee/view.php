<?php
$this->load->view('a_fee/toolbar');
?>
<div class="col-lg-12">
<div class="col-md-2">
<select class="form-control" id="evnt">
<option value="all">All Events</option>
<?php foreach($events as $event):?>
<option value="<?php echo $event->EID;?>"><?php echo $event->name;?></option>
<?php endforeach;?>
</select>
</div>
<div class="col-md-2">
<select class="form-control" id="ticket_type">
<option value="all">All Ticket Types</option>
<?php foreach($tickets as $ticket):?>
<option value="<?php echo $ticket->ID;?>"><?php echo $ticket->name;?></option>
<?php endforeach;?>
</select>
</div>
<div class="col-md-2">
<select class="form-control" id="usrs">
<option value="all">All Users</option>
<?php
foreach($users as $user):?>
<option value="<?php echo $user->id;?>"><?php echo $user->username;?></option>
<?php endforeach;?>
</select>
</div>
<a id="search" href="view" class="btn btn-success" role="button"><i class="fa fa-search"></i></a>
<table class="table table-hover">

<thead>
<th>TransID</th>
<th>Date</th>
<th>Event</th>
<th>Amount</th>
<th>Ticket Type</th>
<th>Attendee</th>
<th>Recieved By</th>
<th>Actions</th>
</thead>
<tbody>
<?php 
foreach($payments_fee as $fee):
$ticket_type = $this->feeM->ticket_info($fee->ticket_id);
$reciever = $this->ion_auth->user($fee->UID)->row();
$attendee = $this->ion_auth->user($fee->event_idUser)->row();
$attendee = $this->userM->user_profile($attendee->ref_id,true,$attendee->id);
?>
<tr>
<td><?php echo $fee->ID;?></td>
<td><?php echo unix_to_human($fee->created_at);?></td>
<td><?php echo $this->userM->idData($fee->event_id,0,'event_name');?></td>
<td><strong>INR. <?php echo $fee->amount;?></strong></td>
<td style="cursor:pointer;" title="<?php echo $ticket_type['description'];?>"><?php echo $ticket_type['name'];?></td>
<td><?php echo $attendee['full_name'];?></td>
<td><?php echo $reciever->username;?></td>
<td>
	<div class="btn-group">
	<a href="<?php echo base_url();?>fees/reciept_print/<?php echo $fee->ID;?>" target="_blank" class="btn btn-default btn-xs"><span class="fa fa-print"></span></a>
	<a href="<?php echo base_url();?>fees/fee_detail/<?php echo $fee->ID;?>" title="Transaction Details" role="button" class="btn btn-default btn-xs" data-target="#modalfee" data-toggle="modal"><i class="fa fa-eye"> </i></a>
	<a href="#" class="btn btn-default btn-xs"><span class="fa fa-trash"></span></a>
	</div>
</td>

</tr>
<?php endforeach; ?>
</tbody>
</table>

</div>
<div class="modal fade" id="modalfee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

    </div>
  </div>
</div>
 <script type="text/javascript">$(document).ready(function(){
 	  $('body').on('hidden.bs.modal', '.modal', function () {
         $(this).removeData('bs.modal');
  });
                    function changeLink()
                    {
                    	
                    	var evnt = $("#evnt").val();
                    	var ticket_type = $("#ticket_type").val();
                    	var usrs = $("#usrs").val();
                        if(!evnt) evnt = "all";
                        if(!ticket_type) ticket_type = "all";
                        if(!usrs) usrs = "all";
                        $("#search").attr("href", baseUrl+"fees/view/"+evnt+"/"+ticket_type+"/"+usrs);
                        
                    }
					$("#search").on("mousedown", function(){
						changeLink();
                    });
					
				});
 </script>
<div class="col-lg-12">
<?php
$this->load->view('a_music/toolbar');
?>
<?php
echo $this->notify->show();
echo validation_errors(); 
?>
<?php echo form_open('music/add','class="form-horizontal"');?>
<div class="form-group">
<div class="col-lg-12">
<label for="title">Title</label>
<input type="text" name="title" id="title" class="form-control">
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<label for="artist">Artist</label>
<input type="text" name="artist" id="artist" class="form-control">
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<label for="album">Album</label>
<input type="text" name="album" id="album" class="form-control">
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<label for="year">Year</label>
<select name="year" id="year" class="form-control">
<?php
$i = 1899;
while($i<2015):
$i++;
?>
<option value="<?php echo $i;?>"><?php echo $i;?></option>
<?php
endwhile;
?>
</select>
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<button type="submit" class="btn btn-success btn-lg">Add</button>
</div>
</div>
<?php echo form_close();?>
</div>
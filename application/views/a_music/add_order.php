<div class="col-lg-12">
<?php
$this->load->view('a_music/toolbar');
?>
<hr>
<?php echo validation_errors();
echo form_open('music/order','class="form-horizontal"');
?>
<div class="form-group">
<div class="col-lg-9">
<label for="song">Title</label>
<select class="song form-control" name="song" id="song">
</select>
</div>
<div class="col-lg-2">
<label for="price">Price <span class="fa fa-inr"></span></label>
<input type="text" disabled="disabled" value="<?php echo $price;?>.00" class="form-control">
</div>
</div>
<div class="form-group">
<div class="col-lg-6">
<label for="from">From</label>
<input type="text" class="form-control" name="from" id="from">
</div>
<div class="col-lg-6">
<label for="to">To</label>
<input type="text" class="form-control" name="to" id="to">
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<button type="submit" class="btn btn-success btn-lg">Queue Order</button>
</div>
</div>
</div>
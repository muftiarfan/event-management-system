<div class="col-lg-12">
<?php
$this->load->view('a_music/toolbar');
?>
<hr>
<div class="row">
<div class="col-lg-2">
<div class="well well-lg text-center">
<h2 class="text text-success">Total</h2>
<h4><?php echo $c_total;?></h4>
<small class="text-muted">Music in dB</small>
</div>
</div>
<div class="col-lg-2">
<div class="well well-lg text-center">
<h2 class="text text-success">Total</h2>
<h4><?php echo $c_ototal;?></h4>
<small class="text-muted">Orders</small>
</div>
</div>
<div class="col-lg-2">
<div class="well well-lg text-center">
<h2 class="text text-success">Total</h2>
<h4><?php echo $c_ototalc;?></h4>
<small class="text-muted">Orders Completed</small>
</div>
</div>
<div class="col-lg-6">
<div class="well well-lg text-center">
<h2 class="text text-success">Last Activity</h2>
<h4><?php echo timespan($last_order,time());?> ago</h4>
<small class="text-muted">Last Order processed</small>
</div>
</div>
</div>
</div>
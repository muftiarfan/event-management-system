<div class="col-lg-12">
<?php
$this->load->view('a_music/toolbar');
?>
<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>#</th>
      <th>Title</th>
      <th>Artist</th>
      <th>Album</th>
      <th>Year</th>
      <th>Added on</th>
      <th>Orders</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($songs as $song):?>
  <tr>
  <td><?php echo $song->ID;?></td>
  <td><?php echo $song->title;?></td>
  <td><a href="<?php echo base_url();?>music/songs/artist/<?php echo $song->artist;?>"><?php echo $song->artist;?></a></td>
  <td><a href="<?php echo base_url();?>music/songs/album/<?php echo $song->album;?>"><?php echo $song->album;?></a></td>
  <td><a href="<?php echo base_url();?>music/songs/year/<?php echo $song->year;?>"><?php echo $song->year;?></a></td>
  <td class="text text-muted"><?php echo unix_to_human($song->created_at);?></td>
  <td class="text text-muted"><?php echo $song->orders;?></td>
  <td><div class="btn-group">
  <?php if($this->ion_auth->is_admin()):
  if($song->status==1){?>
  <a href="" class="btn btn-warning btn-xs">Deactivate</a>
  <?php }elseif($status==0){?>
  <a href="" class="btn btn-success btn-xs">Activate</a>
  <?php } ?>
  <a href="<?php base_url();?>music/song_delete/<?php echo $song->ID;?>" title="Trash Song" class="btn btn-danger btn-xs"><span class="fa fa-trash"></span></a>
<?php endif;?>
  </div>
  </td>
  </tr>
<?php endforeach;?>
  </tbody>
  </table>
</div>
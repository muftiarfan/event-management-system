<div class="col-lg-12">
<?php
$this->load->view('a_music/toolbar');
?>
<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>#</th>
      <th>SID</th>
      <th>Title</th>
      <th>Artist</th>
      <th>Album</th>
      <th>Year</th>
      <th>Ordered on</th>
      <th>Price</th>
      <th>Played</th>
      <th>Dedication</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($songs as $song_o):?>
    <?php $songs = $this->musicM->listing('WHERE ID='.$song_o->SID.'');
    foreach($songs as $song): ?>
  <tr>
  <td><?php echo $song_o->ID;?></td>
  <td><a href="<?php echo base_url();?>music/orders/song/<?php echo $song_o->SID;?>"><?php echo $song_o->SID;?></a></td>
  <td><?php echo $song->title;?></td>
  <td><a href="<?php echo base_url();?>music/songs/artist/<?php echo $song->artist;?>"><?php echo $song->artist;?></a></td>
  <td><a href="<?php echo base_url();?>music/songs/album/<?php echo $song->album;?>"><?php echo $song->album;?></a></td>
  <td><a href="<?php echo base_url();?>music/songs/year/<?php echo $song->year;?>"><?php echo $song->year;?></a></td>
  <td class="text text-muted"><?php echo unix_to_human($song_o->ordered_at);?></td>
  <td class="text text-muted"><?php echo $song_o->price;?></td>
  <td class="text text-muted"><?php if($song_o->played_at==0){?><label class="label label-default">No</label><?php } else { ?><label class="label label-success">Yes</label> :: <?php echo unix_to_human($song_o->played_at);?><?php } ?></td>
  <td><?php if($song_o->from!=''){?><label class="label label-success"><?php echo $song_o->from;?></label>&nbsp;<span class="fa fa-arrow-right"></span>&nbsp;<label class="label label-warning"><?php echo $song_o->to;?></label><?php }else{ ?><label class="label label-info">None</label><?php } ?></td>
  <td><div class="btn-group">
  <?php
  if($song_o->played_at==0){?>
  <a href="<?php echo base_url();?>music/played/<?php echo $song_o->SID;?>" class="btn btn-success btn-xs" title="Mark as played"><span class="fa fa-check"></span></a>
  <a href="" class="btn btn-warning btn-xs" title="Cancel Order"><span class="fa fa-times"></span></a>
  <?php }?>
  </div>
  </td>
  </tr>
<?php endforeach;?>
<?php endforeach;?>
  </tbody>
  </table>
</div>
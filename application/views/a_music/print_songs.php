<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>#</th>
      <th>Title</th>
      <th>Artist</th>
      <th>Album</th>
      <th>Year</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($songs as $song):?>
  <tr>
  <td><?php echo $song->ID;?></td>
  <td><?php echo $song->title;?></td>
  <td><?php echo $song->artist;?></td>
  <td><?php echo $song->album;?></td>
  <td><?php echo $song->year;?></td>
  </tr>
<?php endforeach;?>
  </tbody>
  </table>
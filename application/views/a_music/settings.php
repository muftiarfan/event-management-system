<div class="col-lg-12">
<?php
$this->load->view('a_music/toolbar');
?>
<?php echo validation_errors();
echo form_open('music/settings','class="form-horizontal"');?>
<div class="form-group">
<div class="col-lg-12">
<label for="music_order_price">Price Variable</label>
<input type="text" name="music_order_price" id="music_order_price" class="form-control" value="<?php echo $price;?>">
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<button type="submit" class="btn btn-success btn-lg">Update</button>
</div>
</div>
<?php echo form_close();?>
</div>
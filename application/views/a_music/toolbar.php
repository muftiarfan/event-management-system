<div class="row">
        <div class="btn-group">
        <?php if($this->ion_auth->is_admin()):?>
        <a href="<?php echo base_url();?>music/dashboard" class="btn btn-sm btn-default <?php if($this->uri->segment(1)=="music" && $this->uri->segment(2)=="dashboard"){echo "active";}?>"><span class="fa fa-home"></span></a>
    <?php endif;?>
    	        <?php if($this->ion_auth->is_admin() || $this->ion_auth->in_group(3)):?>
        <a href="<?php echo base_url();?>music/order" class="btn btn-sm btn-primary <?php if($this->uri->segment(1)=="music" && $this->uri->segment(2)=="order"){echo "active";}?>"><span class="fa fa-plus"></span> Order</a>        
        <a href="<?php echo base_url();?>music/orders" class="btn btn-sm btn-primary <?php if($this->uri->segment(1)=="music" && $this->uri->segment(2)=="orders"){echo "active";}?>"><span class="fa fa-list"></span> Orders</a>
        <?php if($this->uri->segment(1)=="music" && $this->uri->segment(2)=="orders"):?>
        	<a href="<?php echo base_url();?>music/orders/played" class="btn btn-primary btn-sm <?php if($this->uri->segment(1)=="music" &&$this->uri->segment(3)=='played'){echo "active";}?>" title="Played Orders"><span class="fa fa-play"></span></a>
        	<a href="<?php echo base_url();?>music/orders/unplayed" class="btn btn-warning btn-sm <?php if($this->uri->segment(1)=="music" &&$this->uri->segment(3)=='unplayed'){echo "active";}?>" title="Unplayed Orders"><span class="fa fa-pause"></span></a>
        <?php endif;?>
        <?php endif;?>
        <?php 
        $groups = array(1,4,7);
        if($this->ion_auth->in_group($groups)):?>
        <a href="<?php echo base_url();?>music/songs" class="btn btn-sm btn-success <?php if($this->uri->segment(1)=="music" && $this->uri->segment(2)=="songs"){echo "active";}?>"><span class="fa fa-music"></span> Songs</a>
        <?php $groups=array(1,4);
        if($this->ion_auth->in_group($groups)):?>
        <a href="<?php echo base_url();?>music/add" class="btn btn-sm btn-primary <?php if($this->uri->segment(1)=="music" && $this->uri->segment(2)=="add"){echo "active";}?>"><span class="fa fa-plus"></span> Add Song</a>
        <?php endif;?>        
        <?php endif;?>        
        </div>
</div>
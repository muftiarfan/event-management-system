<div class="col-lg-12">
<h1 class="text text-center">Music On Demand</h1>
<div class="col-lg-6">
<h3>Choose From</h3>
<marquee direction="up" height="420">
<ul class="list-group">
<?php foreach($songs as $song):?>
<li class="list-group-item">#<?php echo $song->ID;?> - <strong><?php echo $song->title;?></strong> - <?php echo $song->artist;?>/<?php echo $song->album;?></li>
<?php endforeach;?>
</ul>
</marquee>
</div>
<div class="col-lg-6">
<h3>Now Playing</h3>
<?php foreach($order_now as $order):?>
<?php $query = $this->db->query('SELECT * FROM music_songs WHERE ID='.$order->SID.'');
$query = $query->result();
foreach($query as $playing):
	?>
<h4><?php echo $playing->title;?></h4><br/>
<img class="text-center img-responsive" width="320" src="<?php echo base_url();?>assets/images/now_playing.png"/><br/>
<strong class="text text-muted">By:<?php echo $playing->artist;?></strong><br/>
<strong class="text text-muted">Album:<?php echo $playing->album;?></strong><br/>
Dedication: <?php if($order->from!=''){?>From: <label class="label label-success"><?php echo $order->from;?></label>&nbsp; To &nbsp;<label class="label label-warning"><?php echo $order->to;?></label><?php }else{ ?><label class="label label-info">None</label><?php } ?></td>

	<?php endforeach;?>
	<?php endforeach;?>
</div>
</div>
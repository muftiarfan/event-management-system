<div class="col-lg-12">
<?php
$this->load->view('a_sms/toolbar');
?>
<?php echo form_open('sms/composeA','class="form-horizontal"'); ?>

<div class="form-group">
    <label for="rec" class="col-sm-2 control-label">To</label>
    <div class="col-sm-10">
    <!--select class="form-control ajaxlist" id="recipients" name="recipients" multiple="multiple" style="width: 75%"></select-->
    <input type="hidden" class="ajaxlist" id="recipients" style="width: 75%;padding:5px;" name="recipients" multiple="multiple"/>
    </div>
  </div>
  <div class="form-group">
    <label for="msg" class="col-sm-2 control-label">Message</label>
    <div class="col-sm-10">
      <textarea name="message" rows="5" data-charlimit="160" id="msg" class="form-control input-lg"></textarea>    
      </div>
  </div>  
  <div class="form-group">
    <label for="msg" class="col-sm-2 control-label">Via</label>
    <div class="col-sm-10">
    <select id="msg" name="gateway" class="form-control">
    <?php foreach($gateways as $gateway):?>
    <option value="<?php echo $gateway['gateway_id'];?>"><?php echo $gateway['name'];?></option>
  <?php endforeach;?>
    </select>
      </div>
  </div>
<div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-success">Send</button>
    </div>
  </div>
<?php echo form_close();?>
</div>
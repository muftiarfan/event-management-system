<div class="col-lg-12">
<?php
$this->load->view('a_sms/toolbar');
?>
<div class="row">
<div class="col-lg-2">
<div class="well well-lg text-center">
<h2 class="text text-success">Total</h2>
<h4><?php echo $c_total;?></h4>
<small class="text-muted">SMSes Sent</small>
</div>
</div>
<div class="col-lg-2">
<div class="well well-lg text-center">
<h2 class="text text-success">Total</h2>
<h4><?php echo $c_qtotal;?></h4>
<small class="text-muted">SMSes in queue</small>
</div>
</div>
<div class="col-lg-2">
<div class="well well-lg text-center">
<h2 class="text text-success">Total</h2>
<h4><?php echo $c_todaytotal;?></h4>
<small class="text-muted">SMSes sent today</small>
</div>
</div>
<div class="col-lg-6">
<div class="well well-lg text-center">
<h2 class="text text-success">Last Activity</h2>
<h4><?php echo timespan($last_activity,time());?> ago</h4>
<small class="text-muted">Last SMS processed</small>
</div>
</div>
</div>
</div>
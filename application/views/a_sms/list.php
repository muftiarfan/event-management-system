<div class="col-lg-12">
<?php
$this->load->view('a_sms/toolbar');
?>
</div>

<div class="row">
<div class="col-lg-12">

<table class="table table-striped table-hover">
<thead>
<tr>
<th>Recipient</th>
<th>Message</th>
<th>Secheduled</th>
<th>Sent</th>
<th>Actions</th>
</tr>
</thead>

<tbody>
<?php
if(!$smses):
	?>
<tr>
<td colspan="5">
<div class="text-center"><strong class="text-muted">No sms found in the database.</strong></div>
</td>
</tr>
<?php endif;?>
<?php 
foreach($smses as $sms):
	?>
	<tr>
	<td><?php echo $sms->recipient;?></td>
	<td><?php echo $sms->message;?></td>
	<td><?php echo unix_to_human($sms->scheduled);?></td>
	<td><?php echo unix_to_human($sms->updated);?></td>
	<td><div class="btn-group">
		<a href="#" class="btn btn-default btn-xs"><span class="fa fa-eye fa-w"></span></a>
		<a href="#" class="btn btn-danger btn-xs"><span class="fa fa-trash fa-w"></span></a>
	</div></td>
	</tr>
	<?php
	endforeach;
	?>
</tbody>
</table>

</div>

</div>
<div class="row">
        <div class="btn-group">
        <a href="<?php echo base_url();?>sms/dashboard" class="btn btn-sm btn-default <?php if($this->uri->segment(1)=="sms" && $this->uri->segment(2)=="dashboard"){echo "active";}?>"><span class="fa fa-home"></span></a>
        <a href="<?php echo base_url();?>sms/compose" class="btn btn-sm btn-primary <?php if($this->uri->segment(1)=="sms" && $this->uri->segment(2)=="compose"){echo "active";}?>"><span class="fa fa-pencil"></span> Compose</a>
        <a href="<?php echo base_url();?>sms/sent" class="btn btn-sm btn-success <?php if($this->uri->segment(1)=="sms" && $this->uri->segment(2)=="sent"){echo "active";}?>"><span class="fa fa-check"></span> Sent</a>
        <a href="<?php echo base_url();?>sms/queue" class="btn btn-sm btn-warning <?php if($this->uri->segment(1)=="sms" && $this->uri->segment(2)=="queue"){echo "active";}?>"><span class="fa fa-pause"></span> Queue</a>
        </div>
</div>
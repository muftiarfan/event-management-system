<div class="col-lg-12">
<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>#</th>
      <th>Group</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($groups as $group):?>
  	<tr>
  	<td><?php echo $group->id;?></td>
  	<td><p>
  	<span class="label label-info"><?php echo $group->name;?></span>
  	</p>
  	<p>
  	<small class="text text-muted"><?php echo $group->description;?></small>
  	</p>
  	</td>
  	<td>
  	<div class="btn-group">
  	<a href="" class="btn btn-primary btn-xs" title="Edit Group name/description"><span class="fa fa-edit"></span></a>
  	<a href="" class="btn btn-danger btn-xs" title="Delete this group"><span class="fa fa-trash"></span></a>
  	</div>
  	</td>
  	</tr>
  <?php endforeach;?>
  </tbody>
  </table>
</div>
<div class="col-lg-12">
<div class="col-md-2">
<select class="form-control" id="users_s">
<option value="all">All Users</option>
<option value="active">Active</option>
<option value="inactive">Inactive</option>
</select>
</div>
<div class="col-md-2">
<select class="form-control" id="users_type">
<option value="all">All</option>
<option value="individual">Individual</option>
<option value="team">Team</option>
</select>
</div>
<div class="col-md-2">
<select class="form-control" id="profile">
<option value="all">Complete Profiles</option>
<option value="incomplete">Incomplete Profiles</option>
</select>
</div>
<a id="search" href="view" class="btn btn-success" role="button"><i class="fa fa-search"></i></a>
<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>#</th>
      <th>Username</th>
      <th>Name</th>
      <th>Email</th>
      <th>Created On</th>
      <th>Last Login</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($users as $user_):
  $profile = $this->userM->user_profile($user_->ref_id,true,$user_->id);
  ?>
  <tr>
  <td><?php echo $user_->id;?></td>
  <td><?php echo $user_->username;?></td>
  <td><?php echo $profile['full_name'];?></td>
  <td><?php echo $user_->email;?></td>
  <td class="text text-muted"><?php echo unix_to_human($user_->created_on);?></td>
  <td class="text text-muted"><?php echo unix_to_human($user_->last_login);?></td>
  <td><div class="btn-group">
  <a class="btn btn-xs btn-primary" href="<?php echo base_url();?>user/profile/<?php echo $user_->ref_id;?>/<?php echo $user_->id;?>" title="View Profile" target="_blank"><span class="fa fa-user"></span></a>
  <a class="btn btn-xs btn-info" title="Edit User" href="<?php echo base_url();?>auth/edit_user/<?php echo $user_->id;?>/<?php echo $user_->ref_id;?>"><span class="fa fa-edit"></span></a>
  <?php if($user_->active==1):
  $style = 'btn-success';
  elseif($user_->active==0):
    $style  = 'btn-default';
  endif;
  ?>
  <a data-toggle="confirm-modal" data-confirm-title="Change status?"
                    data-confirm-message="Do you really want to change the user status?"
                    data-confirm-ok="Yes!" data-confirm-cancel="Changed my mind" 
                    class="btn btn-xs <?php echo $style;?>" title="Make inactive" href="<?php echo base_url();?>users/changeStatus/<?php echo $user_->id;?>/<?php echo $user_->ref_id;?>"><span class="fa fa-circle"></span></a>
  </div>
  </td>
  </tr>
<?php endforeach;?>
  </tbody>
  </table>
</div>
 <script type="text/javascript">$(document).ready(function(){
            function changeLink()
                    {
                      
                      var users_s = $("#users_s").val();
                      var users_type = $("#users_type").val();
                      var profile = $("#profile").val();
                        if(!users_s) users_s = "all";
                        if(!users_type) users_type = "all";
                        if(!profile) profile = "all";
                        $("#search").attr("href", baseUrl+"users/view/"+users_s+"/"+users_type+"/"+profile);
                        
                    }
          $("#search").on("mousedown", function(){
            changeLink();
                    });
          
        });
 </script>
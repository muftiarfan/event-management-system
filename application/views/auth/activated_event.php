<div class="col-lg-12">

<div class="panel panel-info">
<div class="panel-heading"><h1>Email Verified</h1></div>
<div class="panel-body">

<p>You are another step closer to officially be a part of this event and attending sessions of your choice.</p>
<p><strong>Follow the following steps to add attendees from your organization/school and finally get your ticket.</strong></p>
<p>
<ol>
<li>Login to your account using the credentials sent earlier on your email <strong><?php echo $user->email;?></strong></li>
<li>Visit <a target="_blank" href="<?php echo base_url();?>auth/login"><?php echo base_url();?>auth/login</a> to login to your account</li>
<li>Visit <a target="_blank" href="<?php echo base_url();?>user/event_register/<?php echo $EID;?>"><?php echo base_url();?>user/event_register/<?php echo $EID;?></a> to add attendees from your school/organization</li>
<li>After you have successfully added attendees to the event "<strong><?php echo $event['name'];?></strong>", you can print your ticket from your user dashboard at <a target="_blank" href="<?php echo base_url();?>user/dashboard"><?php echo base_url();?>user/dashboard</a>
<strong>It is compulsory to print the ticket and get it along with you on the day of the event.</strong></li>
<li>You will find all the neccessary instructions and notes for the event inside the ticket.</li>
</ol>
</p>
<p><strong>Thank You!</strong></p>
<p><small class="text-muted">P.S Don't worry, we have sent these instructions on your email too! :)</small></p>
</div>
</div>

</div>
<div class="col-lg-12">
<p>Updating Profile</p>
<?php echo $message;?>
<?php echo form_open(uri_string(),'class="form-horizontal"');?>
<div class="form-group">
<div class="col-lg-6">
            <label for="full_name">Full Name</label>
            <?php echo form_input($full_name);?>
</div>
<div class="col-lg-6">
            <label for="occupation">Occupation</label>
            <?php echo form_input($occupation);?>
</div>
</div>

<div class="form-group">
<div class="col-lg-8">
            <label for="t_org">Team/Organization</label>
            <?php echo form_input($t_org);?>
</div>
<div class="col-lg-4">
            <label for="upto_class">School Level</label>
            <?php echo form_dropdown('upto_class',$upto_class,$upto_class_selected,'class="form-control"');?>
</div>
</div>
<div class="form-group">
<div class="col-lg-6">
            <label for="mobile">Mobile</label>
            <?php echo form_input($mobile);?>
</div>
<div class="col-lg-6">
            <label for="phone">Phone</label>
            <?php echo form_input($phone);?>
</div>
</div>
<div class="form-group">
<div class="col-lg-8">
<label for="address">Address</label>
<?php echo form_textarea($address);?>
</div>
<div class="col-lg-4">
<label for="address">District</label>
<?php echo form_dropdown('district',$district,$district_selected,'class="form-control"');?>
</div>
</div>
<div class="form-group">
<div class="col-lg-8">
            <label for="password">Password <span class="fa fa-info-circle fa-1x text-info" data-toggle="tooltip" data-placement="right" title="" data-original-title="If Changing Password"></span></label>
            <?php echo form_input($password);?>
</div>
</div>
<div class="form-group">
<div class="col-lg-8">
            <label for="password_confirm">Confirm Password</label>
            <?php echo form_input($password_confirm);?>
</div>
</div>
<div class="col-lg-12">
      <?php if ($this->ion_auth->is_admin()): ?>

          <h3><?php echo lang('edit_user_groups_heading');?></h3>
          <div class="checkbox">
          <ul class="list-group">
          <?php foreach ($groups as $group):?>
              <li class="list-group-item list-group-item-danger" style="padding:24px;">
              <?php
                  $gID=$group['id'];
                  $checked = null;
                  $item = null;
                  foreach($currentGroups as $grp) {
                      if ($gID == $grp->id) {
                          $checked= ' checked="checked"';
                      break;
                      }
                  }
              ?>
              <input type="checkbox" class="checkbox" name="groups[]" value="<?php echo $group['id'];?>" <?php echo $checked;?>>
              <strong><?php echo htmlspecialchars($group['name'],ENT_QUOTES,'UTF-8');?></strong><br/>
              <small><?php echo htmlspecialchars($group['description'],ENT_QUOTES,'UTF-8');?></small>
              </li>          
            <?php endforeach?>
</ul>
</div>
      <?php endif ?>
</div>
      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>
<div class="form-group">
<div class="col-lg-8">
<button type="submit" class="btn btn-lg btn-success">Save</button>
</div>
</div>
<?php echo form_close();?>
</div>
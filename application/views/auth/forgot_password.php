<div class="col-lg-12">
<h1>Forgot Password <small>Reset</small></h1>
<?php echo $this->notify->show();?>
<?php echo $message;?>
<?php echo form_open("auth/forgot_password",'class="form-horizontal"');?>

      <div class="form-group">
      <div class="col-lg-12">
      	<label for="email"><?php echo sprintf(lang('forgot_password_email_label'), $identity_label);?></label> <br />
      	<?php echo form_input($email);?>
      </div>
      </div>
      <div class="form-group">
      <div class="col-lg-12">
      <?php echo form_submit('submit', lang('forgot_password_submit_btn'),'class="btn btn-warning btn-lg"');?>
      </div>
      </div>
<?php echo form_close();?>
</div>
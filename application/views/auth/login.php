<div class="col-lg-12">
<h1>Login</h1>
<?php echo $this->notify->show();?>
<?php echo $message;?>
<?php echo form_open("auth/login",'class="form-horizontal"');?>

  <div class="form-group">
  <div class="col-lg-12">
    <label for="identity"><?php echo lang('login_identity_label', 'identity');?></label>
    <?php echo form_input($identity);?>
  </div>
  </div>

  <div class="form-group">
  <div class="col-lg-12">
    <label for="password"><?php echo lang('login_password_label', 'password');?></label>
    <a href="forgot_password" class="pull-right"><?php echo lang('login_forgot_password');?></a>
    <?php echo form_input($password);?>
  </div>
  </div>

  <div class="form-group">
  <div class="col-lg-12">
    <?php echo lang('login_remember_label', 'remember');?>
    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
  </div>
  </div>
  <div class="form-group">
  <div class="col-lg-6">
     <?php echo form_submit('submit', lang('login_submit_btn'),'class="btn btn-success btn-lg"');?>
     </div>
     </div>
<?php echo form_close();?>
</div>
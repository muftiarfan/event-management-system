<div class="col-lg-12">
<h1><?php echo lang('reset_password_heading');?></h1>
<?php echo $this->notify->show();?>
<?php echo $message;?>
<?php echo form_open('auth/reset_password/' . $code,'class="form-horizontal"');?>

<div class="form-group">
<div class="col-lg-6">
		<label for="new_password"><?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length);?></label>
		<?php echo form_input($new_password);?>
</div>
</div>

<div class="form-group">
<div class="col-lg-6">
		<label for="new_confirm"><?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm');?></label>
		<?php echo form_input($new_password_confirm);?>
</div>
</div>

	<?php echo form_input($user_id);?>
	<?php echo form_hidden($csrf); ?>

<div class="form-group">
<div class="col-lg-6">
<button class="btn btn-lg btn-success" type="submit">Change</button>
</div>
</div>
<?php echo form_close();?>
</div>
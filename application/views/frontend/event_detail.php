<?php
use Carbon\Carbon;
?>
<?php foreach($events as $event):?>
	<?php

	$date = $event->event_date;
	$date = Carbon::parse($date);
	
	?>
	<?php
$organizer = $this->eventM->organizerDetails($event->event_organizerID); 
?>

<!-- ## PAGE TITLE ## -->
	<section id="qcSecbar">
		<div class="qcContainer">
			<h1><?php echo $event->name;?> <span>Know more about <?php echo $event->name;?></span></h1>
		</div>
	</section>

	<!-- ## PAGE CONTENT ## -->
	<section id="qcContent">
		<div class="qcContainer">

			<!-- ## MOB NAV ## -->
			<div id="qcMbTrigger"></div>

			<!-- ## TABS ## -->
			<div id="qcTabs" class="tabs">

				<!-- ## TAB NAV ## -->
				<ul id="qcTabNav" class="clearfix">
					<li><a href="#tab-1"><i class="icon-book-open icon"></i> <span>About</span></a></li>
					<li><a href="#tab-2"><i class="icon-clock-1 icon"></i> <span>Sessions</span></a></li>
					<li><a href="#tab-4"><i class="icon-map icon"></i> <span>Venue</span></a></li>
				</ul>


				<!-- ===============================================

					PAGE 1 - ABOUT

				=============================================== -->
				<div id="tab-1" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="stretch row clearfix">

						<!-- ## TAB TITLE & DESC ## -->
						<div class="col-12 col">
							<div class="qcTabTitle">
								<h4>About<span> Know more about the event</span></h4>
								<strong><span class="fa fa-calendar"></span></strong><small class="text-muted"> <?php echo $date->formatLocalized('%A, %d %B %Y'); ?></small> | <strong><span class="fa  fa-map-marker"></span></strong><small class="text-muted"> <?php echo $event->location;?></small> | <strong><span class="fa fa-inbox"></span></strong><small class="text-muted"> <?php echo $organizer['name'];?></small>	
							</div>

							<p class="qcPageDesc">
							<?php echo $event->description;?>
							</p>
						</div>

					</div>
					<!-- ## ROW END ## -->


				</div>
				<!-- ## PAGE 1 END ## -->



				<!-- ===============================================

					PAGE 2 - Sessions

				=============================================== -->
				<div id="tab-2" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="row clearfix">

						<!-- ## TAB TITLE ## -->
						<div class="col-12 col">
							<div class="qcTabTitle no-border">
								<h4>Sessions<span> List of sessions</span></h4>
							</div>
						</div>

					</div>
					<!-- ## ROW END ## -->

					<!-- ## ROW ## -->
					<div class="dblBorder">
						<div class="row clearfix">
							<div class="col-12 col">

								<!-- ## SCHEDULE LIST ## -->
								<div id="qcScheduleWrapper">
								<table class="table table-stripped table-hover">
								<thead>
								<tr>
								<th>#</th>
								<th>Session</th>
								</tr>
								</thead>
								<tbody>
								<?php foreach($sessions as $session): ?>
									<tr>
									<td><?php echo $session->PID;?></td>
									<td><?php echo $session->name;?></td>
									</tr>
								<?php endforeach;?>
								</tbody>
								</table>
								</div>
								<!-- ## SCHEDULE LIST END ## -->

							</div>
						</div>
					</div>
					<!-- ## ROW END ## -->

					<!-- ## ROW END ## -->

				</div>
				<!-- ## PAGE 2 END ## -->






				<!-- ===============================================

					PAGE 4 - VENUE

				=============================================== -->
				<div id="tab-4" class="qcTabPage clearfix">

					<!-- ## ROW ## -->
					<div class="row clearfix">

						<!-- ## TAB TITLE ## -->
						<div class="col-5 col">
							<div class="qcTabTitle no-border">
								<h4>Venue &amp; Organizer<span> Venue &amp; Organizer of the event</span></h4>
							</div>
						</div>

						<!-- ## ADDRESS LIST ## -->
						<div class="col-7 col">
							<ul class="qcAddress">
								<li><i class="fa  fa-map-marker"></i><p><strong>ADDRESS</strong>: <?php echo $event->location;?></p></li>
								<li><i class="fa fa-user"></i><p><strong>NAME</strong>:  <?php echo $organizer['name'];?></p></li>
								<li><i class="fa fa-mobile"></i><p><strong>PHONE</strong>:  +91 <?php echo $organizer['contact'];?></p></li>
								<li><i class="fa fa-inbox"></i><p><strong>EMAIL</strong>: <?php echo $organizer['email'];?></p></li>
								<!--li><i class="fa fa-globe"></i><p><strong>WEBSITE</strong>: <a title="website" href="#" target="_blank">webddegg.in</a></p></li-->
							</ul>
						</div>

					</div>
					<!-- ## ROW END ## -->

					<!-- ## ROW ## -->
					<div class="dblBorder">
						<div class="row clearfix">
							<div class="col-12 col">

								<div class="dblBorder only"></div>

								<!-- ## VENUE MAP ## -->
								<div class="qcEventlayout">
									<h1>Map</h1>
									<div id="qcContactMap"></div>
									<div id="qcMapAddress" data-lat="37.4416" data-lng="-122.1516" data-add="1600, Amphitheatre Parkway, Mountain View, CA 94043"></div>
								</div>

							</div>
						</div>
					</div>
					<!-- ## ROW END ## -->

				</div>
				<!-- ## PAGE 4 END ## -->


			</div>
			<!-- ## TABS END ## -->

		</div>
	</section>
	<!-- ## PAGE CONTENT END ## -->

<?php endforeach;?>

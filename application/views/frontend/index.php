
	<script type="text/javascript">
	function slide_fullscreen() {
	jQuery(function($){
		$.supersized({
			// Functionality
			slide_interval          :   8000,
			transition              :   1, // 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
			transition_speed	:   1000,
			// Components
			slide_links		:   'blank', // Individual links for each slide (Options: false, 'num', 'name', 'blank')
			progress_bar	:	1,
			slides 			:   [
								{	image : '<?php echo base_url();?>uploads/events_banners/ems_techknow.jpg'
								}
							]
		});
	});
	}
	window.onload = slide_fullscreen;
	</script>
<!-- ## HEADER ## -->
<header id="qcHomeHeader">
	<div class="row">

		<!-- ## LOGO ## -->
		<div id="qcLogo" class="col-6 col">
			<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/v2/img/placeholder/logo-header.png" alt="" /></a>
		</div>

		<!-- ## SITE NAVIGATION ## -->
		<nav id="qcPriNav" class="col-6 col">
			<ul class="clearfix">
				<?php
				foreach($event_upcoming as $upcoming):
				?>
				<li><a href="<?php echo base_url();?>events/<?php echo $upcoming->url_slug;?>"><i class="icon-calendar-2 icon"></i> <span>Event Details</span></a></li>
				<li><a href="<?php echo base_url();?>register/<?php echo $upcoming->EID;?>"><i class="icon-ticket icon"></i> <span>Register</span></a></li>
				<?php
				endforeach;
				?>
			</ul>
		</nav>
	</div>
	<div class="row">

		<!-- ## COUNTDOWN TIMER ## -->
		<div id="qcEventCountDown" class="col-6 col">
			<!-- ## DAYS ## -->
			<div class="dash days_dash">
				<div class="dash_title">days</div>
				<div class="digits clearfix">
					<div class="digit digit-1">0</div>
					<div class="digit digit-2">0</div>
					<div class="digit digit-3">0</div>
				</div>
			</div>
			<!-- ## HOURS ## -->
			<div class="dash hours_dash">
				<div class="dash_title">hours</div>
				<div class="digits clearfix">
					<div class="digit digit-1">0</div>
					<div class="digit digit-2">0</div>
				</div>
			</div>
			<!-- ## MINUTES ## -->
			<div class="dash minutes_dash">
				<div class="dash_title">minutes</div>
				<div class="digits clearfix">
					<div class="digit digit-1">0</div>
					<div class="digit digit-2">0</div>
				</div>
			</div>
			<!-- ## SECONDS ## -->
			<div class="dash seconds_dash">
				<div class="dash_title">seconds</div>
				<div class="digits clearfix">
					<div class="digit digit-1">0</div>
					<div class="digit digit-2">0</div>
				</div>
			</div>
		</div>

		<!-- ## EVENT BANNER ## -->
		<div id="qcEventBanner" class="col-6 col">
			<ul>
			<?php
			if(!$events):
				?>
			<li><b>No Upcoming events</b></li>
<?php
endif;
?>
<?php foreach($events as $event):?>
  <li>
  <a href="<?php echo base_url();?>events/<?php echo $event->url_slug;?>" title="Event details for <?php echo $event->name;?>">
    <b><?php echo $event->name;?></b></a>  <?php echo $event->event_date;?></li>
<?php endforeach;?>
			</ul>
		</div>

	</div>
</header>
<!-- ## HEADER END ## -->

<!-- ## FULLSCREEN SLIDES ## -->
<section id="slideContent">

	<!-- ## SLIDE CONTROLS ## -->
	<div id="qcHomeSlideControls">

		<!-- ## SLIDE DOT NAV ## -->
		<ul id="slide-list"></ul>

		<!-- ## PROGRESS BAR ## -->
		<div id="progress-back" class="load-item">
			<div id="progress-bar"></div>
		</div>

	</div>

</section>
<!-- ## FULLSCREEN SLIDES ## -->

<!-- ## BACKGROUND OVERLAY ## -->
<section id="qcOverlay">
	<!--div class="loading text-center">
		<!--a href="#">
			<!--img src="<?php echo base_url();?>assets/v2/img/placeholder/center-logo.png" alt="" />
		</a-->
		<div class="btn-group">
		<a href="<?php echo base_url();?>register/<?php echo $upcoming->EID;?>" class="btn btn-lg btn-warning">Register as a school</a>
		<a href="<?php echo base_url();?>register/individual/<?php echo $upcoming->EID;?>" class="btn btn-lg btn-primary">Register as an individual</a>
		</div>
	<!--/div-->
</section>
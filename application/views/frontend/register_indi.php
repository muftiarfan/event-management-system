<?php foreach($events as $event):
$organizer = $this->eventM->organizerDetails($event->event_organizerID); 
?>
<!-- ## PAGE TITLE ## -->
    <section id="qcSecbar">
        <div class="qcContainer">
            <h1>Ticket. <span>Reserve seat for yourself as an individual attendee.</span></h1>
        </div>
    </section>

    <!-- ## PAGE CONTENT ## -->
    <section id="qcContent">
        <div class="qcContainer">

            <!-- ## MOB NAV ## -->
            <div id="qcMbTrigger"></div>

            <!-- ## TABS ## -->
            <div id="qcTabs" class="tabs">

                <!-- ## TAB NAV ## -->
                <ul id="qcTabNav" class="clearfix">
                    <li><a href="#tab-1"><i class="icon-ticket icon"></i> <span>Book Ticket</span></a></li>
                    <li><a href="#tab-2"><i class="icon-book-open icon"></i> <span>FAQ's</span></a></li>
                    <li><a href="#tab-3"><i class="icon-lkdto icon"></i> <span>T & C</span></a></li>
                    <li><a href="#tab-4"><i class="icon-mail-1 icon"></i> <span>Contact</span></a></li>
                    <?php
                    if($event->detail_file!=NULL):
                    ?>
                    <li><a href="<?php echo base_url().$event->detail_file;?>"><i class="icon-book-open icon"></i> <span>Brochure</span></a></li>
                    <?php
                    endif;
                    ?>
                </ul>


                <!-- ===============================================

                    PAGE 1 - TICKET

                =============================================== -->
                <div id="tab-1" class="qcTabPage clearfix">

                    <!-- ## ROW ## -->
                    <div class="ticket row clearfix">

                        <!-- ## TAB TITLE & DESC ## -->
                        <div class="col-4 col" >
                            <div class="qcTabTitle no-border">
                                <h4>Book ticket<span> Hurry ! Limited attendees only</span></h4>
                                <p class="qcPageDesc full">
                                    <?php if($event->registration_extra_info!=''):?>
                                        <div class="alert alert-warning"><strong>Attention:</strong><?php echo $event->registration_extra_info;?></div>
                                    <?php endif;?>
                                <a class="btn btn-md btn-default" href="<?php echo base_url();?>register/<?php echo $event->EID;?>">I want a ticket for my team.</a>  
                                </p>
                            </div>
                        </div>

                        <!-- ## TICKET ## -->
                        <div class="col-8 col">
                            <div class="box no-border nopad">
                                <div class="qcTcktRegForm">
                                
                                    <!-- ## MODULE TITLE ## -->
                                    <div class="qcModTitle">
                                        <h1><?php echo $event->name;?></h1>
                                        <p>
To be able to register yourself as an individual attendee, please fill out the form below.
                                        </p>
                                    </div>
<?php echo $this->notify->show(); ?>
<?php echo validation_errors(); ?>
<h2><span class="label label-success label-lg">1</span> Basic Details</h2>
<?php echo form_open_multipart('frontend/register_indi/'.$event->EID,'class="form-horizontal"');?>
<input type="hidden" name="eid" value="<?php echo $event->EID;?>">
<div class="form-group">
<div class="col-lg-7">
<label for="name">Name</label>
<input type="text" pattern="[a-zA-Z\s]+" required="required" class="form-control" name="name" id="name" placeholder="Full Name" value="<?php echo set_value('name');?>">
</div>
<div class="col-lg-5">
<label for="email">Email</label>
<input type="email" class="form-control" name="email" id="email" placeholder="email@company.com" value="<?php echo set_value('email');?>">
</div>
</div>
<div class="form-group">
<div class="col-lg-6">
<label for="mobile">Mobile</label>
<input type="text" required="required" class="form-control" id="mobile" name="mobile" placeholder="Mobile Number" value="<?php echo set_value('mobile');?>">
</div>
<div class="col-lg-6">
<label for="mobile">Phone</label>
<input type="text" class="form-control" id="phone" name="phone" placeholder="Landline" value="<?php echo set_value('phone');?>">
</div>
</div>
<div class="form-group">
<div class="col-lg-12">
<label for="t_org">Team/Organization/Institution</label>
<input type="text" pattern="[a-zA-Z\s]+" required="required" class="form-control" name="t_org" id="t_org" placeholder="Name of your team/organization/School" value="<?php echo set_value('t_org');?>">
</div>
</div>
<div class="form-group">
<div class="col-lg-6">
<label for="dob">Birthday</label>
                <div class="input-group date" id='datetimepicker1'>
    <input type="text" required="required" class="form-control input-lg" id="dob" name="dob" placeholder="Birthday" value="<?php echo set_value('dob');?>">
                        <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
</div>
<div class="col-lg-6">
<label for="member_gender">Gender</label>
<select class="form-control" id="member_gender" name="member_gender">
<option value="Male">Male</option>
<option value="Female">Female</option>
</select>
</div>
</div>
<div class="form-group">
<div class="col-lg-8">
<label for="session">Session to attend</label>
<select name="session" class="form-control" id="session">
<?php foreach($sessions as $session): ?>
<option value="<?php echo $session->PID;?>"><?php echo $session->name;?></option>
<?php endforeach;?>
</select>
</div>
<div class="col-lg-4">
<label for="t_org_card">Team/Organization ID Card</label>
<input type="file" name="t_org_card" id="t_org_card" />
</div>
</div>

<div class="form-group">
<div class="col-lg-12">
<button class="btn btn-success btn-lg">Register</button>
</div>
</div>
<?php echo form_close();?>
<?php endforeach;?>
                                </div>
                                <!-- ## TICKET FORM END ## -->
                            </div>
                        </div>
                        <!-- ## TICKET END ## -->

                    </div>
                    <!-- ## ROW END ## -->

                </div>
                <!-- ## PAGE 1 END ## -->



                <!-- ===============================================

                    PAGE 2 - FAQ's

                =============================================== -->
                <div id="tab-2" class="qcTabPage clearfix">

                    <!-- ## ROW ## -->
                    <div class="row clearfix">

                        <!-- ## TAB TITLE ## -->
                        <div class="col-12 col" >
                            <div class="qcTabTitle no-border">
                                <h4>FAQ's<span> Frequently asked questions</span></h4>
                            </div>
                        </div>


                    </div>
                    <!-- ## ROW END ## -->

                    <!-- ## ROW ## -->
                    <div class="dblBorder">
                        <div class="row clearfix">
                            <div class="col-12 col">

                                <!-- ## FAQ's LIST ## -->
                                <div class="qcEventlayout">
                                    <?php
                                    echo $event->event_faq;
                                    ?>

                                </div>
                                <!-- ## FAQ's END ## -->

                            </div>
                        </div>
                    </div>
                    <!-- ## ROW END ## -->

                </div>
                <!-- ## PAGE 2 END ## -->



                <!-- ===============================================

                    PAGE 3 - TERMS & CONDITIONS

                =============================================== -->
                <div id="tab-3" class="qcTabPage clearfix">

                    <!-- ## ROW ## -->
                    <div class="row clearfix">

                        <!-- ## TAB TITLE ## -->
                        <div class="col-12 col" >
                            <div class="qcTabTitle no-border">
                                <h4>T & C<span> Terms & conditions for the event</span></h4>
                            </div>
                        </div>

                    </div>
                    <!-- ## ROW END ## -->

                    <!-- ## ROW ## -->
                    <div class="dblBorder">
                        <div class="row clearfix">
                            <div class="col-12 col">

                                <!-- ## T & C LIST ## -->
                                <div class="qcEventlayout">
                                    <?php echo $event->ticket_extra_info;?>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- ## ROW END ## -->


                </div>
                <!-- ## PAGE 3 END ## -->



                <!-- ===============================================

                    PAGE 4 - CONTACT

                =============================================== -->
                <div id="tab-4" class="qcTabPage clearfix">

                    <!-- ## ROW ## -->
                    <div class="row clearfix">

                        <!-- ## TAB TITLE ## -->
                        <div class="col-6 col" >
                            <div class="qcTabTitle no-border">
                                <h4>Contact<span> Organizer contact details</span></h4>
                            </div>
                        </div>

                        <!-- ## TAB DESC ## -->
                        <div class="col-6 col">
                            <ul class="qcAddress">
                                <li><i class="fa  fa-map-marker"></i><p><strong>ADDRESS</strong>: <?php echo $event->location;?></p></li>
                                <li><i class="fa fa-user"></i><p><strong>NAME</strong>:  <?php echo $organizer['name'];?></p></li>
                                <li><i class="fa fa-mobile"></i><p><strong>PHONE</strong>:  +91 <?php echo $organizer['contact'];?></p></li>
                                <li><i class="fa fa-inbox"></i><p><strong>EMAIL</strong>: <?php echo $organizer['email'];?></p></li>
                                <!--li><i class="fa fa-globe"></i><p><strong>WEBSITE</strong>: <a title="website" href="#" target="_blank">webdegg.in</a></p></li-->
                            </ul>
                        </div>

                    </div>
                    <!-- ## ROW END ## -->

                

                </div>
                <!-- ## PAGE 4 END ## -->



            </div>
            <!-- ## TABS END ## -->

        </div>
    </section>
    <!-- ## PAGE CONTENT END ## -->

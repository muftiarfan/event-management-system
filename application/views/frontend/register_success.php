<div class="container">
<div class="col-lg-12">
<div class="panel panel-success">
<div class="panel-heading"><h1>Thank You.</h1></div>
<div class="panel-body">
<p>
Thank you, your request has been received. You will shortly recieve an email/sms, please click on the link to confirm your account.
</p>
<p class="text text-info">
Note: If you don't find the email in your inbox, please check spam/junk folder.
</p>
</div>
</div>
</div>
</div>
<!DOCTYPE html>
<html>
<head>
<title><?php echo $title;?> :: EMS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Mufti Arfan,Webdegg">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.flat.css">
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/ems.front.css">
<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<?php isset($_styles) and print $_styles;?>
<script type="text/javascript">
  var baseUrl = '<?php echo base_url();?>';
</script>
<script src="<?php echo base_url();?>assets/js/jquery-2.1.4.min.js"></script>
</head>
<body>

<div class="container">
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url('home');?>">WD EMS</a>
    </div>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="<?php if($this->uri->segment(1)=="home" || $this->uri->segment(1)==""){echo "active";}?>"><a href="<?php echo base_url('home');?>">Home</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
      <?php
      if($this->ion_auth->logged_in()):
        $user =  $this->ion_auth->user()->row();
      ?>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->username;?> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="<?php echo base_url();?>user/dashboard"><span class="fa fa-w fa-cog"></span> Dashboard</a>
                        </li>
                                                <li class="divider"></li>

                        <li>
                            <a href="<?php echo base_url();?>auth/logout"><span class="fa fa-w fa-sign-out"></span> Logout</a>
                        </li>
                    </ul>
                </li>      <?php else: ?>
        <li><a href="<?php echo base_url();?>auth/login" title="Login" class="<?php if($this->uri->segment(2)=="login"){echo "active";}?>">Login</a></li>
      <?php endif; ?>
      </ul>
    </div>
  </div>
</nav>
<?php if(!$this->ion_auth->logged_in()):?>
<!-- Modal -->
<div id="login_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Already Registered?</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-lg-12">
        <?php echo form_open('auth/login','class="form-horizontal"');?>
        <div class="form-group">
        <div class="col-lg-12">
        <label for="identity">Email</label>
        <input type="email" class="form-control" placeholder="Email" name="identity" id="identity"/>

        </div>
        </div>        
        <div class="form-group">
        <div class="col-lg-12">
        <label for="password">Password</label>
        <input type="password" placeholder="Password" class="form-control" name="password" id="password"/>

        </div>
        </div>        
        <div class="form-group">
        <div class="col-lg-12">
        <button type="submit" class="btn btn-lg btn-success">Login</button>
        </div>
        </div>
        <?php echo form_close();?>
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Nope</button>
      </div>
    </div>

  </div>
</div>
<?php endif;?>
<div class="row">
<?php echo $content;?>
</div>
<hr>
<footer>
        <div class="row">
          <div class="col-lg-12">
            <p>
            <span class="text text-muted">EMS V<?php echo APP_VERSION;?></span>
            Made by <a href="http://webdegg.in" rel="nofollow">WebDegg</a>.</p>
            <p class="text text-right">&copy; <?php echo date('Y');?> All rights reserved.
          </div>
        </div>

      </footer>
</div>
<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url();?>assets/js/js.cookie.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  <?php isset($_scripts) and print $_scripts;?>
<?php if(!$this->ion_auth->logged_in()):?>
<script type="text/javascript">
    $(window).load(function() {
    if (Cookies.get('modal_shown') == null) {
        Cookies.set('modal_shown', 'yes', { expires: 1, path: '/' });
        $('#login_modal').modal('show');
    }
});
</script>
<?php endif;?>
</body>
</html>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>EMS | <?php echo $title;?></title>
	<meta name="author" content="Mufti Arfan,WebDegg">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/v2/img/placeholder/favicon.ico">
	<link rel="stylesheet" media="all" href="<?php echo base_url();?>assets/v2/css/style.css"/>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Raleway:100,300,400' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'>
	<?php isset($_styles) and print $_styles;?>
	<script type="text/javascript">
	var baseUrl = '<?php echo base_url();?>';
	</script>
	<script src="<?php echo base_url();?>assets/v2/js/2.1.1.jquery.min.js" type="text/javascript"></script>

</head>

<body>

<div class="content">
<?php if(!$this->ion_auth->logged_in()):?>
<!-- Modal -->
<div id="login_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Already Registered?</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-lg-12">
        <?php echo form_open('auth/login','class="form-horizontal"');?>
        <div class="form-group">
        <div class="col-lg-12">
        <label for="identity">Email</label>
        <input type="email" class="form-control" placeholder="Email" name="identity" id="identity"/>

        </div>
        </div>        
        <div class="form-group">
        <div class="col-lg-12">
        <label for="password">Password</label>
        <input type="password" placeholder="Password" class="form-control" name="password" id="password"/>

        </div>
        </div>        
        <div class="form-group">
        <div class="col-lg-12">
        <button type="submit" class="btn btn-lg btn-success">Login</button>
        </div>
        </div>
        <?php echo form_close();?>
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Nope</button>
      </div>
    </div>

  </div>
</div>
<?php endif;?>
<?php echo $content; ?>

</div>
<script src="<?php echo base_url();?>assets/js/js.cookie.js"></script>
<script src="<?php echo base_url();?>assets/v2/js/supersized.3.2.7.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/v2/js/countdown.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/v2/js/jquery.marquee.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/v2/js/library.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/v2/js/init.js" type="text/javascript"></script>
<?php isset($_scripts) and print $_scripts;?>
<?php if(!$this->ion_auth->logged_in()):?>
<script type="text/javascript">
    $(window).load(function() {
    if (Cookies.get('modal_shown') == null) {
        Cookies.set('modal_shown', 'yes', { expires: 1, path: '/' });
        $('#login_modal').modal('show');
    }
});
</script>
<?php endif;?>
</body>
</html>
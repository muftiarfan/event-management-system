<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<title>EMS | <?php echo $title;?></title>
	<meta name="author" content="Mufti Arfan,WebDegg">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/v2/img/placeholder/favicon.ico">
	<link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" media="all" href="<?php echo base_url();?>assets/v2/css/style.css"/>
	<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.min.css">
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Raleway:100,300,400' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Architects+Daughter' rel='stylesheet' type='text/css'>
<?php isset($_styles) and print $_styles;?>
<script type="text/javascript">
  var baseUrl = '<?php echo base_url();?>';
</script>
<script src="<?php echo base_url();?>assets/js/jquery-2.1.4.min.js"></script>

</head>

<body>

<header id="qcHeader">
	<div class="row">

		<div id="qcLogo" class="col-6 col">
			<a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/v2/img/placeholder/logo-header.png" alt="" /></a>
		</div>

		<?php
		echo $navigation;
		?>

	</div>
</header>

<div id="qcContentWrapper">
<?php echo $content; ?>
</div>
<?php if(!$this->ion_auth->logged_in()):?>
<!-- Modal -->
<div id="login_modal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Already Registered?</h4>
      </div>
      <div class="modal-body">
        <div class="row">
        <div class="col-lg-12">
        <?php echo form_open('auth/login','class="form-horizontal"');?>
        <div class="form-group">
        <div class="col-lg-12">
        <label for="identity">Email</label>
        <input type="email" class="form-control" placeholder="Email" name="identity" id="identity"/>

        </div>
        </div>        
        <div class="form-group">
        <div class="col-lg-12">
        <label for="password">Password</label>
        <input type="password" placeholder="Password" class="form-control" name="password" id="password"/>

        </div>
        </div>        
        <div class="form-group">
        <div class="col-lg-12">
        <button type="submit" class="btn btn-lg btn-success">Login</button>
        </div>
        </div>
        <?php echo form_close();?>
        </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Nope</button>
      </div>
    </div>

  </div>
</div>
<?php endif;?>
<footer id="qcFooter" class="clearfix">
	<div class="qcContainer">

		<div class="col-5 col">
			<nav id="qcFooterNav">
				<ul class="clearfix">
					<li><a href="<?php echo base_url();?>">Home</a></li>
					<?php
					if($this->ion_auth->logged_in()):
					?>
					<li><a href="<?php echo base_url();?>user/dashboard" title="Goto dashboard"><?php echo $this->ion_auth->user()->row()->username;?></a></li>
					<?php
					else:
						?>
					<li><a href="<?php echo base_url();?>auth/login">Login</a></li>
					<?php
					endif;
					?>
				</ul>
			</nav>
		</div>

		<div id="qcFooterLogo" class="col-2 col">
			<a href="#">
				<img src="<?php echo base_url();?>assets/v2/img/placeholder/logo-footer.png" alt="LOGO" />
			</a>
		</div>

		<div id="qcFooterPara" class="col-5 col">
			<p>EMS V<?php echo APP_VERSION;?> <br /> <span>&copy; <?php echo date('Y');?> Copyright <a href="http://webdegg.in">Webdegg</a></span></p>
		</div>

	</div>
</footer>

<script src="<?php echo base_url();?>assets/js/js.cookie.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/v2/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/v2/js/library.js" type="text/javascript"></script>
<script src="<?php echo base_url();?>assets/v2/js/init.js" type="text/javascript"></script>
  <?php isset($_scripts) and print $_scripts;?>
<?php if(!$this->ion_auth->logged_in()):?>
<script type="text/javascript">
    $(window).load(function() {
    if (Cookies.get('modal_shown') == null) {
        Cookies.set('modal_shown', 'yes', { expires: 1, path: '/' });
        $('#login_modal').modal('show');
    }
});
</script>
<?php endif;?>
</body>

</html>
<!DOCTYPE html>
<html>
<head>
<title><?php echo $title;?> :: EMS</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Mufti Arfan,Webdegg">
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>assets/css/bootstrap.css">
<?php isset($_styles) and print $_styles;?>
</head>
<body>

<?php echo $content;?>

<!-- Latest compiled and minified JavaScript -->
<script src="<?php echo base_url();?>assets/js/jquery-2.1.4.min.js"></script>
<script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
  <?php isset($_scripts) and print $_scripts;?>
</body>
</html>
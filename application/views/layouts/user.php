<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="Mufti Arfan,Webdegg">

    <title><?php echo $title;?> :: EMS</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url();?>assets/css/bootstrap.min.flat.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url();?>assets/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    
    <?php isset($_styles) and print $_styles;?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- jQuery -->
    <script src="<?php echo base_url();?>assets/js/jquery-2.2.3.min.js"></script>
    <script type="text/javascript">
  var baseUrl = '<?php echo base_url();?>';
</script>
</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="<?php echo base_url();?>">EMS</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu message-dropdown">
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-preview">
                            <a href="#">
                                <div class="media">
                                    <span class="pull-left">
                                        <img class="media-object" src="http://placehold.it/50x50" alt="">
                                    </span>
                                    <div class="media-body">
                                        <h5 class="media-heading"><strong>John Smith</strong>
                                        </h5>
                                        <p class="small text-muted"><i class="fa fa-clock-o"></i> Yesterday at 4:32 PM</p>
                                        <p>Lorem ipsum dolor sit amet, consectetur...</p>
                                    </div>
                                </div>
                            </a>
                        </li>
                        <li class="message-footer">
                            <a href="#">Read All New Messages</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell"></i> <b class="caret"></b></a>
                    <ul class="dropdown-menu alert-dropdown">
                        <li>
                            <a href="#">Alert Name <span class="label label-default">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-primary">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-success">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-info">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-warning">Alert Badge</span></a>
                        </li>
                        <li>
                            <a href="#">Alert Name <span class="label label-danger">Alert Badge</span></a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="#">View All</a>
                        </li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $user->username;?><b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?php echo base_url();?>user/profile" title="View Profile"><i class="fa fa-fw fa-user"></i> Profile</a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>auth/edit_user/<?php echo $user->id;?>"><i class="fa fa-fw fa-gear"></i> Edit</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo base_url();?>auth/logout"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="<?php if($this->uri->segment(2)=="dashboard" || $this->uri->segment(2)==""){echo "active";}?>">
                        <a href="<?php echo base_url();?>user/dashboard"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <?php if($this->ion_auth->in_group(array(1,2,5,6))):?>
                    <li class="<?php if($this->uri->segment(1)=="event" || $this->uri->segment(1)=="user" && $this->uri->segment(2)=='events'){echo "active";}?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#events"><i class="fa fa-fw fa-calendar"></i> Events <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="events" class="collapse">
                        <?php if($this->ion_auth->in_group(array(1,10))):?>
                            <li>
                            <a class="<?php if($this->uri->segment(1)=="event" && $this->uri->segment(2)=="add"){echo "active";}?>" href="<?php echo base_url();?>event/add">Add</a>
                            </li>   
                            <?php endif; ?>  
                            <?php if($this->ion_auth->in_group(array(1,12))):?>                       
                            <li>
                            <a class="<?php if($this->uri->segment(1)=="event" && $this->uri->segment(2)=="icards"){echo "active";}?>" href="<?php echo base_url();?>event/icards">IDCards</a>
                            </li>
                        <?php endif;?>
                        <?php if($this->ion_auth->in_group(array(1,13))):?>
                            <li>
                            <a class="<?php if($this->uri->segment(1)=="event" && $this->uri->segment(2)=="event_organizers"){echo "active";}?>" href="<?php echo base_url();?>event/event_organizers">Organizers</a>
                            </li>

                        <?php endif;?>
                            <li>
                                <a class="<?php if($this->uri->segment(2)=="events"){echo "active";}?>" href="<?php echo base_url();?>user/events">View</a>
                            </li>
                        </ul>
                    </li> 
                <?php endif;?>
                <?php if($this->ion_auth->in_group(array(1,8,9))):?>
                    <li class="<?php if($this->uri->segment(1)=="fees"){echo "active";}?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#fees"><i class="fa fa-fw fa-rupee"></i> Fees <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="fees" class="collapse">
                        <?php if($this->ion_auth->in_group(array(1,8))):?>                   
                           <li>
                            <a class="<?php if($this->uri->segment(1)=="fees" && $this->uri->segment(2)=="view"){echo "active";}?>" href="<?php echo base_url();?>fees/view">View</a>
                            </li>
                        <?php endif; ?>
                            <?php if($this->ion_auth->in_group(array(1,9))):?>                   
                            <li>
                            <a class="<?php if($this->uri->segment(1)=="fees" && $this->uri->segment(2)=="add"){echo "active";}?>" href="<?php echo base_url();?>fees/add">Add</a>
                            </li>
                            <?php endif;?>
                        </ul>
                    </li>
                    <?php endif;?>
                 <?php if($this->ion_auth->in_group(array(1,14))):?>                   
                    <li class="<?php if($this->uri->segment(1)=="users"){echo "active";}?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#users"><i class="fa fa-fw fa-users"></i> Users <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="users" class="collapse">
                            <li>
                            <a class="<?php if($this->uri->segment(1)=="users" && $this->uri->segment(2)=="view"){echo "active";}?>" href="<?php echo base_url();?>users/view">View</a>
                            </li>                              
                            <li>
                            <a class="<?php if($this->uri->segment(1)=="users" && $this->uri->segment(2)=="add"){echo "active";}?>" href="<?php echo base_url();?>users/add">Add</a>
                            </li>                            
                            <li>
                            <a class="<?php if($this->uri->segment(1)=="users" && $this->uri->segment(2)=="groups"){echo "active";}?>" href="<?php echo base_url();?>users/groups">Groups</a>
                            </li>
                        </ul>
                    </li>
                    <?php endif;?>
                    <?php if($this->ion_auth->in_group(array(1,17,18))):?>                   
                    <li class="<?php if($this->uri->segment(1)=="sms"){echo "active";}?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#sms"><i class="fa fa-fw fa-phone"></i> SMS <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="sms" class="collapse">
                            <li>
                            <a class="<?php if($this->uri->segment(1)=="sms" && $this->uri->segment(2)=="dashboard"){echo "active";}?>" href="<?php echo base_url();?>sms/dashboard">Dashboard</a>
                            </li>                              
                            <li>
                            <a class="<?php if($this->uri->segment(1)=="sms" && $this->uri->segment(2)=="compose"){echo "active";}?>" href="<?php echo base_url();?>sms/compose">Compose</a>
                            </li>                            
                        </ul>
                    </li>
                    <?php endif;?>
                    <?php $groups = array(1,3,4);
                    if($this->ion_auth->in_group($groups)):?>                   
                    <li class="<?php if($this->uri->segment(1)=="music"){echo "active";}?>">
                        <a href="javascript:;" data-toggle="collapse" data-target="#music"><i class="fa fa-fw fa-music"></i> Music <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="music" class="collapse">
                            <li>
                            <?php
                            $groups= array(1,3);
                            if($this->ion_auth->in_group($groups)):?>
                            <a class="<?php if($this->uri->segment(1)=="music" && $this->uri->segment(2)=="dashboard"){echo "active";}?>" href="<?php echo base_url();?>music/orders">Orders</a>
                            </li> 
                            <?php endif;?> 
                            <?php $groups= array(1,4);
                            if($this->ion_auth->in_group($groups)):?>                            
                            <li>
                            <a class="<?php if($this->uri->segment(1)=="music" && $this->uri->segment(2)=="add"){echo "active";}?>" href="<?php echo base_url();?>music/add">Add Songs</a>
                            </li>
                            <?php endif;?>                             
                            <?php if($this->ion_auth->is_admin()):?>                            
                            <li>
                            <a class="<?php if($this->uri->segment(1)=="music" && $this->uri->segment(2)=="settings"){echo "active";}?>" href="<?php echo base_url();?>music/settings">Settings</a>
                            </li>
                            <?php endif;?>                               
                        </ul>
                    </li>
                    <?php endif;?>
                </ul>

            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            <?php echo $title;?>
                        </h1>
                    </div>
                </div>
                <!-- /.row -->
                <div class="row">
                    <div class="col-lg-12">
                    <?php echo $this->notify->show();?>
                    </div>
                </div>
                <!-- /.row -->


                <div class="row">
                <?php echo $content;?>
                </div>
                <!-- /.row -->
                <hr>
<footer>
        <div class="row">
          <div class="col-lg-12">
            <p>
            <span class="text text-muted">EMS V<?php echo APP_VERSION;?></span>
            Made by <a href="http://webdegg.in" rel="nofollow">WebDegg</a>.</p>
            <p class="text text-right">&copy; <?php echo date('Y');?> All rights reserved.
          </div>
        </div>

      </footer>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    
    <script src="<?php echo base_url();?>assets/js/bootstrap.min.js"></script>
    <?php isset($_scripts) and print $_scripts;?>
    <script>
    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip(); 
    });
    </script>
</body>

</html>

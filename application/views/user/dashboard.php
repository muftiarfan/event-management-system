<div class="col-lg-12">
<?php
  $yesterday = date('Y-m-d',time()-(24*60*60));
  $date_epoch = date_create($yesterday); 
  $date_epoch = date_format($date_epoch, 'U');
?>
<div class="panel panel-info">
<div class="panel-heading"><h4>Upcoming Events<small> That you have registered for</small></h4></div>
<div class="panel-body">
<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>#</th>
      <th>Event</th>
      <th>Details</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($events_reg as $events):
  $event_details = $this->frontM->get_events('WHERE EID='.$events->EID.' AND status=1 AND date_epoch>'.$date_epoch.'');
  foreach($event_details as $event):
  ?>
    <tr>
      <td><?php echo $event->EID;?></td>
      <td><?php echo $event->name;?></td>
      <td>
        <span class="fa fa-calendar fa-1x"></span>
        <?php echo $event->event_date;?>
        <p class="text text-justify">
        <?php echo $event->description;?>
        </p>
      </td>
      <td>
      <div class="btn-group">
      <?php
      $added = $this->userM->already_members($event->EID,$user->id);
      ?>
      <a class="btn btn-sm btn-success" href="<?php echo base_url();?>user/event_register/<?php echo $event->EID;?>">Add Members</a>
      <?php
      if($added>0):
      ?>
      <a class="btn btn-sm btn-primary" href="<?php echo base_url();?>user/event_edit/<?php echo $event->EID;?>">Modify</a>
    <?php endif;?>
      <a class="btn btn-sm btn-default" target="_blank" href="<?php echo base_url();?>event/event_ticket/<?php echo $user->id;?>/<?php echo $event->EID;?>/<?php echo $user->ref_id;?>">Print Ticket</a>
      <?php
      if($event->detail_file!=NULL):
      ?>

      <a class="btn btn-sm btn-success" href="<?php echo base_url().$event->detail_file;?>">Brochure</a>
      <?php
      endif;
      ?>
      </div>
      </td>
    </tr>
  <?php endforeach;?>
  <?php endforeach;?>
  </tbody>
</table>
<?php
//foreach($events_reg2 as $user_events):
	//echo $user_events->EID;
	/*
	* Issue : If an event was deleted, that was upcoming but the user had added participants,(not deleted) then it returns TRUE 
	*/
  //Patch: 25-4-2016, just check if there was any attendees in db by this user for any event, thats it.
//$check = $this->userM->idData($user_events->EID,0,'event_passed');
if(!$events_reg2){
	?>
	<div class='well'>
	<p class="text text-center text-muted">
	<strong>You have not registered for any upcoming events yet. <br/><a href="<?php echo base_url();?>user/events" title="Register">Register</a></strong>
	</p>
	</div>
<?php
}
//endforeach;
?>
</div>
</div>
<?php 
?>
</div>
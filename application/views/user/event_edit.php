<div class="col-lg-12">
<?php echo validation_errors(); ?>
<?php
foreach($events as $event):
	//echo $test['cnt'];
if($test['cnt']<0){ //for incriminting purposes.
	$i=0;
}else{
	$i=$test['cnt'];
}
?>
<p class="pull-right">
<span class="text text-muted text-right">
Members allowed this event: <strong><?php echo $event->participants;?></strong>
</span>
<span class="text text-info text-right">
You have added: <strong><?php echo $test['cnt'];?> member(s)</strong>
<?php if($test['cnt']<$event->participants):?>
<a href="<?php echo base_url();?>user/event_register/<?php echo $this->uri->segment(3);?>" class="btn btn-xs btn-primary">Add More</a>
<?php endif;?>
</p>
<?php echo form_open('user/event_edit/'.$this->uri->segment(3),'class="form-horizontal"');?>
<?php
foreach($members as $member):
//while($i<$event->participants):
//	$i = $i+1;
//while($i<$event->participants):
//	$i++;
?>
<h4><strong>Member <?php echo $member->PID;?></strong></h4>
<div class="form-group">
<div class="col-lg-3">
<label for="member[<?php echo $member->PID;?>]">Name</label>
<input type="text" name="member[<?php echo $member->PID;?>]" placeholder="Name" class="form-control" value="<?php echo $member->name;?>">
</div>
<div class="col-lg-3">
<label for="member_email[<?php echo $member->PID;?>]">Email</label>
<input type="Email" name="member_email[<?php echo $member->PID;?>]" placeholder="Email" class="form-control" value="<?php echo $member->email;?>">
</div>
<div class="col-lg-2">
<label for="member_phone[<?php echo $member->PID;?>]">Phone</label>
<input type="text" name="member_phone[<?php echo $member->PID;?>]" placeholder="Mobile" class="form-control" value="<?php echo $member->phone;?>">
</div>
<div class="col-lg-1">
<label for="member_gender[<?php echo $member->PID;?>]">Gender</label>
<select class="form-control" id="member_gender[<?php echo $member->PID;?>]" name="member_gender[<?php echo $member->PID;?>]">

<option value="<?php echo $member->gender;?>" selected><?php echo $member->gender;?></option>
<option disabled value="0">Change?</option>
<option value="Male">Male</option>
<option value="Female">Female</option>
</select>
</div>
<div class="col-lg-2">
<label for="member_phone[<?php echo $member->PID;?>]">Session</label>
<select class="form-control" id="platform[<?php echo $member->PID;?>]" name="platform[<?php echo $member->PID;?>]">
<?php
$platform_name = $this->userM->idData($member->platform,$this->uri->segment(3),'platform_name');
$attendee_limit = $this->userM->idData($member->platform,0,'session_attendee_limit');
?>
<option value="<?php echo $member->platform;?>" selected><?php echo $platform_name;?> (Max attendees: <?php echo $attendee_limit;?>)</option>
<option disabled value="0">Other</option>
<?php foreach($platforms as $platform):
$attendee_limit = $this->userM->idData($platform->PID,0,'session_attendee_limit');
?>
<option value="<?php echo $platform->PID;?>"><?php echo $platform->name;?> (Max attendees: <?php echo $attendee_limit;?>)</option>
<?php endforeach;?>
</select>
</div>
</div>
<?php endforeach; ?>
<?php //endwhile; ?>
<div class="col-lg-4">
<button class="btn btn-lg btn-success" type="submit">Update</button>
</div>
<?php echo form_close();?>
<?php endforeach; ?>
</div>
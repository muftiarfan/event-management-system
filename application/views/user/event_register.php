<div class="col-lg-12">
<?php echo validation_errors(); ?>
<?php
foreach($events as $event):
	//echo $test['cnt'];
if($test['cnt']<0){ //for incriminting purposes.
	$i=0;
}else{
	$i=$test['cnt'];
}
?>
<div class="well well-sm">
<p>
Adding registrations for <strong><?php echo $event->name;?></strong>
</p>
</div>
<p class="pull-right">
<span class="text text-muted text-right">
Members allowed this event: <strong><?php echo $event->participants;?></strong>
</span>
<span class="text text-info text-right">
You have added: <strong><?php echo $test['cnt'];?> member(s)</strong>
</p>
<?php
while($i<$event->participants):
	$i = $i+1;
//while($i<$event->participants):
//	$i++;
?>
<h4><strong>Member <?php echo $i;?></strong></h4>
<?php echo form_open('user/event_register/'.$event->EID,'class="form-horizontal"');?>
<div class="form-group">
<div class="col-lg-3">
<label for="member[<?php echo $i;?>]">Name</label>
<input type="text" name="member[<?php echo $i;?>]" placeholder="Name" class="form-control">
</div>
<div class="col-lg-3">
<label for="member_email[<?php echo $i;?>]">Email</label>
<input type="Email" name="member_email[<?php echo $i;?>]" placeholder="Email" class="form-control">
</div>
<div class="col-lg-2">
<label for="member_phone[<?php echo $i;?>]">Phone</label>
<input type="text" name="member_phone[<?php echo $i;?>]" placeholder="Mobile" class="form-control">
</div>
<div class="col-lg-1">
<label for="member_gender[<?php echo $i;?>]">Gender</label>
<select class="form-control" id="member_gender[<?php echo $i;?>]" name="member_gender[<?php echo $i;?>]">
<option value="Male">Male</option>
<option value="Female">Female</option>
</select>
</div>
<div class="col-lg-2">
<label for="platform[<?php echo $i;?>]">Session</label>
<select class="form-control" id="platform[<?php echo $i;?>]" name="platform[<?php echo $i;?>]">
 <option value="0" selected disabled>Session</option>
<?php foreach($platforms as $platform):
	$attendee_limit = $this->userM->idData($platform->PID,0,'session_attendee_limit');
?>
<option value="<?php echo $platform->PID;?>"><?php echo $platform->name;?> (Max attendees: <?php echo $attendee_limit;?>)</option>
<?php endforeach;?>
</select>
</div>
</div>
<?php endwhile; ?>
<div class="col-lg-4">
<button class="btn btn-lg btn-success" type="submit">Add</button>
</div>
<?php echo form_close();?>
<?php endforeach; ?>
</div>
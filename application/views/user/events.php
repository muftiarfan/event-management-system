<div class="col-lg-12">
<?php
$this->load->view('a_event/toolbar');
?>
<table class="table table-striped table-hover ">
  <thead>
    <tr>
      <th>#</th>
      <th>Event</th>
      <th>Details</th>
      <th>Action</th>
    </tr>
  </thead>
  <tbody>
  <?php foreach($events as $event):?>
    <?php
    $check = $this->userM->idData($event->EID,0,'event_passed');
    if($check==TRUE){ $label = '<label class="label label-success">Upcoming</label>';}elseif($check==FALSE){$label = '<label class="label label-default">Passed</label>';}
    ?>
    <tr>
      <td><?php echo $event->EID;?></td>
      <td><?php echo $label;?>&nbsp;<?php echo $event->name;?></td>
      <td>
        <span class="fa fa-calendar fa-1x"></span>
        <?php echo $event->event_date;?>
        <p class="text text-justify">
        <?php echo $event->description;?>
        </p>
      </td>
      <td>
      <div class="btn-group">
      <?php
      $check = $this->userM->idData($event->EID,0,'event_passed');
      if($check==TRUE):
      $added = $this->userM->already_members($event->EID,$user->id);
      ?>
      <a class="btn btn-sm btn-success" href="<?php echo base_url();?>user/event_register/<?php echo $event->EID;?>">Add Members</a>
      <?php
      if($added>0):
      ?>
      <a class="btn btn-sm btn-primary" href="<?php echo base_url();?>user/event_edit/<?php echo $event->EID;?>">Modify</a>
    <?php endif;?>
      <?php
      if($event->detail_file!=NULL):
      ?>

      <a class="btn btn-sm btn-success" href="<?php echo base_url().$event->detail_file;?>">Brochure</a>
      <?php
      endif;
      ?>
    <?php endif;?>
      <?php if($this->ion_auth->in_group(array(1,11))):?>
      <a href="<?php echo base_url();?>event/edit/<?php echo $event->EID;?>" title="Edit event details" class="btn btn-sm btn-warning">Edit</a>
      <!--a href="<?php echo base_url();?>event/add_membersAdmin/<?php echo $event->EID;?>" title="Add members for other users" class="btn btn-sm btn-primary">Add Extra Members</a-->
      <a href="<?php echo base_url();?>event/sessions/<?php echo $event->EID;?>" title="Sessions" class="btn btn-sm btn-primary">Sessions</a>
      <a href="<?php echo base_url();?>event/dashboard/<?php echo $event->EID;?>" title="Dashboard" class="btn btn-sm btn-primary">Dashboard</a>
      <a data-toggle="modal" data-target="#event-<?php echo $event->EID;?>" class="btn btn-sm btn-danger">Delete</a>
      <div id="event-<?php echo $event->EID;?>" class="modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">Delete Event?</h4>
      </div>
      <div class="modal-body">
        <p>Are you sure you want to delete the event named <strong><?php echo $event->name;?></strong>, scheduled to be held on <strong><?php echo $event->event_date;?></strong></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Changed mind</button>
        <a href="<?php echo base_url();?>event/delete/<?php echo $event->EID;?>" class="btn btn-danger">Yes</a>
      </div>
    </div>
  </div>
</div>
    <?php endif;?>
      </div>
      </td>
    </tr>
  <?php endforeach;?>
  </tbody>
</table>
</div>
<?php foreach($user_basics as $profile):?>
<div class="col-lg-12">
<div class="row">
<div class="col-lg-12">
<div class="panel panel-info">
<div class="panel-heading"><h3><strong>Basic Details</strong></h3></div>
<div class="panel-body">
<div class="col-lg-3">
<h4>Name</h4>
<strong><?php echo $profile->full_name;?></strong>
</div>
<div class="col-lg-3">
<h4>Occupation</h4>
<strong><?php echo $profile->occupation;?></strong>
</div>
<div class="col-lg-3">
<h4>Email</h4>
<strong><?php echo $profile->email;?></strong>
</div>
<div class="col-lg-3">
<h4>Mobile</h4>
<strong><?php echo $profile->mobile;?><?php if($profile->mobile_status==0):?><span data-toggle="tooltip" data-placement="top" title="" data-original-title="Number not verified" class="text text-danger fa fa-exclamation-circle fa-1x"></span><?php endif;?></strong>

</div>
<div class="col-lg-3">
<h4>Phone</h4>
<strong><?php echo $profile->phone;?></strong>

</div>
</div>
</div>
</div>
<div class="col-lg-12">
<div class="panel panel-info">
<div class="panel-heading"><h3><strong>Team/Org/School Details</strong></h3></div>
<div class="panel-body">
<div class="col-lg-3">
<h4>Name</h4>
<strong><?php echo $profile->t_org;?></strong>
</div>
<div class="col-lg-3">
<h4>Address</h4>
<strong><?php echo $profile->address;?></strong>
</div>
<div class="col-lg-3">
<h4>District</h4>
<strong><?php echo $profile->district;?></strong>
</div>
<div class="col-lg-3">
<h4>Level</h4>
<strong><?php echo $profile->upto_class;?></strong>

</div>

</div>
</div>
</div>
<?php
if($this->ion_auth->is_admin()):

?>
<div class="col-lg-5">
<div class="panel panel-success">
<div class="panel-heading"><h3><strong>Other Info</strong></h3></div>
<div class="panel-body">
<div class="col-lg-3">
<h4>Individual</h4>
<strong><?php if($profile->individual==1):?>Yes <a href="<?php echo base_url().$profile->icard_path;?>" class="btn btn-primary btn-xs"><span class="fa fa-qrcode fa-w"></span></a><?php else: echo'No'; endif;?></strong>
</div>

</div>
</div>
</div>
<?php
endif;
?>
</div>
</div>

</div>
<?php endforeach;?>

<div class="col-lg-12">
<p class="text text-info">
We have sent an sms with a code to your mobile number <strong><?php echo $phone;?> <a href="<?php echo base_url();?>auth/edit_user/<?php echo $this->ion_auth->user()->row()->id;?>">Incorrect Number?</a></strong>
 </p> 
<?php echo validation_errors(); ?>
<?php echo form_open('user/verify_mobile','class="form-horizontal"');
?>
<div class="form-group">
<div class="col-lg-4">
<label for="otp">Code</label>
<input type="text" placeholder="OTP" name="otp" maxlength="6" id="otp" class="form-control input-lg">
</div>
</div>
<div class="form-group">
<div class="col-lg-4">
<button class="btn btn-lg btn-success">Verify</button>
</div>
</div>
<?php echo form_close();?>
</div>